<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin Dashboard</title>


   <%@include file="admin_header_css.jsp" %>
 
 <script type="text/javascript" src="static/adminResources/clock/jquery-1.6.4.min.js"></script>
 
  <link rel="stylesheet" href="static/adminResources/clock/watch.css">
  
</head>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<body class="hold-transition skin-blue sidebar-mini" onload="activeSideMenu('menu1','0')">

<div class="wrapper">

  
  
  <%@include file="admin_header_bar.jsp" %>
  
  
  
  
  <%@include file="admin_sidemenu_bar.jsp" %>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    
    
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

   
   <c:forEach items="${adminData}" var="list">
   
   
    <!-- Main content -->
    <section class="content">
     
     
      <div class="row">
       
        <section class="col-lg-12 connectedSortable" style="text-align:center;">
         

              
              <img src="static/adminResources/MedicinesKaya.png" height="140px" >

			  <div style="width:100%;font-size:22px;padding:0px ! important;" class="external-event bg-light-blue ui-draggable ui-draggable-handle" style="position: relative;">Admin Panel</div>
                  
        </section>
        
     </div>
     
     
     
      
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
         

              <div class="timeContainer">

					<div class="clock">
						
						<div class="alert alert-info alert-dismissable" style="font-size:18px;padding:2px 1px 2px 9px; background-color: #3c8dbc !important;">		                    		                   
		                    <i class="icon fa fa-clock-o"></i>Last access time : <fmt:formatDate pattern="dd/MM/yyyy hh:mm:ss" value="${list.last_access_time}" />	
		                </div>	
						<!-- fa-info-circle  -->
						<div id="Date"></div>

						<ul>
							<li id="hours"></li>
							<li id="point">:</li>
							<li id="min"></li>
							<li id="point">:</li>
							<li id="sec"></li>
						</ul>

					</div>
				</div>
              


        </section>
        <!-- /.Left col -->


				


				<!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">


          <!-- Calendar -->
          <div class="box box-solid bg-green-gradient">
            <div class="box-header">
              <i class="fa fa-calendar"></i>

              <h3 class="box-title">Calendar</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <!-- button with a dropdown -->
                
                <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <!--The calendar -->
              <div id="calendar" style="width: 100%"></div>
            </div>
            <!-- /.box-body -->
            
          </div>
          <!-- /.box -->
		
		 
			
			
        </section>
        <!-- right col -->
        
        
        
        
        
      </div>
      <!-- /.row (main row) -->



   </c:forEach>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
 
 
  <%@include file="admin_footer.jsp" %>

 
 
</div>
<!-- ./wrapper -->



  <%@include file="admin_script.jsp" %>

</body>


</html>