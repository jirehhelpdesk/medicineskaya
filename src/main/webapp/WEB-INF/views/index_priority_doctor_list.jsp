<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Recommend Doctor</title>

    <%@include file="index_header_css.jsp" %>
    
    
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<body onload="loadAboutUs('<%=request.getAttribute("noOfDoctor")%>')">


<div id="all">
        
        
        <%@include file="index_header_menu.jsp" %>
        

        
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-7">
                        <h1>Doctors</h1>
                    </div> -->
                    <div class="col-md-12">
                        <ul class="breadcrumb" style="padding: 2px 0;margin-bottom:5px;">
                            <li><a href="index">Home</a>
                            </li>
                            <li>Doctors</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        
        
        
        
        <div id="content">
            
            <div class="container">
                
                <div class="row">
                   
                    <!-- *** LEFT COLUMN ***  -->

                    <div id="searchDoctorId"><%@include file="index_search_priority_doctor.jsp" %></div>
                    
                    <!-- /.col-md-9 -->

                    <!-- *** LEFT COLUMN END *** -->

                   
                   
                   
                   
                   
                   
                   
                   
                   
                    <!-- *** RIGHT COLUMN *** -->

                    
                    
                    <div class="col-md-2" style=" padding-left: 1px;padding-right: 1px;">
                        
                        <!-- *** CUSTOMER MENU *** -->
                        <div class="panel panel-default sidebar-menu">

                            <div class="panel-heading">
                                <h3 class="panel-title">Doctor List</h3>
                            </div>

                            <div class="panel-body">

                                <ul class="nav nav-pills nav-stacked">
                                   
                                    <li id="place0" class="active">
                                        <a href="recomendDoctor" style="padding: 5px 1px;" onclick="getSearchDoctor('0','<%=request.getAttribute("noOfDoctor")%>','0')"><i class="fa fa-list"></i>All</a>
                                    </li>
                                    
                                    <%int j=1; %>
                                    <c:forEach items="${doctorDetails}" var="list">	
		                                    
		                                    <li id="place<%=j%>" class="" >
		                                        <a href="#" style="padding: 5px 1px;" onclick="getSearchDoctor('${list.priority_doctor_id}','<%=request.getAttribute("noOfDoctor")%>','<%=j%>')"><i class="fa fa-user-md"></i>${list.doctor_name}</a>
		                                    </li>
                                    <%j = j + 1;%>
                                    </c:forEach>
                                    
                                </ul>
                            </div>

                        </div>
                       
                    </div>
                    <!-- /.col-md-3 -->

                    <!-- *** RIGHT COLUMN END *** -->







                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->



        
        

        

         <%@include file="index_footer.jsp" %>


    </div>
    <!-- /#all -->

    
    <%@include file="index_common_script.jsp" %>
    

</body>
</html>