<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Setting</title>


   <%@include file="admin_header_css.jsp" %>
  
  
</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<body class="hold-transition skin-blue sidebar-mini" onload="activeSideMenu('menu4','0')">


<div class="wrapper">

  
  
  <%@include file="admin_header_bar.jsp" %>
  
  
  
  
  <%@include file="admin_sidemenu_bar.jsp" %>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    
    
    <section class="content-header">
      <h1>
        Setting
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Setting</li>
      </ol>
    </section>

   
   
   
   
    <!-- Main content -->
    <section class="content">
     
     <div class="row">
            
            <!-- left column -->
            
            <div class="col-md-2">
            
            </div>
            
            
            <!-- right column -->
            
            <div class="col-md-8">
              <!-- general form elements disabled -->
              
              
              <!-- Input addon -->
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Basic Changes</h3>
                </div>
                
                
                <div class="box-body">
                 
                 <form id="adminDetailForm" >
                  
		              <c:forEach items="${adminData}" var="list">
		                   
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-user-secret"></i></span>
		                    <input type="text" name="admin_name" id="admin_name" value="${list.admin_name}" class="form-control" placeholder="Admin full name">
		                  </div>
		                  
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-ship"></i></span>
		                    <input type="text" name="admin_designation" id="admin_designation" value="${list.admin_designation}" class="form-control" placeholder="Admin Designation">
		                  </div>
		                  
		                  <!-- <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-photo"></i></span>
		                    <input type="file" name="admin_photo" id="admin_photo" class="form-control" placeholder="Admin Photo">
		                  </div> -->
		               
		             </c:forEach>
		                  
                  </form> 
                  
                  	
						  <div class="box-footer" style="float:right;margin-top:10px;">
		                    <button  class="btn btn-primary" onclick="saveAdminDetails()">Submit</button>
		                  </div>
		                  
		                  
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              
              
            </div><!--/.col (right) -->
            
            
            <div class="col-md-2">
            
            </div>
            
            
          </div>   <!-- /.row -->
          
          
          
          
          
          
          
          
          
          
      
     		<div class="row">
            
            <!-- left column -->
            
            <div class="col-md-2">
            
            </div>
            
            
            <!-- right column -->
            
            <div class="col-md-8">
              <!-- general form elements disabled -->
              
              
              <!-- Input addon -->
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Change Password</h3>
                </div>
                
                
                <div class="box-body">
                 
                 <form id="adminPasswordForm" >
                 
	                
	                 
			                  <div class="input-group">
			                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
			                    <input type="password" name="password" id="password" class="form-control" placeholder="Current Password">
			                  	<span class="errorMsg" id="errPassword"></span>
			                  	
			                  </div>
			                  
			                  <div class="input-group">
			                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
			                    <input type="password" name="new_password" id="new_password" class="form-control" placeholder="New Password">
			                    <span class="errorMsg" id="errNewPassword"></span>
			                  </div>
			                  
			                  <div class="input-group">
			                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
			                    <input type="password" name="cnf_password" id="cnf_password" class="form-control" placeholder="Confirm new password">
			                    <span class="errorMsg" id="errCnfPassword"></span>
			                  </div>
			         
			         
		                  
                  </form> 
                  
                  	
						  <div class="box-footer" style="float:right;margin-top:10px;">
		                    <button  class="btn btn-primary" onclick="saveAdminPassword()">Submit</button>
		                  </div>
		                  
		                  
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              
              
            </div><!--/.col (right) -->
            
            
            <div class="col-md-2">
            
            </div>
            
            
          </div>   <!-- /.row -->
          


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
 
 
  <%@include file="admin_footer.jsp" %>

 
 
</div>
<!-- ./wrapper -->



  <%@include file="admin_script.jsp" %>

</body>


</html>