<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Contact Us</title>

    <%@include file="index_header_css.jsp" %>
    
    
</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<body>


<div id="all">
        
        
        <%@include file="index_header_menu.jsp" %>
        

        
        <div id="heading-breadcrumbs" style="margin-bottom:10px;">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-7">
                        <h1>Contact us</h1>
                    </div> -->
                    <div class="col-md-12">
                        <ul class="breadcrumb" style="padding: 2px 0;margin-bottom:5px;">
                            <li>Home</li>
                            <li>Contact us</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        
        
        
        
        <div id="content">
            <div class="container" id="contact">

                <div class="row">
                    <div class="col-md-8">

                        <section>

                            <div class="heading">
                                <h2>We are here to help you</h2>
                            </div>

                            <p>Are you curious about something? Do you have some kind of problem with our service ? Please feel free to contact us, our customer service center is working for you 24/7.</p>

                            <div class="heading"  style="margin-bottom:10px;">
                                <h3>Contact form</h3>
                            </div>

                            
                                <div class="row">
                                
                                
		                                  <form:form  modelAttribute="contactModel" id="contactForm" >
			                                
			                                    <div class="col-sm-12">
			                                        <div class="form-group">
			                                            <label for="firstname" class="alignLabel" >Full name</label>
			                                            <input type="text" class="form-control" name="contact_name" path="contact_name" id="contact_name" value="${userBean.user_full_name}">
			                                            <span  class="errorMsg" id="errNameId"></span>
			                                        </div>
			                                    </div>
			                                    
			                                    <div class="col-sm-6">
			                                        <div class="form-group">
			                                            <label for="email" class="alignLabel">Email</label>
			                                            <input type="text" class="form-control" name="contact_email_id" path="contact_email_id" id="contact_email_id" value="${userBean.user_emailid}">
			                                            <span class="errorMsg" id="errEmailId"></span>
			                                        </div>
			                                    </div>
			                                    
			                                    <div class="col-sm-6">
			                                        <div class="form-group">
			                                            <label for="email" class="alignLabel">Phone</label>
			                                            <input type="text" class="form-control" name="contact_phone" path="contact_phone" id="contact_phone" value="${userBean.user_mobile}">
			                                            <span class="errorMsg" id="errPhoneId"></span>
			                                        </div>
			                                    </div>
			                                    
			                                    <div class="col-sm-12">
			                                        <div class="form-group" >
			                                            <label for="subject" class="alignLabel">Reason for contact</label>
			                                            <select class="form-control" name="contact_reason" path="contact_reason" id="contact_reason" >
			                                            			<option value="Select reason for contact">Select reason for contact</option>
			                                            			<option value="Enquiry">Enquiry</option>
			                                            			<option value="Feedback">Feedback</option>
			                                            			<option value="Complain">Complain</option>
			                                            </select>
			                                            <span class="errorMsg" id="errReasonId"></span>
			                                        </div>
			                                    </div>
			                                    
			                                    <div class="col-sm-12">
			                                        <div class="form-group" >
			                                            <label for="subject" class="alignLabel">Subject</label>
			                                            <input type="text" class="form-control" name="contact_subject" path="contact_subject" id="contact_subject">
			                                            <span class="errorMsg" id="errSubjectId"></span>
			                                        </div>
			                                    </div>
			                                    
			                                    <div class="col-sm-12">
			                                        <div class="form-group">
			                                            <label for="message"  class="alignLabel">Message</label>
			                                            <textarea name="contact_message" path="contact_message" id="contact_message" style="height: 240px;" class="form-control"></textarea>
			                                            <span class="errorMsg" id="errMessageId"></span>
			                                        </div>
			                                    </div>
			
										</form:form>
										
										
	                                    <div class="col-sm-12 text-center alignLabel">
	                                        <button class="btn btn-template-main" onclick="saveContactDetails()"><i class="fa fa-envelope-o"></i>Submit</button>
	                                    </div>
                                    
                                </div>
                                
                                <!-- /.row -->
                            

                        </section>

                    </div>

                    <div class="col-md-4">

                        <section>

                            <h3 class="text-uppercase">Address</h3>
							
							<p>
							<b>India Office</b>
							<br>HO:- 42, Vishal, Vasundhara Enclave, Delhi, India
							<br>Mobile (India)  :  +91 91024 81133 , +91 98733 49502 ,  +91 99101 50660, +91 7290818133.
							</p>
						
							<p>
							<b>Nepal Office</b>
							<br>Soaltee Mode, Kathmandu, Nepal
							<br>Mobile (Nepal) : +977 9851179515
							</p>
							
							
                            <h4><b>Email us</b></h4>

                            <p class="text-muted">Please feel free to write an email to us.</p>
                            <ul>
                                <li><strong><a href="mailto:contact@medicineskaya.com">contact@medicineskaya.com</a></strong>
                                </li>                               
                            </ul>

                        </section>

                    </div>

                </div>
                <!-- /.row -->


            </div>
            <!-- /#contact.container -->
        </div>
        <!-- /#content -->

        <div id="map">
        </div>
        
        

        

         <%@include file="index_footer.jsp" %>


    </div>
    <!-- /#all -->

    
    <%@include file="index_common_script.jsp" %>
    

</body>
</html>