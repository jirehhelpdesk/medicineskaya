<html>
	<head>
		<link rel="stylesheet" href="static/adminResources/clock/compiled/flipclock.css">

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

		<script src="static/adminResources/clock/compiled/flipclock.js"></script>		
	</head>
	<body>
		<div class="clock" style="margin:2em;"></div>

		<script type="text/javascript">
			var clock;
			
			$(document).ready(function() {
				clock = $('.clock').FlipClock({
					clockFace: 'TwelveHourClock'
				});
			});
		</script>
		
	</body>
</html>