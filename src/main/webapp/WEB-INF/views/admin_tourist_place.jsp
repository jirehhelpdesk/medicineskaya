<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tourist Places</title>


   <%@include file="admin_header_css.jsp" %>
  
    <script type="text/javascript" src="static/adminResources/editor/jscripts/tiny_mce/tiny_mce.js"></script>

	<script type="text/javascript">
	tinyMCE.init({
			// General options
			mode : "textareas",
			theme : "advanced",
			plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
			// Theme options
			theme_advanced_buttons1 : "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,		
			template_replace_values : 
			{
			 username : "Some User",
			 staffid : "991234"
			}
		});
		
	</script>
  
  
  
  
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<body class="hold-transition skin-blue sidebar-mini" onload="activeSideMenu('menu3','1')">

<div class="wrapper">

  
  
  <%@include file="admin_header_bar.jsp" %>
  
  
  
  
  <%@include file="admin_sidemenu_bar.jsp" %>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    
    
    <section class="content-header">
      <h1>
        Tourist Places
        <small>MedicinesKaya</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li >Portal Data</li><li class="active">Tourist Places</li>
      </ol>
    </section>

   
   
   
   
    <!-- Main content -->
        <section class="content">
          
          <div class="row">
            
            <!-- left column -->
            
            <div class="col-md-2">
            
            </div>
            
            
            <!-- right column -->
            
            <div class="col-md-8">
              <!-- general form elements disabled -->
              
              
              <!-- Input addon -->
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Add Tourist Details</h3>
                </div>
                
                
                <div class="box-body">
                 
                <form:form  id="touristPlaceForm" modelAttribute="placeModel" method="post" action="saveTouristPlaceDetails" >
                  
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-globe"></i></span>
		                    <input type="text" name="place_name" id="place_name" class="form-control" placeholder="Tourist place name">
		                  	 <span class="errorMsg" id="errName"></span>
		                  </div>
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
		                    <input type="text" name="location" id="location" class="form-control" placeholder="Location briefly">
		                  	 <span class="errorMsg" id="errLocation"></span>
		                  </div>
		                  
		                  <div class="input-group" style="width:98%;" >		                  	
		                  	<textarea  class="form-control" id="aboutUs" name="about_place"></textarea>
		                  	 <span class="errorMsg" id="errAboutUs"></span>
		                  </div>
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-photo"></i></span>
		                    <input type="file"  class="form-control" id="file" name="file">
		                  </div>
		                  
		                  
                  </form:form> 
                  
                  	
						  <div class="box-footer" style="float:right;margin-top:10px;">
		                    <button  class="btn btn-primary" onclick="saveTouristPlaceDetails()">Submit</button>
		                  </div>
		                  
		                  
                </div><!-- /.box-body -->
                
              </div><!-- /.box -->
              
              
            </div><!--/.col (right) -->
            
            
            <div class="col-md-2">
            
            </div>
            
            
          </div>   <!-- /.row -->
          
          
        </section><!-- /.content -->
        
        
        
        
        
        
  </div>
  <!-- /.content-wrapper -->
 
 
 
  <%@include file="admin_footer.jsp" %>

 
 
</div>
<!-- ./wrapper -->



  <%@include file="admin_script.jsp" %>

</body>


</html>