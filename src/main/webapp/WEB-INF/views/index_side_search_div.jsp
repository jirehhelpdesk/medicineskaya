                    
                    
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
                    
                    
                    <div class="panel panel-default sidebar-menu with-icons">

                            <div class="panel-heading">
                                <h3 class="panel-title">Search</h3>
                            </div>

                          
                            <div class="panel-body">

							  
								
								<div class="row">
									
									<form id="searchForm">
									
										<div class="col-sm-12">
											<div class="form-group">
												<label for="firstname"></label> 
												
												<select id="category" name="categoryName" class="form-control" onchange="showCityNames(this.value)">
	
												      <option value="Select Category">Select Category</option>
												      
												      <c:forEach items="${categoryList}" var="list">	 
												      
												      			<option value="${list.category_name}">${list.category_name}</option>
												      
												      </c:forEach>
												      
												</select>
											</div>
										</div>
									
										<div class="col-sm-12">
											<div class="form-group">
												<label for="firstname"></label> 
												
												<select id="city" name="cityName" class="form-control" onchange="showValueList(this.value)">
	
												      <option value="Select City">Select City</option>
												      
												</select>
											</div>
										</div>

										<div class="col-sm-12">
											<div class="form-group">
												<label for="message"></label> 
												
												<select id="value" name="valueName" class="form-control">
	
												      <option value="Select form the list">Select from the list</option>
												      
												</select>
											</div>
										</div>

									</form>
									
									
										<div class="col-sm-12 text-center"  id="sideDiv" >
											
											<%@include file="index_side_search_button.jsp" %>
											
										</div>
									
								</div>
								<!-- /.row -->
							

						</div>
                            
                            
                            
                        </div>