<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Change Password</title>

    <%@include file="index_header_css.jsp" %>
    
    
</head>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<body onload="activeHeaderMenu('menu8')">


<div id="all">
        
        
        <%@include file="index_header_menu.jsp" %>
        

        
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1>Change Password</h1>
                    </div>
                    <div class="col-md-5">
                        <ul class="breadcrumb">
                            <li><a href="index">Profile</a>
                            </li>
                            <li>Change Password</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        
        
        
        
        <div id="content">
            <div class="container">

                <div class="row">
                    <div class="col-md-6">
                        <div class="box">
                            <h2 class="text-uppercase">Change Password</h2>

                            <p class="lead">Not our registered user yet?</p>
                            <p>With registration with us new safe world of health, get safest and healthiest and much more opens to you! The whole process will not take you more than a minute!</p>
                            <p class="text-muted">If you have any questions, please feel free to <a href="contactUs">contact us</a>, our customer service center is working for you 24/7.</p>
							
							
							<div class="text-center">
                                <button class="btn btn-template-main" onclick="redirectUrl('registration')"><i class="fa fa-user-md"></i> Go to Sign Up</button>
                            </div>
                            
                            <hr>

                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="box">
                            <h2 class="text-uppercase" style="margin-bottom: 8px; margin-top: 1px;">Change Password</h2>

                            <p class="text-muted">Change your password to make secure your account.</p>

                            <hr>
							
							
        					<span class="alertMsg"></span>
        					
                            <form class="login-form"  id="changePasswordForm" >
						                    
                                <div class="form-group">
                                    <label for="email">Current Password</label>
                                    <input type="password" class="form-control" name="exist_password" id="exist_password">
                                    <span class="errorMsg" id="errCurPasswordId"></span>
                                </div>
                                
                                <div class="form-group">
                                    <label for="password">New Password</label>
                                    <input type="password" class="form-control" name="new_password"  id="new_password">
                                    <span class="errorMsg" id="errPasswordId"></span>
                                </div>
                                
                                <div class="form-group">
                                    <label for="password">Confirm New Password</label>
                                    <input type="password" class="form-control" name="cnf_password"  id="cnf_password">
                                    <span class="errorMsg" id="errCnfPasswordId"></span>
                                </div>
                                
                            </form>
                            
                                <div class="text-center">
                                    <button style="margin-top: 10px;" class="btn btn-template-main" onclick="changePassword()"><i class="fa fa-sign-in"></i> Change Password </button>
                                </div>
                            
                        </div>
                    </div>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
        
        

        

         <%@include file="index_footer.jsp" %>


    </div>
    <!-- /#all -->

    
    <%@include file="index_common_script.jsp" %>
    

</body>
</html>