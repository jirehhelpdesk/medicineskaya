<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Second Opinion</title>

    <%@include file="index_header_css.jsp" %>
    
     <link rel="stylesheet" type="text/css" href="static/datePicker/jquery.datetimepicker.css"/>
     
</head>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<body onload="activeHeaderMenu('menu5')">


<div id="all">
        
        
        <%@include file="index_header_menu.jsp" %>
        

        
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-7">
                        <h1>Second Opinion</h1>
                    </div> -->
                    <div class="col-md-12">
                        <ul class="breadcrumb" style="padding: 2px 0;margin-bottom:5px;">
                            <li>Home</li>
                            <li>Second Opinion</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        
        
        <div id="content">
            <div class="container">

                <section>
                    
                    <div class="row">
                        
                        
                     <div class="col-md-3">
                     
                        <!-- *** MENUS AND WIDGETS *** -->

                            <%@include file="index_side_search_div.jsp" %>

                        <!-- *** MENUS AND FILTERS END *** -->
                        
                     </div>
                    
                     
                     
                     <div class="col-md-9 rightContentDiv" id="categoryResultHeading">

                          <div class="heading">
                              <h2>Second Opinion</h2>
                          </div>

                          <div class="col-md-10 col-md-offset-1">
                            
                            
                                <div class="row">
                                    
                                     
                                    <form:form modelAttribute="opinionModel" id="opinionForm">
                                    
		                                    <div class="col-sm-12">
		                                        <div class="form-group">
		                                            <label for="firstname">Treatment On</label>
		                                             <select class="form-control" name="treatment_on" path="treatment_on" id="treatment_on" onchange="getDoctorList(this.value)">
								                    		<option value="Select Treatment On">Select Treatment On</option>
								                    		<c:forEach items="${specialist_list}" var="val">
								                    			<option value="${val.specialist_type}">${val.specialist_type}</option>
								                    		</c:forEach>
								                    </select>
								                    <span class="errorMsg" id="errSpecializationId"></span>
		                                        </div>
		                                    </div>
		                                    
		                                    <div class="col-sm-12">
		                                        <div class="form-group">
		                                            <label for="lastname">Doctor</label>
		                                             <select class="form-control" name="doctor_id" path="doctor_id" id="doctor_id" >
								                    		<option value="Select Doctor">Select Doctor</option>								                    		
								                    </select>
								                    <span class="errorMsg" id="errDoctorId"></span>
		                                        </div>
		                                    </div>
		                                    
		                                   <div class="col-sm-12">
		                                        <div class="form-group">
		                                            <label for="firstname">Appointment Date</label>
		                                            <input class="some_class form-control" name="appointment_date" id="some_class_1" type="text">
		                                            <span class="errorMsg" id="errDateId"></span>
		                                        </div>
		                                    </div>
		                                    
		                                    <div class="col-sm-12">
		                                        <div class="form-group">
		                                            <label for="message">Remark</label>
		                                            <textarea id="remark" class="form-control" style="height:240px;" name="remark" path="remark" ></textarea>
		                                            <span class="errorMsg" id="errRemarkId"></span>
		                                        </div>
		                                    </div>
		                                    

                                   </form:form>
                                   
                                   <div id="lowerDiv">
                                    
	                                    <%@include file="index_opinion_button.jsp" %>
												
                                   </div>  
                                    
                                    
                                    
                                    
                                </div>
                                
                                
                                <!-- /.row -->
                            



                        </div>

                      </div>
                      
                      
                    </div>

                    
                    
                </section>
                
            </div>
            <!-- /#contact.container -->

            
           

            

            <%@include file="index_hospital.jsp" %>

            

		<div id="get-it" style="padding: 0 0 3px ! important;">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                   
                </div>
                <div class="col-md-4 col-sm-12">
                
                </div>
            </div>
        </div>
        
        
        </div>
        <!-- /#content -->

        
		 
	 <!-- /#all -->

    <script src="static/datePicker/jquery.js"></script>
	<script src="static/datePicker/jquery.datetimepicker.js"></script>
	<script>
	
	$('.some_class').datetimepicker();
	
	$('#some_class3').datetimepicker();
	
	/* $('#datetimepicker_dark').datetimepicker({theme:'dark'}) */
		
	</script>
	
	
         <%@include file="index_footer.jsp" %>


    </div>
    
    
   
	
	
    <%@include file="index_common_script.jsp" %>
    

</body>
</html>