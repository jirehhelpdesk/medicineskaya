<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Service</title>


   <%@include file="admin_header_css.jsp" %>
  
  
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>



<body class="hold-transition skin-blue sidebar-mini" onload="activeSideMenu('menu2','0')">

<div class="wrapper">

  
  
  <%@include file="admin_header_bar.jsp" %>
  
  
  
  
  <%@include file="admin_sidemenu_bar.jsp" %>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    
    
    <section class="content-header">
      <h1>
        Service
        <small>MedicinesKaya</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Service</li>
      </ol>
    </section>

   
   
   
   
    <!-- Main content -->
        <section class="content">
          
          <div class="row">
            
            <!-- left column -->
            
            <div class="col-md-3">
            
            </div>
            
            
            
            
            <!-- right column -->
            
            <div class="col-md-6">
              <!-- general form elements disabled -->
              
              
              <!-- Input addon -->
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Insert Category Record</h3>
                </div>
                
                
                <div class="box-body">
                 
                <form id="categoryForm" >
                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-sitemap"></i></span>
		                    <select class="form-control" name="category_type" id="category_type" onchange="showHiddenComponet(this.value)">
		                    		<option value="Select Category">Select Category</option>
		                    		
		                    		<c:forEach items="${categoryList}" var="val">
		                    			<option value="${val.category_name}">${val.category_name}</option>
		                    		</c:forEach>
		                    		
		                    </select>
		                    <span class="errorMsg" id="errCategory"></span>
		                  </div>
		                  
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
		                    <input type="text" name="name" id="name" class="form-control" placeholder="Name">
		                    <span class="errorMsg" id="errName"></span>
		                  </div>
		                  
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
		                    <input type="email" name="email" id="email" class="form-control" placeholder="Email">
		                    <span class="errorMsg" id="errEmail"></span>
		                  </div>
		                  
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
		                    <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone number">
		                    <span class="errorMsg" id="errPhone"></span>
		                  </div>
		                  
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-institution"></i></span>
		                    <textarea  class="form-control" name="address" id="address" placeholder="Full Address with pincode"></textarea>
		                    <span class="errorMsg" id="errAddress"></span>
		                  </div>
		                  
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-globe"></i></span>
		                    <select class="form-control" name="city_name" id="city_name" >
		                    		<option value="Select City">Select City</option>
		                    		<c:forEach items="${cityList}" var="val">
		                    			<option value="${val.city_name}">${val.city_name}</option>
		                    		</c:forEach>
		                    </select>
		                    <span class="errorMsg" id="errCity"></span>
		                  </div>
		                  
		                
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-photo"></i></span>
		                    <input type="file"  class="form-control" id="file" name="file">
		                  </div>
		                  
		                  
		                  
		                  <div class="input-group" id="specialist" style="display:none;">
		                  	
		                  	<span class="input-group-addon"><i class="fa fa-stethoscope"></i></span>
		                    <select class="form-control" name="specialist" id="specialist" >
		                    		<option value="Select Specialist">Select Specialist</option>
		                    		<c:forEach items="${specialist_list}" var="val">
		                    			<option value="${val.specialist_type}">${val.specialist_type}</option>
		                    		</c:forEach>
		                    </select>
		                    <span class="errorMsg" id="errSpecialist"></span>
		                  </div>
		                  
		                  <div class="input-group" id="cusiene" style="display:none;">
		                  	
			                  	<span class="input-group-addon"><i class="fa fa-skyatlas"></i></span>
			                    <select class="form-control" name="food_type" id="food_type" >
			                    		<option value="Select food type">Select food type</option>
			                    		<option value="Food not availabe">Food not available</option>
			                    		<c:forEach items="${foodTypeList}" var="val">
			                    			<option value="${val.food_type}">${val.food_type}</option>
			                    		</c:forEach>
			                    </select>
			                    <span class="errorMsg" id="errFoodType"></span>
		                  </div>
		                  
		                  
		                  <div class="input-group" id="gender" style="display:none;">
		                    <span class="input-group-addon"><i class="fa fa-venus-mars"></i></span>
		                    <select class="form-control" name="gender" id="gender" >
		                    		<option value="Select Gender">Select Gender</option>
		                    		<option value="Female">Female</option>
		                    		<option value="Male">Male</option>
		                    </select>
		                    <span class="errorMsg" id="errGender"></span>
		                  </div>
		                  
		                  
		                  <div class="input-group" id="hotelTypeDiv" style="display:none;">
		                  	
		                  	<span class="input-group-addon"><i class="fa fa-hotel"></i></span>
		                    <select class="form-control" name="hotelType" id="hotelType">
		                    		
		                    		<option value="Select hotel type">Select hotel type</option>
		                    		<c:forEach items="${shelterTypeList}" var="val">
		                    			<option value="${val.shelter_type}">${val.shelter_type}</option>
		                    		</c:forEach>
		                    </select>
		                    <span class="errorMsg" id="errHotelType"></span>
		                  </div>
		                  
		                  
		                  <div class="input-group" id="website" style="display:none;">
		                  	
		                  	<span class="input-group-addon"><i class="fa fa-link"></i></span>
		                    <input type="text" class="form-control" name="websiteLink" id="websiteLink" placeholder="Website link">
		                  	
		                  </div>
		                  
		                  <div class="input-group" id="taxiType" style="display:none;">
		                  	
		                  	<span class="input-group-addon"><i class="fa fa-taxi"></i></span>
		                    <select class="form-control" name="taxiType" id="taxiType">
		                    		
		                    		<option value="Select cab service type">Select cab service type</option>
		                    		<option value="Radio Cab">Radio Cab</option>
		                    		<option value="Day Cab">Day Cab</option>
		                    		<option value="Fixed Charge Cab">Fixed Charge Cab</option>
		                    		
		                    </select>
		                    <span class="errorMsg" id="errCabType"></span>
		                  </div>
		                  
		                  
		                  <div class="input-group" id="language" style="display:none;">
		                  	
		                  	
			                  	<h5>Language know</h5>
			                  	<ul class="gridCheckBix" style="margin-left:-50px;">
			                  		
			                  		<%int l = 1; %>
			                  		<c:forEach items="${languageList}" var="val">
		                    			<li><input type="checkbox" class="language" value="${val.language_name}" >&nbsp;${val.language_name}</li>
		                    		    <% l = l + 1; %>
		                    		</c:forEach>
			                  		
			                  	</ul>
		                  		<span class="errorMsg" id="errLanguage"></span>
		                  </div>
		                  
		                  
		                  
		                  <br/>
		                  
                  </form> 
                  
                  	
						  <div class="box-footer" style="float:right;margin-top:30px;">
		                    <button  class="btn btn-primary" onclick="saveCategoryDetails()">Submit</button>
		                  </div>
		                  
		                  
                  
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              
              
            </div><!--/.col (right) -->
            
            
            <div class="col-md-3">
            
            </div>
            
            
          </div>   <!-- /.row -->
          
          
        </section><!-- /.content -->
        
        
        
        
        
        
        
        
  </div>
  <!-- /.content-wrapper -->
 
 
 
  <%@include file="admin_footer.jsp" %>

 
 
</div>
<!-- ./wrapper -->



  <%@include file="admin_script.jsp" %>

</body>


</html>