<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<body>

<c:forEach items="${adminData}" var="list">
      
      
      <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="static/indexResources/common_images/MedicinesKaya_admin.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>${list.admin_name}</p>
          <a href="#" style="font-size:12px;"><i class="fa fa-user-secret text-success"></i> ${list.admin_designation}</a>
        </div>
      </div>
      
      
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        
        
        <li id="menu1" class="treeview">
          <a href="adminHome">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span> <!-- <i class="fa fa-angle-left pull-right"></i> -->
          </a>         
        </li>
        
        <li id="menu5" class="treeview">
          <a href="userReport">
            <i class="fa fa-newspaper-o"></i>
            <span>Report </span>
            <!-- <span class="label label-primary pull-right">4</span> -->
          </a>          
        </li>
        
        <li id="menu2" class="treeview">
          <a href="adminService">
            <i class="fa fa-sitemap"></i>
            <span>Category Data</span>
            <!-- <span class="label label-primary pull-right">4</span> -->
          </a>          
        </li>
        
        <li id="menu3"  class="treeview">
              <a href="touristPlace">
                <i class="fa fa-database"></i>
                <span>Portal Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li id="menu3_1"><a href="touristPlace"><i class="fa fa-circle-o"></i>Tourist Place details</a></li>
                <li id="menu3_2"><a href="priorityDoctor"><i class="fa fa-circle-o"></i>Doctor Details</a></li>             
              </ul>
       </li>
            
        
        <li id="menu4">
          <a href="adminSetting">
            <i class="fa fa-gears"></i> <span>Setting</span>
            <!-- <small class="label pull-right bg-green">new</small> -->
          </a>
        </li>
        
        
       <!--  <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Charts</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
            <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
            <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
            <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
          </ul>
        </li> -->
        
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
      
      
 </c:forEach>
      
</body>
</html>