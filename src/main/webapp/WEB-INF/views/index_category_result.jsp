<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="static/adminResources/paging/other.css">
<script type="text/javascript" src="static/adminResources/paging/pageingScript.js"></script> 

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<body>



	<div class="col-md-12" id="customer-orders">
		
		
		<div id="viewMoreDiv"></div>
		
		
		<div class="heading" style="margin-bottom: 12px;">
            <h2 style="font-size: 19px;font-weight: 300;">Search Result</h2>
        </div>
		
		<c:if test="${!empty resultCategoryBean}">
		
		
			<div class="box">
	
				<div class="table-responsive">
					<table class="table table-hover" style="margin-bottom:2px;" role="grid" id="tablepaging" >
						<thead>
							<tr>
								<th>Name</th>
								<th>Phone</th>
								<th>Email id</th>
								<th>City</th>
								<th>Action</th>
							</tr>
						</thead>
	
						<tbody id="myTableBody">
	
							<c:forEach items="${resultCategoryBean}" var="list">
	
								<tr>
									<th>${list.name}</th>
									<td>${list.phone}</td>
									<td>${list.email_id}</td>
									<td>${list.city}</td>
									<td><a href="#"  onclick="viewMoreAboutCategory('${list.id}','${list.type}')">View More</a></td>
								</tr>
	
							</c:forEach>
	
						</tbody>
	
					</table>
					
					<div id="pageNavPosition" style="padding-top: 20px;float: right;" align="center">
								</div>
								<script type="text/javascript">
								
								var pager = new Pager('tablepaging', 10);
								pager.init();
								pager.showPageNav('pager', 'pageNavPosition');
								pager.showPage(1);
								</script>
								
								
				</div>
				<!-- /.table-responsive -->
	
			</div>
			<!-- /.box -->
				
		</c:if>
		
		<c:if test="${empty resultCategoryBean}">
		
			<div class="alert alert-info" role="alert">As per the search criteria no data was found.</div>
		
		</c:if>
			
			
			
			
	</div>
	<!-- /.col-md-9 -->



</body>
</html>