

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
		
<script type="text/javascript" src="static/paging/pageingScript.js"></script> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	  $("#btnExport").click(function(e) {
	    e.preventDefault();

	    //getting data from our table
	    var data_type = 'data:application/vnd.ms-excel';
	    var table_div = document.getElementById('table_wrapper');
	    var table_html = table_div.outerHTML.replace(/ /g, '%20');

	    var a = document.createElement('a');
	    a.href = data_type + ', ' + table_html;
	    a.download = 'exported_table_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
	    a.click();
	  });
	});
</script>

			
		<div class="row">
            
            <div class="col-xs-12">
              
              <div class="box" style="background-color:#fff ! important;">
                
                
                <div class="box-header">
                  <h3 class="box-title">Contact Us Report</h3>
                  <div class="box-tools">
                    <div class="input-group">                      
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default" style="float:right;" id="btnExport" onclick="downloadReport()"><i class="fa fa-file-excel-o"></i> &nbsp; Export Report</button>
                      </div>
                    </div>
                  </div>
                </div><!-- /.box-header -->
                
                
                
                <div id="table_wrapper" class="box-body table-responsive no-padding">
                  
                  <c:if test="${!empty recordDetails}">  
                  
                  
		                  <table id="tablepaging" class="table table-hover table2excel">
		                    <tr>
		                          <th>Sl No</th>
			                      <th>Full Name</th>
			                      <th>Email id</th>
			                      <th>Phone</th>
			                      <th>Contact For</th>
			                      <th>Subject</th>
			                      <th>Contact Date</th>
			                     
			                      <th>Manage</th>
		                    </tr>
		                    
		                    <tbody  id="myTableBody">  
		                     
				                    <%int i=1; %>
				                    <c:forEach items="${recordDetails}" var="list">	
					                    
					                    <tr>
					                          
						                      <td><%=i++%></td>
						                     
						                      <td>${list.contact_name}</td>
						                      <td>${list.contact_email_id}</td>
						                      <td>${list.contact_phone}</td>
						                      <td>${list.contact_reason}</td>
						                      <td>${list.contact_subject}</td>
						                      <td><fmt:formatDate pattern="dd/MM/yyyy" value="${list.contact_date}" /></td>
						                      
						                      <td><button class="btn btn-primary" style="padding: 1px 3px ! important;" onclick="viewMessage('${list.contact_id}','Contact')">view</button></td>
					                          
				                       </tr>
				                       
				                   </c:forEach> 
				                   
		                    </tbody>
		                    
		                    
		                  </table>
                  			
                  			
                  			<!-- Paging zone -->
          							
				     			<div id="pageNavPosition" style="padding-top: 20px;float: right;" align="center">
								</div>
								<script type="text/javascript">
								
								var pager = new Pager('tablepaging', 10);
								pager.init();
								pager.showPageNav('pager', 'pageNavPosition');
								pager.showPage(1);
								</script>
				                          
			                 
		                   <!-- End of  Paging zone -->
                  			
                  			
                  </c:if>
                  
                  <c:if test="${empty recordDetails}">  
                  
                  		<div class="modal-content">
				                  
		                  <div class="modal-body">
		                    <p>No record was found as per your criteria.</p>
		                  </div>
		                  
		                </div><!-- /.modal-content -->
		                
                  </c:if>
                  
                </div><!-- /.box-body -->
                
                
                
                
                
              </div><!-- /.box -->
            </div>
          </div>
          

          
   