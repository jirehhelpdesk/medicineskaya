    
    
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="static/indexResources/js/jquery-1.11.0.min.js"><\/script>')
    </script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <script src="static/indexResources/js/jquery.cookie.js"></script>
    <script src="static/indexResources/js/waypoints.min.js"></script>
    <script src="static/indexResources/js/jquery.counterup.min.js"></script>
    <script src="static/indexResources/js/jquery.parallax-1.1.3.js"></script>
    <script src="static/indexResources/js/front.js"></script>

    

    <!-- owl carousel -->
    <script src="static/indexResources/js/owl.carousel.min.js"></script>
    
    
    
    
     <script src="static/indexResources/user_operation_script.js"></script>
    
    
    
     <!-- gmaps -->

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

    <script src="static/indexResources/js/gmaps.js"></script>
    <script src="static/indexResources/js/gmaps.init.js"></script>

    <!-- gmaps end -->
    
    
    
    
   
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	
	
    
    <!-- Loader Div -->
    
    <div  id="loaderDiv" >
  
           <div class="backGroundFade" id="bgModel" ></div>
            
            <div class="" id="loaderId" style="display:none;">

                <%@include file="rippleLoader.jsp" %>
                
            </div>
    </div>
   
   <!-- End of Loader Div -->
   
   
   <!-- Pop Up Div -->
   
   <div  id="popUpDiv" >
  
           <div class="backGroundFade" id="popUpBgModel" ></div>
            
            <div class="" id="popContentId" style="display:none;">

                 <div class="modal-content popDivStyle" id="">
                    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closePopDiv()">&times;</button>
                        <h4 class="modal-title" id="Login">Message</h4>
                    </div>
                    
                    <div class="modal-body">
                        
                        <p class="text-center text-muted" id="messageDivId"></p>
                        
                        <!-- <p class="text-center text-muted"><a href="registration"><strong>Register now</strong></a>! It is easy and done in 1&nbsp;minute and gives you access to portal and much more!</p>
						 -->
						
						<p class="text-center">
                             <button class="btn btn-template-main" onclick="closePopDiv()"><i class="fa fa-sign-in"></i>OK</button>
                        </p>
                            
                    </div>
                    
                </div>
                
            </div>
    </div>
    
   <!-- End of Pop Up div -->
   
   
   
   
   
   
   
   
   
   
   
   
   <!-- Pop Up Div -->
   
   <div  id="authAlertDiv" >
  
           <div class="backGroundFade" id="popUpBgModel" ></div>
            
             <div class="" id="popAlertId" style="display:none;">

                 <div class="modal-content popDivStyle" id="">
                    
                    <div class="modal-header">
                        <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closePopDiv()">&times;</button> -->
                        <h4 class="modal-title" id="Login">Message</h4>
                    </div>
                    
                    <div class="modal-body">
                        
                        <p class="text-center text-muted" id="alertDivId"></p>
                        
                        <!-- <p class="text-center text-muted"><a href="registration"><strong>Register now</strong></a>! It is easy and done in 1&nbsp;minute and gives you access to portal and much more!</p>
						 -->``
						
						<p class="text-center">
                             <button class="btn btn-template-main" onclick="closeAlertDiv()"><i class="fa fa-sign-in"></i>OK</button>
                        </p>
                            
                    </div>
                    
                </div>
                
            </div>
    </div>
    
   <!-- End of Pop Up div -->
   
   
   
   
   
   
   
   
   
   <!-- Pop Up Div -->
   
   <div  id="authPopDiv" >
  
            <div class="backGroundFade" id="popUpBgModel" ></div>
            
            
            
            
            <div class="" id="loginContentId" style="display:none;">

                 <div class="modal-content loginPopDiv">
                    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeAuthPop()">&times;</button>
                        <h4 class="modal-title" id="Login">User login</h4>
                    </div>
                    
                    <div class="modal-body">
                        
                        <span class="errorMsg" id="authMessage"></span>
                        
                        <form id="loginForm">
                           
                            <div class="form-group">
                                <input type="text" autocomplete="off" class="form-control" id="email_id" name="email_id" placeholder="Email id">
                                <span class="errorMsg" id="errLoginEmailId"></span>
                            </div>
                            <div class="form-group">
                                <input type="password" autocomplete="off" class="form-control" id="password" name="password" placeholder="Password">
                                <span class="errorMsg" id="errLoginPasswordId"></span>
                            </div>

                        </form>
							
							
                            <p class="text-center">
                                <button class="btn btn-template-main" onclick="checkPopLogin()"><i class="fa fa-sign-in"></i> Log in</button>
                            </p>
							
                        
                        <p class="text-center text-muted"><a href="#" onclick="showRegistrationDiv()"><strong>Register now &nbsp;</strong></a>If you do not have an account.</p>

                    </div>
                    
                </div>
                
            </div>
            
            
            
            
            
            <div class="" id="registrationContentId" style="display:none;">

                 <div class="modal-content regPopDiv">
                    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeAuthPop()">&times;</button>
                        <h4 class="modal-title" id="Login">User Registration</h4>
                    </div>
                    
                    <div class="modal-body">
                        
                        <form id="regForm">
                           
                               <div class="row">
	                                    	
                                     <div class="col-md-12">
                                         <div class="form-group">
		                                    <label for="name-login">Name</label>
		                                    <input type="text" autocomplete="off" class="form-control" name="user_full_name"  id="user_full_name">
		                                    <span class="errorMsg" id="errNameId"></span>
		                                </div>
                                     </div>
                                     
                               </div>
                               
                               <div class="row">
	                                    	
                                     <div class="col-md-6">
                                         <div class="form-group">
		                                    <label for="email-login">Email</label>
		                                    <input type="text" autocomplete="off" class="form-control" name="user_emailid"  id="user_emailid">
		                                    <span class="errorMsg" id="errEmailId"></span>
		                                </div>
                                     </div>
                                     
                                     <div class="col-md-6">
                                         <div class="form-group">
		                                    <label for="email-login">Mobile no</label>
		                                    <input type="text" autocomplete="off" class="form-control" name="user_mobile"  id="user_mobile">
		                                    <span class="errorMsg" id="errMobileId"></span>
		                                </div>
                                     </div>
                                     
                               </div>
                               
                               <div class="row">
	                                    	
                                     <div class="col-md-6">
                                         <div class="form-group">
		                                    <label for="email-login">Country *</label>
		                                    <select class="form-control" name="user_country" path="user_country" id="user_country" onchange="showStateNames(this.value)">
		                                    			<option value="Select Country">Select Country</option>
		                                    			
		                                    			<c:forEach items="${countryList}" var="list">	
		                                    					<option value="${list.country_name}">${list.country_name}</option>
		                                    			</c:forEach>
		                                    			
		                                    </select>
		                                    <span class="errorMsg" id="errCountryId"></span>
		                                </div>
                                     </div>
                                     
                                     <div class="col-md-6">
                                         <div class="form-group">
		                                    <label for="email-login">State</label>
		                                    <select class="form-control" name="user_state" disabled path="user_state" id="user_state" >
		                                    			<option value="Select State">Select State</option>
		                                    </select>
		                                    <span class="errorMsg" id="errStateId"></span>
		                                </div>
                                     </div>
                                     
                               </div>
                               
                               <div class="row">
	                                    	
                                     <div class="col-md-6">
                                         <div class="form-group">
		                                    <label for="password-login">Password</label>
		                                    <input type="password" autocomplete="off" class="form-control" name="user_password" id="user_password">
		                                    <span class="errorMsg" id="errPasswordId"></span>
		                                </div>
                                     </div>
                                     
                                     <div class="col-md-6">
                                          <div class="form-group">
		                                    <label for="password-login">Confirm Password</label>
		                                    <input type="password" autocomplete="off" class="form-control" id="cnfpassword">
		                                    <span class="errorMsg" id="errCnfPasswordId"></span>
		                                </div>
                                     </div>
                                     
                               </div>


                        </form>
								
								
	                           <p class="text-center">
	                               <button class="btn btn-template-main" onclick="regPopUpReg()"><i class="fa fa-sign-in"></i>Register</button>
	                           </p>
	                           
	                           
                        <p class="text-center text-muted"><a href="#" onclick="showLoginDiv()"><strong>Login</strong></a>If you have already have an account !</p>

                    </div>
                    
                </div>
                
            </div>
            
            
    </div>
    
   <!-- End of Pop Up div -->
   
   
   
    