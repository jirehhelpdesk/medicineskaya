

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript" src="static/paging/pageingScript.js"></script> 


        <div class="row">
            <div class="col-xs-12">
              <div class="box" style="background-color:#fff ! important;">
                
                
                <div class="box-header">
                  <h3 class="box-title">Opinion Report</h3>
                  <div class="box-tools">
                    <div class="input-group">                      
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default" style="float:right;" onclick="downloadReport()"><i class="fa fa-file-excel-o"></i> &nbsp; Export Report</button>
                      </div>
                    </div>
                  </div>
                </div><!-- /.box-header -->
                
                
                
                
               <div class="box-body table-responsive no-padding">
                  
                 <c:if test="${!empty recordDetails}">  
                  
                  
		                  <table id="tablepaging" class="table table-hover">
		                    <tr>
		                      <th>Sl No</th>                      
		                      <th>Treatment on</th>
		                      <th>Doctor</th>
		                      <th>Appointment Date</th>
		                      <th>Full Name</th>
		                      <th>Email id</th>
		                      <th>Phone</th>
		                      <th>Contact Date</th>
		                      <th>Manage</th>
		                    </tr>
		                    
		                     <tbody  id="myTableBody">  
				                    <%int i=1; %>
				                    <c:forEach items="${recordDetails}" var="list">	
					                    
					                   <tr>
					                      
					                      <td><%=i++%></td>
					                      <td>${list.treatment_on}</td>
					                      <td>${list.doctor_name}</td>
					                      <td><fmt:formatDate pattern="dd/MM/yyyy" value="${list.appointment_date}" /></td>
					                      <td>${list.user_name}</td>
					                      <td>${list.user_emailId}</td>
					                      <td>${list.user_phone}</td>
					                      <td><fmt:formatDate pattern="dd/MM/yyyy" value="${list.cr_date}" /></td>
					                      
					                      <td><button class="btn btn-primary" style="padding: 1px 3px ! important;" onclick="viewMessage('${list.opinion_id}','Opinion')">view</button></td>
					                      
				                       </tr>
				                       
				                       
				                   </c:forEach> 
				                   
		                    </tbody>
		                    
		                  </table>
		                  
		                  	<!-- Paging zone -->
          							
				     			<div id="pageNavPosition" style="padding-top: 20px;float: right;" align="center">
								</div>
								<script type="text/javascript">
								
								var pager = new Pager('tablepaging', 10);
								pager.init();
								pager.showPageNav('pager', 'pageNavPosition');
								pager.showPage(1);
								</script>
				                          
			                 
		                   <!-- End of  Paging zone -->
		                  
                  
                  </c:if>
                  
                  <c:if test="${empty recordDetails}">  
                  
                  		<div class="modal-content">
				                  
		                  <div class="modal-body">
		                    <p>No record was found as per your criteria.</p>
		                  </div>
		                  
		                </div><!-- /.modal-content -->
		                
                  </c:if>
                  
                </div><!-- /.box-body -->
                
                
                
                
                
              </div><!-- /.box -->
            </div>
          </div>
          
          
   