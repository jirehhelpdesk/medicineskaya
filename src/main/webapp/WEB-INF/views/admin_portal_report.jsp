<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Portal Report</title>


   <%@include file="admin_header_css.jsp" %>
  
   <link rel="stylesheet" type="text/css" href="static/datePicker/jquery.datetimepicker.css"/>
  
</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<body class="hold-transition skin-blue sidebar-mini" onload="activeSideMenu('menu5','0')">


<div class="wrapper">

  
  
  <%@include file="admin_header_bar.jsp" %>
  
  
  
  
  <%@include file="admin_sidemenu_bar.jsp" %>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    
    
    <section class="content-header">
      <h1>
        User Data
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User Record</li>
      </ol>
    </section>

   
   
   
   
    <!-- Main content -->
    <section class="content">
     
     <div class="row">
            
            <!-- left column -->
            
            <div class="col-md-2">
            
            </div>
            
            
            <!-- right column -->
            
            <div class="col-md-8">
              <!-- general form elements disabled -->
              
              
              <!-- Input addon -->
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Search user</h3>
                </div>
                
                
                <div class="box-body">
                 
                 <form id="reportDetailForm" >
                  
		              <c:forEach items="${adminData}" var="list">
		                  
		                  
		                  <div class="row">
	                                    	
                                <div class="col-md-12">
                                       <div class="input-group">
						                    <span class="input-group-addon"><i class="fa fa-list-alt"></i></span>
						                    <select name="report_type" id="report_type"  class="form-control" onchange="reportNotifyMessage(this.value)">
						                    		 <option value="Select report type">Select report type</option>
						                    		 <option value="Second opinion">Second opinion</option>
						                    		 <option value="User List">User List</option>
						                    		 <option value="Contact us">Contact us</option>		                   		 
						                    </select>
						                    <span class="errorMsg" id="errReportType"></span>
						              </div>
                               </div>
                                     
                          </div>
                          
                          <div class="row" id="rowDiv" style="display:none;">
	                                    	
                                <div class="col-md-12">
                                       <div class="input-group">
						                    
						                    <span style="color:green;margin-left:10px;"><i class="fa fa-info-circle"></i> &nbsp; <i id="notifyId"> </i></span>
	                             
						              </div>
                               </div>
                                     
                          </div>
                          
                          <div class="row">
	                            
	                                   	
                                <div class="col-md-6">
                                       <div class="input-group">
					                     <span class="input-group-addon"><i class="fa fa-calendar"></i></span>		                  
					                     <input class="some_class form-control" name="from_date" id="some_class_1" type="text" placeholder="from date">
					                     <span class="errorMsg" id="errFromDate"></span>
					                  </div>
                               </div>
                               
                               <div class="col-md-6">
                                      <div class="input-group">
					                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>		                  
					                    <input class="some_class form-control" name="to_date" id="some_class_2" type="text" placeholder="to date">
					                    <span class="errorMsg" id="errToDate"></span>
					                  </div>
                               </div>
                                     
                          </div>
                          
                            
		                  
		                  
		             </c:forEach>
		                  
                  </form> 
                  
                  	
						  <div class="box-footer" style="float:right;margin-top:10px;">
		                    <button  class="btn btn-primary" onclick="retrivePortalRecord()">Submit</button>
		                  </div>
		                  
		                  
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              
              
            </div><!--/.col (right) -->
            
            
            <div class="col-md-2">
            
            </div>
            
            
          </div>   <!-- /.row -->
          
          
          
          
          
             
          <div class="row" id="messageDivId"> </div><!-- /.row -->
       
       
       
          
          <div id="portReportCard"><%--  <%@include file="admin_user_report.jsp" %>  --%></div>
          
          
      
     		
          


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
 
   <script src="static/datePicker/jquery.js"></script>
	<script src="static/datePicker/jquery.datetimepicker.js"></script>
	<script>
	
	$('.some_class').datetimepicker();
	
	$('#some_class3').datetimepicker();
	
	/* $('#datetimepicker_dark').datetimepicker({theme:'dark'}) */
		
	</script>
	
	
	
  <%@include file="admin_footer.jsp" %>

 
 
</div>
<!-- ./wrapper -->



  <%@include file="admin_script.jsp" %>

</body>


</html>