<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Services</title>

    <%@include file="index_header_css.jsp" %>
    
    
</head>
<body onload="activeHeaderMenu('menu3')">


<div id="all">
        
        
        <%@include file="index_header_menu.jsp" %>
        

        
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-7">
                        <h1>Services</h1>
                    </div> -->
                    <div class="col-md-12">
                        <ul class="breadcrumb" style="padding: 2px 0;margin-bottom:5px;">
                            <li>Home</li>
                            <li>Services</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        
        
        <div id="content">
            <div class="container">

                <section>
                    
                    <div class="row">
                        
                        
                        
                        
                        <div class="col-md-3">
                        
                        <!-- *** MENUS AND WIDGETS *** -->

                            <%@include file="index_side_search_div.jsp" %>


                        <!-- *** MENUS AND FILTERS END *** -->
                    </div>
                    
                     
                     
                     
                     <div class="col-md-9 rightContentDiv" id="categoryResultHeading">

                          
				                <section>
				
				                    <div class="row">
				                        <div class="col-md-12">
				                            <div class="heading">
				                                <h2>Our main services</h2>
				                            </div>
				
				                            <p class="lead"></p>
				                        </div>
				                    </div>
				
				                    <div class="row services">
				                        
				                        <div class="col-md-6">
				                            <div class="box-simple">
				                                
				                                <div class="icon" style="border-color: #fff;margin-right:24px;cursor: pointer;" onclick="redirectUrl('touristPlaces')"> 
				                                    <!-- <i class="fa fa-desktop" style="line-height:4;"></i> -->
				                                    <img style="margin-right:10px;" src="static/indexResources/common_images/holiday 1.png" />
				                                </div>
				                                
				                                <div style="margin-top:44px;">
						                                
						                                <h3 style="color: #38a7bb;cursor: pointer;margin-left: 25px;" onclick="redirectUrl('touristPlaces')">Tourism</h3>
						                                 
														<p>	India has a diverse culture, and includes the Hindu pilgrimage centers of
															Char Dham, Haridwar, Mathura, Allahabad and Varanasi, the Buddhist
															Mahabodhi Temple, the Sikh Golden Temple in Amritsar, Punjab. It houses
															the world heritage sites such as the Valley of flowers, Qutab Minar, Taj
															Mahal etc. Folk dances like the bhangra of the Punjab, rouf and bhand
															pather of Kashmir are quite famous. Jammu and Kashmir is known as the
															paradise of the India. The states Uttarakhand and Himachal Pradesh houses
															the famous hill stations like Kullu, Manali, Shimla, Dharamsala, Palampur,
															Mussoorie, Dehradun, Nainital etc.</p>
				                                </div>
				                                
				                            </div>
                        				</div>
                        
				                        <div class="col-md-6">
				                            <div class="box-simple">
				                                
				                                <div class="icon" style="border-color: #fff;margin-right:56px;cursor: pointer;" onclick="redirectUrl('recomendDoctor')">
				                                    <!-- <i class="fa fa-print" style="line-height:4;"></i> -->
				                                    <img style="margin-right:10px;" src="static/indexResources/common_images/Hospital.png" />
				                                </div>
				                                
				                                <div style="margin-top:44px;" >
				                                
				                                		<h3 style="color: #38a7bb;cursor: pointer;" onclick="redirectUrl('recomendDoctor')">Specialist</h3>
				                                        <p>We have partnered with best specialist Doctors, high
															quality hospitals and professionals to earn customer's trust
															and build reputation. We have been gaining ample experience in
															Medical Tourism sector with a great satisfaction among our
															international customers. The positive feedback has always
															encouraged us to expand our outreach to serve more number of
															patients who are in need of right medical care solutions.</p>
				                                </div>
				                                
				                            </div>
				                        </div>
				                        
				                       
				                    </div>
				                    
				                    
				                    
				                    
				                    <div class="row services">
				                        
				                        <div class="col-md-12">
				                            <div class="box-simple">
				                                
				                                <div class="icon" style="border-color: #fff;margin-right:74px;cursor: pointer;" onclick="redirectUrl('specialization')"> 
				                                    <!-- <i class="fa fa-desktop" style="line-height:4;"></i> -->
				                                    <img style="margin-right:10px;" src="static/indexResources/common_images/mt1.png" />
				                                </div>
				                                
				                                <div style="margin-top:44px;" >
				                                
				                                		<h3 style="color: #38a7bb;cursor: pointer;" >Medical Tourism</h3>

														<p>Medical tourism refers to people traveling to a country
															other than their own to obtain medical treatment. In the past
															this usually referred to those who traveled from
															less-developed countries to major medical centers in highly
															developed countries for treatment unavailable at home.Medical
															tourism most often is for surgeries (cosmetic or otherwise)
															or similar treatments, though people also travel for dental
															tourism or fertility tourism.People with rare genetic
															disorders may travel to countries where the treatment is
															better understood. However, almost all types of health care
															are available, including psychiatry, alternative medicine,
															convalescent care, and even burial services.</p>
				
													</div>
				                                
				                            </div>
				                        </div>
				                        
				                        
				                    </div>
				                    
				                    
				                    
				                    <div class="row services">
				                        
				                        <div class="col-md-6">
				                            <div class="box-simple">
				                                
				                                <div class="icon" style="border-color: #fff;margin-right:56px;cursor: pointer;" >
				                                    <!-- <i class="fa fa-print" style="line-height:4;"></i> -->
				                                    <img style="margin-right:10px;" src="static/indexResources/common_images/travel.png" />
				                                </div>
				                                
				                                <div style="margin-top:44px;" >
				                                
				                                		<h3 style="color: #38a7bb;cursor: pointer;" >Local Travel</h3>
														<p>All your travel needs will be taken care of by us
															starting from Visa to your local conveyance; airport pickup
															and drop etc. In case of any local sightseeing or visit to
															any nearby tourist places of interest can also be arranged as
															per your budget.</p>
													</div>
				                                
				                            </div>
				                        </div>
				                        
				                        
				                        
				                        <div class="col-md-6">
				                            <div class="box-simple">
				                                
				                                <div class="icon" style="border-color: #fff;margin-right:56px;cursor: pointer;" >
				                                    <!-- <i class="fa fa-print" style="line-height:4;"></i> -->
				                                    <img style="margin-right:10px;" src="static/indexResources/common_images/accomodation1.png" />
				                                </div>
				                                
				                                <div style="margin-top:44px;" >
				                                
				                                		<h3 style="color: #38a7bb;cursor: pointer;" >Accomodation</h3>
														<p>Where do you want to stay? Just tell us and we take
															care. Be it Star Hotel or Guest House, we have it all and
															would be glad to help you at no extra cost.</p>
													</div>
				                                
				                            </div>
				                        </div>
				                        
				                        
				                        
				                        
				                    </div>
				
				                    
				
				                </section>
				
				                
                      </div>
                      
                      
                    </div>

                    
                    
                </section>
                
            </div>
            <!-- /#contact.container -->

            
           

            

            <%@include file="index_hospital.jsp" %>
            
            

            
			<div id="get-it" style="padding: 0 0 3px ! important;">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                   
                </div>
                <div class="col-md-4 col-sm-12">
                
                </div>
            </div>
        </div>
			
			

        </div>
        <!-- /#content -->

        

         <%@include file="index_footer.jsp" %>


    </div>
    <!-- /#all -->

    
    <%@include file="index_common_script.jsp" %>
    

</body>
</html>