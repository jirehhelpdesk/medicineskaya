<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>



 
        <!-- *** FOOTER *** -->

        
        <footer id="footer">
            <div class="container">
               
                <div class="col-md-6 col-sm-6">

                    <h4>Contact</h4>

                    <p><strong>MedicinesKaya India.</strong>
                        <br>India Office
                        <br>HO:- 42, Vishal, Vasundhara Enclave, Delhi, India
						<br>Mobile (India)  :  +91 91024 81133 , +91 98733 49502 , 						
                        <br>+91 99101 50660, +91 7290818133
                        <br>                      
                    </p>

                    <a href="contactUs" class="btn btn-small btn-template-main">Go to contact page</a>

                    <hr class="hidden-md hidden-lg hidden-sm">

                </div>
               
               

                <div class="col-md-6 col-sm-6" >

                    <h4 style="text-align: right;">Social Media</h4>

                    <div class="photostream" style="float:right;">
                    	
                    	<div class="social" >
                    	   
	                    	   <ul>
	                    	       <li><a target="_" href="http://www.facebook.com" class="external facebook" data-animate-hover="pulse"><i class="fa fa-facebook-square"></i>facebook</a>
	                               </li>
	                               
	                               <li><a target="_" href="http://www.googleplus.com" class="external gplus" data-animate-hover="pulse"><i class="fa fa-google-plus-square"></i>google+</a>
	                               </li>
	                               
	                               <li><a target="_" href="http://www.twitter.com" class="external twitter" data-animate-hover="pulse"><i class="fa fa-twitter-square"></i>twitter</a>
	                               </li>
	                               
	                               <li><a target="_" href="http://www.linkedin.com" class="email" data-animate-hover="pulse"><i class="fa fa-linkedin-square"></i>linkedin</a>
	                               </li>
	                          </ul>
	                          
	                          
                         </div>
                            
                      
                    </div>
					
                
                </div>
                <!-- /.col-md-3 -->
            </div>
            <!-- /.container -->
        </footer>
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->

        <!-- *** COPYRIGHT *** -->

        <div id="copyright">
            <div class="container">
                <div class="col-md-12">
                    <p class="pull-left">&copy; 2017. MedicinesKaya</p>
                    <p class="pull-right">Design & Developed by <a target="_" href="http://www.jirehsol.co.in/">Jireh</a> 
                        
                    </p>

                </div>
            </div>
        </div>
        <!-- /#copyright -->

        <!-- *** COPYRIGHT END *** -->



</body>
</html>