	 <!-- Favicon and apple touch icons-->
    <link rel="shortcut icon" href="static/indexResources/img/favicon.ico" type="image/x-icon" />
    
    <!-- owl carousel css -->
    
    
    <link href='static/indexResources/bootStrap_resources/font-style.css' rel='stylesheet' type='text/css'>

    <!-- Bootstrap and Font Awesome css -->
    <!-- <link rel="stylesheet" href="static/indexResources/bootStrap_resources/font-awesome.min.css"> -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
   <!--  <link rel="stylesheet" href="static/indexResources/bootStrap_resources/bootstrap.min.css"> -->

    <!-- Css animations  -->
    <link href="static/indexResources/css/animate.css" rel="stylesheet">

    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href="static/indexResources/css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes -->
    <link href="static/indexResources/css/custom.css" rel="stylesheet">

    <!-- Responsivity for older IE -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

   

    <link href="static/indexResources/css/owl.carousel.css" rel="stylesheet">
    <link href="static/indexResources/css/owl.theme.css" rel="stylesheet">
    