<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Home ::</title>

    <%@include file="index_header_css.jsp" %>
    
</head>


<body onload="activeHeaderMenu('menu1')">

    <div id="all">

        
        <%@include file="index_header_menu.jsp" %>
        
        
        
        
        <!-- Start menu section -->
	   			<%@include file="index_slider.jsp" %> 
	  	<!-- End menu section -->
  
        
        
        <section class="bar background-white" style=" padding:2px 0 ! important;margin-bottom:12px ! important; margin-top:2px;">
            
            <div class="container">
                <div class="col-md-12">


                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="box-simple">
                                
                                <div class="icon" style="border-color: #fff;margin-right:24px;cursor: pointer;" onclick="redirectUrl('touristPlaces')"> 
                                    <!-- <i class="fa fa-desktop" style="line-height:4;"></i> -->
                                    <img style="margin-right:10px;" src="static/indexResources/common_images/holiday 1.png" />
                                </div>
                                
                                <div style="margin-top:44px;">
		                                
		                                <h3 style="color: #38a7bb;cursor: pointer;margin-left: 25px;" onclick="redirectUrl('touristPlaces')">Tourism</h3>
		                               
										<p>	India has a diverse culture, and includes the Hindu pilgrimage centers of
											Char Dham, Haridwar, Mathura, Allahabad and Varanasi, the Buddhist
											Mahabodhi Temple, the Sikh Golden Temple in Amritsar, Punjab. It houses
											the world heritage sites such as the Valley of flowers, Qutab Minar, Taj
											Mahal etc. Folk dances like the bhangra of the Punjab, rouf and bhand
											pather of Kashmir are quite famous. Jammu and Kashmir is known as the
											paradise of India. The states Uttarakhand and Himachal Pradesh houses
											the famous hill stations like Kullu, Manali, Shimla, Dharamsala, Palampur,
											Mussoorie, Dehradun, Nainital etc.</p>
		                                
                                </div>
                                
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="box-simple">
                                
                                <div class="icon" style="border-color: #fff;margin-right:56px;cursor: pointer;" onclick="redirectUrl('recomendDoctor')">
                                    <!-- <i class="fa fa-print" style="line-height:4;"></i> -->
                                    <img style="margin-right:10px;" src="static/indexResources/common_images/Hospital.png" />
                                </div>
                                
                                <div style="margin-top:44px;" >
                                
                                		<h3  style="color: #38a7bb;cursor: pointer;" onclick="redirectUrl('recomendDoctor')">Specialist</h3>
										<p>We have partnered with best specialist Doctors, high
											quality hospitals and professionals to earn customer's trust
											and build reputation. We have been gaining ample experience in
											Medical Tourism sector with a great satisfaction among our
											international customers. The positive feedback has always
											encouraged us to expand our outreach to serve more number of
											patients who are in need of right medical care solutions.</p>

							</div>
                                
                            </div>
                        </div>
                        
                    </div>

                </div>
            </div>
        </section>
        
        
        
        
        
        
        <section>
            <div class="container">

                <div class="row">

                    
                    
                    <div class="col-md-3">
                        <!-- *** MENUS AND WIDGETS *** -->

                        	
                        	<%@include file="index_side_search_div.jsp" %>


                        <!-- *** MENUS AND FILTERS END *** -->
                    </div>

                    
                    
                    
                    
                    
                    <div class="col-md-9 rightContentDiv" id="categoryResultHeading">

                          <div class="heading">
                              <h2>About Us</h2>
                          </div>

                          <p class="lead">Medical Tourism being an integral part of the
							world right now has been a key growth sector for more than a
							decade. As per experts' estimation, the volume of medical
							tourists worldwide could reach up to 5 million by 2016. Hosting
							about 1.27 million tourists India has been rapidly emerging as a
							wellness hub due to world class Medical Infrastructure at
							comparatively fractional cost. Health Tourism holds immense hope
							for India due to modern medical amenities, alternative systems of
							medical treatment, cost-effective treatment, strong pharma
							Industry, less language barrier. Hence, Various medical tourism
							companies which facilitate the healthcare of tourists in India
							are catering to their needs. <a href="aboutUs" style="float:right;">Know more</a></p> 


                      </div>
                </div>

            </div>
            <!-- /.container -->

        </section>
        
        
        
        <section class="bar background-pentagon no-mb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading text-center" ><!-- style="margin-bottom: 14px;text-align: left;" -->
                            <h2>Testimonials</h2>
                        </div>

                        <!-- *** TESTIMONIALS CAROUSEL *** -->

                        <ul class="owl-carousel testimonials same-height-row" > <!-- Remove the style when multiple data come -->
                            
                            <li class="item">
                                <div class="testimonial same-height-always">
                                    <div class="text">
                                        <p>"Very good service. I was very happy. Have got my father's treatment done without any
                                          problem"</p>
                                    </div>
                                    <div class="bottom">
                                        <div class="icon"><i class="fa fa-quote-left"></i>
                                        </div>
                                        <div class="name-picture">
                                            <img class="testimonialImage" alt="" src="static/indexResources/img/dummy.png">
                                            <h5><b>Raj Chhetri</b> </h5>
                                            <p style="text-align:unset;">Kathmandu</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            
                           
                            
                            <li class="item">
                                <div class="testimonial same-height-always">
                                    <div class="text">

									<p>"I thank Dr. Pandey for saving my life! He is the
										best! My special thanks to MedicinesKaya Team for being so
										caring & concerned. I just felt at home."</p>

								</div>
                                    <div class="bottom">
                                       <div class="icon"><i class="fa fa-quote-left"></i>
                                        </div>
                                        <div class="name-picture">
                                            <img class="testimonialImage" alt="" src="static/indexResources/img/patient_1.png">
                                            <h5><b>D. Jumaev</b></h5>
                                            <p style="text-align:unset;">Turkmenistan</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            
                            <li class="item">
                                <div class="testimonial same-height-always">
                                    <div class="text">

									<p>"It was a wonderful treatment experience. In my country,
										doctors said my case was very complex. Thanks to MedicinesKaya
										India that arranged my appointment with Dr. Pandey. He is one
										of the most prolific surgeons I have come across. He has given
										me a new lease of life."</p>

								</div>
                                    <div class="bottom">
                                        <div class="icon"><i class="fa fa-quote-left"></i>
                                        </div>
                                        <div class="name-picture">
                                            <img class="testimonialImage" alt="" src="static/indexResources/img/dummy.png">
                                            <h5><b>A. Jhorik </b></h5>
                                            <p style="text-align:unset;">Uzbekistan</p>
                                        </div>
                                        
                                    </div>
                                </div>
                            </li>
                            
                            <li class="item">
                                <div class="testimonial same-height-always">
                                    <div class="text">

									<p>"They are very helpful in all respects. Their people are
										very courteous. I did not have to face any obstacle for the
										treatment of my wife. Allah may give them all the support"</p>

								</div>
                                    <div class="bottom">
                                        <div class="icon"><i class="fa fa-quote-left"></i>
                                        </div>
                                        <div class="name-picture">
                                            <img class="testimonialImage" alt="" src="static/indexResources/img/dummy.png">
                                            <h5><b>Md Shamshul</b></h5>
                                            <p style="text-align:unset;">Bangladesh</p>
                                        </div>
                                        
                                    </div>
                                </div>
                            </li>
                            
                            <li class="item">
                                <div class="testimonial same-height-always">
                                    <div class="text">

									<p>"My mother had a knee replacement done which she was su
										ering from a long time. They have helped me in all respects. I
										wish them all the success in the future"</p>

								</div>
                                    <div class="bottom">
                                        <div class="icon"><i class="fa fa-quote-left"></i>
                                        </div>
                                        <div class="name-picture">
                                            <img class="testimonialImage" alt="" src="static/indexResources/img/dummy.png">
                                            <h5><b>Ekuwa</b></h5>
                                            <p style="text-align:unset;">Ghana</p>
                                        </div>
                                        
                                    </div>
                                </div>
                            </li>
                           
                            <li class="item">
                                <div class="testimonial same-height-always">
                                    <div class="text">

									<p>"My wife has been successfully treated. She had been
										suffering from a dangerous disease. Our family is feeling
										lucky. We were fortunate to get such a wonderful service from
										MedicinesKaya India."</p>

								</div>
                                    <div class="bottom">
                                        <div class="icon"><i class="fa fa-quote-left"></i>
                                        </div>
                                        <div class="name-picture">
                                            <img class="testimonialImage" alt="" src="static/indexResources/img/dummy.png">
                                            <h5><b>Joshua</b></h5>
                                            <p style="text-align:unset;">Nigeria</p>
                                        </div>
                                        
                                    </div>
                                </div>
                            </li>
                            
                        </ul>
                        <!-- /.owl-carousel -->

                        <!-- *** TESTIMONIALS CAROUSEL END *** -->
                    </div>

                </div>
            </div>
        </section>
        <!-- /.bar -->
        
        
        
        
        
         <%@include file="index_hospital.jsp" %>


        <!-- *** GET IT *** -->

        <div id="get-it" style="padding: 0 0 3px ! important;">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                   
                </div>
                <div class="col-md-4 col-sm-12">
                
                </div>
            </div>
        </div>


        <!-- *** GET IT END *** -->


        <%@include file="index_footer.jsp" %>
       

    </div>
    <!-- /#all -->



    <%@include file="index_common_script.jsp" %>



</body>



</html>