<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<body>



<div class="col-md-10" id="blog-listing-medium">
                 
            <% int i = 1;%>
                       
            <c:forEach items="${doctorDetails}" var="list">	 
                 
                 
                 <c:set var="fileName" value="${list.doctor_photo}"/>
                 <% String fileName = (String)pageContext.getAttribute("fileName"); %>


                <section class="post" style="border-bottom: 1px solid rgb(206, 206, 206); margin-bottom: 25px;" id="section<%=i%>">
                   
                    <div class="row" style="margin-bottom:10px;">
                        
                        <div class="col-md-3">
                            <div class="image imageOuterDiv">
                                <a href="#">  
                                	
                                	<img class="img-responsive categoryImgae" alt="<%=fileName%>" src="${pageContext.request.contextPath}<%="/showImages?fileName="+fileName+"&directoryType=Portal&subDirectory=PriorityDoctor"%>"   />				  
                                    
                                </a>
                            </div>
                        </div>
                        
                        <div class="col-md-9">
                            
                            <h2><a href="#">${list.doctor_name}</a></h2>
                            
                            <div class="clearfix">
                                <p class="author-category"> <span style="color:#38a7bb;font-weight:bold;">${list.doctor_degree}</span><%--  in <a href="#">${list.doctor_specialization}</a> --%>
                                </p>
                              
                            </div>
                             
	                        <h4>Contact Details</h4>
	                            
                            <div class="clearfix">
                                
                                <div class="row">
	                                 
	                                 <c:if test="${list.doctor_email != ''}">   
		                                 <div class="col-md-6">
	                                         <div class="form-group">
	                                            
	                                            <a href="mailto:${list.doctor_email}" class="email" data-animate-hover="pulse" style="opacity: 1;"><i class="fa fa-envelope"></i>${list.doctor_email}</a>
	                                            
			                                </div>
	                                     </div>    	
                                     </c:if>
                                     
                                     <c:if test="${list.doctor_phone != ''}">                                    
	                                     <div class="col-md-6">
	                                         <div class="form-group">
	                                         	
	                                            <span class="external phone" data-animate-hover="pulse" style="opacity: 1;color:#38a7bb;"><i class="fa fa-phone"></i>&nbsp; ${list.doctor_phone}</span>
	                                           
			                                </div>
	                                      </div>	                                      
                                      </c:if>
                                      
                                     <!-- LINKS  -->
                                     
                                     <c:if test="${list.doctor_website != ''}">
	                                     <div class="col-md-6">
	                                         <div class="form-group">
	                                         	
	                                            <a href="${list.doctor_website}" target="_" class="external link" data-animate-hover="pulse" style="opacity: 1;"><i class="fa fa-globe"></i>${list.doctor_website}</a>
	                                           
			                                </div>
	                                     </div>
                                     </c:if>
                                     
                                     <c:if test="${list.doctor_facebook != ''}">
	                                     <div class="col-md-6">
	                                         <div class="form-group">
	                                         	
	                                            <a href="${list.doctor_facebook}" target="_"  class="external facebook" data-animate-hover="pulse" style="opacity: 1;"><i class="fa fa-facebook"></i>${list.doctor_facebook}</a>
	                                            
			                                </div>
	                                     </div>
                                     </c:if>
                                     
                                     <c:if test="${list.doctor_skype != ''}">
	                                     <div class="col-md-6">
	                                         <div class="form-group">
	                                         	
	                                            <span  class="external skype" data-animate-hover="pulse" style="opacity: 1;color:#38a7bb;"><i class="fa fa-skype"></i>&nbsp; ${list.doctor_skype}</span>
	                                           
			                                </div>
	                                     </div>
                                      </c:if>
                                     
                                </div>
                                
		                            
                            </div>
                            
                            
                            <h4>About Doctor</h4>
                            
                            <div class="intro" id="aboutUs<%=i%>" style="height:183px;overflow:hidden;">
                    
                    			${list.about_doctor}
                    
                   			</div>
                    		
                    		<span id="seeMore<%=i%>" class="seemore" style="float:right;display:none;color:#38a7bb;cursor:pointer;" onclick="showMoreAboutUs('${list.priority_doctor_id}','<%=i%>')">See more</span>
                   
                        </div>
                        
                        
                    </div>
                    
                    
                </section>
              		
              		<% i = i + 1;%>
              		
              </c:forEach>
              
              
</div>
          
                    
                    
</body>
</html>