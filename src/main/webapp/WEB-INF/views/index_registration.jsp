<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registration</title>

    <%@include file="index_header_css.jsp" %>
    
    
</head>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<body onload="activeHeaderMenu('menu7')">


<div id="all">
        
        
        <%@include file="index_header_menu.jsp" %>
        

        
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-7">
                        <h1>Registration</h1>
                    </div> -->
                    <div class="col-md-12">
                        <ul class="breadcrumb" style="padding: 2px 0;margin-bottom:5px;">
                            <li>Home</li>
                            <li>Registration</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        
        
        
        
        <div id="content">
            <div class="container">

                <div class="row">
                    
                    <div class="col-md-6">
                        
                        <div class="box">
                            
                            <h2 class="text-uppercase"></h2>
							
								<img src="static/indexResources/img/1.png" style="width:409px;height:360px;"/>
								
	                            <!-- <p class="lead">Already our user?</p>
	                            <p class="text-muted">Please login with your credential and search or find the relevant things which you need in your journey to make a healthiest and safest tour.</p>
	
	                            
	                            <div class="text-center">
	                                <button style="margin-top: 10px;" class="btn btn-template-main" onclick="redirectUrl('signIn')"><i class="fa fa-sign-in"></i> Go to Sign In</button>
	                            </div> -->
	                        
	                        
                        </div>
                        
                    </div>
                    
                    
                    <div class="col-md-6">
                        <div class="box">
                            <h2 class="text-uppercase" style="margin-bottom: 8px; margin-top: 1px;">New account</h2>

                            <!-- <p class="lead" style="margin-bottom:10px;">Not our registered user yet?</p> -->
                            <p>Register with us and get safest and healthiest and much more opens to you ! The whole process will not take you more than a minute!</p>
                            
                            <hr>

                            <form:form  id="userRegForm" modelAttribute="userDetails" method="post" >
                                
                                <div class="row">
	                                    	
                                     <div class="col-md-12">
                                         <div class="form-group">
		                                    <label for="name-login">Name </label>
		                                    <input type="text" class="form-control" autocomplete="off" name="user_full_name" path="user_full_name" id="user_full_name">
		                                    <span class="errorMsg" id="errNameId"></span>
		                                </div>
                                     </div>
                                     
                               </div>
                               
                               <div class="row">
	                                    	
                                     <div class="col-md-6">
                                         <div class="form-group">
		                                    <label for="email-login">Email </label>
		                                    <input type="text" class="form-control" autocomplete="off" name="user_emailid" path="user_emailid" id="user_emailid">
		                                    <span class="errorMsg" id="errEmailId"></span>
		                                </div>
                                     </div>
                                     
                                     <div class="col-md-6">
                                         <div class="form-group">
		                                    <label for="email-login">Mobile no </label>
		                                    <input type="text" class="form-control" autocomplete="off" name="user_mobile" path="user_mobile" id="user_mobile">
		                                    <span class="errorMsg" id="errMobileId"></span>
		                                </div>
                                     </div>
                                     
                               </div>
                               
                               
                               <div class="row">
	                                    	
                                     <div class="col-md-6">
                                         <div class="form-group">
		                                    <label for="email-login">Country </label>
		                                    <select class="form-control" name="user_country" path="user_country" id="user_country" onchange="showStateNames(this.value)">
		                                    			<option value="Select Country">Select Country</option>
		                                    			
		                                    			<c:forEach items="${countryList}" var="list">	
		                                    					<option value="${list.country_name}">${list.country_name}</option>
		                                    			</c:forEach>
		                                    			
		                                    </select>
		                                    <span class="errorMsg" id="errCountryId"></span>
		                                </div>
                                     </div>
                                     
                                     <div class="col-md-6">
                                         <div class="form-group">
		                                    <label for="email-login">State</label>
		                                    <select class="form-control" name="user_state" disabled path="user_state" id="user_state" >
		                                    			<option value="Select State">Select State</option>
		                                    </select>
		                                    <span class="errorMsg" id="errStateId"></span>
		                                </div>
                                     </div>
                                     
                               </div>
                               
                               <div class="row">
	                                    	
                                     <div class="col-md-6">
                                         <div class="form-group">
		                                    <label for="password-login">Password </label>
		                                    <input type="password" class="form-control" autocomplete="off" name="user_password" path="user_password" id="user_password">
		                                    <span class="errorMsg" id="errPasswordId"></span>
		                                </div>
                                     </div>
                                     
                                     <div class="col-md-6">
                                          <div class="form-group">
		                                    <label for="password-login">Confirm Password </label>
		                                    <input type="password" class="form-control" id="cnfpassword" autocomplete="off">
		                                    <span class="errorMsg" id="errCnfPasswordId"></span>
		                                </div>
                                     </div>
                                     
                               </div>
	                                  
                                
                            </form:form>
                            	
                            	<div class="text-center">
                                    <button style="margin-top: 10px;" class="btn btn-template-main" onclick="saveUserRegistrartion()"><i class="fa fa-user-md"></i> Register</button>
                                </div>
                           
                        </div>
                    </div>

                    

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
        
        

        

         <%@include file="index_footer.jsp" %>


    </div>
    <!-- /#all -->

    
    <%@include file="index_common_script.jsp" %>
    

</body>
</html>