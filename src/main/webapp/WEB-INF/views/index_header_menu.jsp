       
       
       
       <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
       
       <header>

            <!-- *** NAVBAR *** -->

            <div class="navbar-affixed-top" data-spy="affix" data-offset-top="200">

                <div class="navbar navbar-default yamm" role="navigation" id="navbar">

                    <div class="container">
                        
                        
                        <div class="navbar-header">

                            <a class="navbar-brand home" href="index" style="margin-left:-3px;">
                                <!-- <i class="hidden-xs hidden-sm">MedicinesKaya</i> -->
                                <img src="static/indexResources/img/MedicinesKaya_India_Logo.png" alt="Universal logo" class="hidden-xs hidden-sm" width="140px" height="59px">
                                <!-- <img src="static/indexResources/img/MedicinesKaya_India_Logo.png" alt="Universal logo" class="visible-xs visible-sm" width="140px" height="52px"><span class="sr-only"><i class="visible-xs visible-sm">MedicinesKaya</i> - go to homepage</span> -->
                            </a>
                            <div class="navbar-buttons">
                                <button type="button" class="navbar-toggle btn-template-main" data-toggle="collapse" data-target="#navigation">
                                    <span class="sr-only">Toggle navigation</span>
                                    <i class="fa fa-align-justify"></i>
                                </button>
                            </div>
                        </div>
                        
                        <!--/.navbar-header -->

                        <div class="navbar-collapse collapse" id="navigation">

                           <div id="headerDiv">
                            
	                            
	                            <ul class="nav navbar-nav navbar-right">
	                                
	                                <li class="dropdown" id="menu1">
	                                    <a href="index" class="dropdown-toggle">Home</a>                                   
	                                </li>
	                                
	                                <li class="dropdown use-yamm yamm-fw" id="menu2">
	                                    <a href="aboutUs" class="dropdown-toggle" >About Us</a>                                    
	                                </li>
	                               
								    <li class="dropdown use-yamm yamm-fw" id="menu3">
	                                    <a href="services" class="dropdown-toggle">Services</a>                                   
	                                </li>
	                               
	                                <li class="dropdown use-yamm yamm-fw" id="menu4">
	                                    <a href="specialization" class="dropdown-toggle">Specialization</a>                                   
	                                </li>
	                                
	                                <li class="dropdown use-yamm yamm-fw" id="menu5">
	                                    <a href="secOpinion" class="dropdown-toggle">Second Opinion</a>                                   
	                                </li>
	                                
	                                <c:if test="${empty userBean}">
	                                
		                                <li class="dropdown use-yamm yamm-fw" id="menu6">
		                                    <a href="signIn" class="dropdown-toggle">Sign in</a>                                   
		                                </li>
		                                
		                                 <li class="dropdown use-yamm yamm-fw" id="menu7">
		                                    <a href="registration" class="dropdown-toggle">Sign up</a>                                   
		                                </li>
	                               
	                               </c:if>
	                               
	                               <c:if test="${!empty userBean}">
	                               
	                               			
	                               		<li class="dropdown" id="menu8">
		                                    <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown">${userBean.user_full_name} <b class="caret"></b></a>
		                                    <ul class="dropdown-menu">
		                                        <li><a href="changePassword">Change Password</a></li>
		                                        <li><a href="userLogout">Sign out</a></li>	                                        
		                                    </ul>
	                                   </li>	
	                               
	                               </c:if>
	                               
	                                <li style="margin-top:25px;">
          				                    <div id="google_translate_element"></div><script type="text/javascript">
											function googleTranslateElementInit() {
											  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
											}
											</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
											        

                                   </li>
	                               
	                            </ul>
	                            
                            
                            </div> 
                            

                        </div>
                        <!--/.nav-collapse -->


                    </div>


                </div>
                <!-- /#navbar -->

            </div>

            <!-- *** NAVBAR END *** -->

        </header>
        
        
        
        
        
        