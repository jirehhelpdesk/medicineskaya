<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sign In</title>

    <%@include file="index_header_css.jsp" %>
    
    
</head>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<body onload="activeHeaderMenu('menu6')">


<div id="all">
        
        
        <%@include file="index_header_menu.jsp" %>
        

        
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-7">
                        <h1>Sign In</h1>
                    </div> -->
                    <div class="col-md-12">
                        <ul class="breadcrumb" style="padding: 2px 0;margin-bottom:5px;">
                            <li>Home</li>
                            <li>Sign in</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        
        
        
        
        <div id="content">
            <div class="container">

                <div class="row">
                    <div class="col-md-6">
                        <div class="box">
                            <!-- <h2 class="text-uppercase">Sign in with your credential</h2> -->
							
							<img src="static/indexResources/img/2.png" style="width:409px;height: 324px;"/>
								
                            <!-- <p class="lead">Not our registered user yet?</p>
                            <p>With registration with us new safe world of health, get safest and healthiest and much more opens to you! The whole process will not take you more than a minute!</p>
                            <p class="text-muted">If you have any questions, please feel free to <a href="contactUs">contact us</a>, our customer service center is working for you 24/7.</p>
							
							
							<div class="text-center">
                                <button class="btn btn-template-main" onclick="redirectUrl('registration')"><i class="fa fa-user-md"></i> Go to Sign Up</button>
                            </div> -->
                          
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="box">
                            <h2 class="text-uppercase" style="margin-bottom: 8px; margin-top: 1px;">Sign In</h2>

                            <p class="lead" style="margin-bottom:10px;">Already our user?</p>
                            <p class="text-muted">Please login with your credential and search or find the relevant things which you need in your journey to make a healthiest and safest tour.</p>

                            <hr>
							
							<% String userName = (String)request.getAttribute("userName");%> 
        					
        					
        					<% String actionMsg = (String)request.getAttribute("actionMessage");
			                            			
                           			if(actionMsg.equals("Deactive"))
                           			{
                           				%>
                           				<span class="alertMsg">Your account is blocked,Please contact to support team !</span>
                           				<%
                           			}else if(actionMsg.equals("Invalid"))
                           			{
                           		     %>
                           				<span class="alertMsg">Given credential was invalidate.</span>
                           			                           		
                           		  <%}else{ %>
                           		  
                           		       
                           		  <%} %>
			                   
                            <form:form class="login-form"  modelAttribute="userDetails" method="post" action="authUserCredential" >
						                    
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" autocomplete="off" name="user_emailid" path="user_emailid" id="user_emailid">
                                </div>
                                
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" autocomplete="off" name="user_password" path="user_password" id="user_password">
                                </div>
                                
                                <div class="form-group" style="float:right;">
                                    <a href="forgetPassword"> forget password !</a>
                                </div>
                                
                                <div class="text-center">
                                    <button type="submit" style="margin-top:10px;margin-left: 100px;" class="btn btn-template-main"><i class="fa fa-sign-in"></i> Log in</button>
                                </div>
                            
                            </form:form>
                            
                        </div>
                    </div>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
        
        

        

         <%@include file="index_footer.jsp" %>


    </div>
    <!-- /#all -->

    
    <%@include file="index_common_script.jsp" %>
    

</body>
</html>