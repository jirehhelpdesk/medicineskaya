<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Specialization</title>

    <%@include file="index_header_css.jsp" %>
    
    
</head>
<body onload="activeHeaderMenu('menu4')">


<div id="all">
        
        
        <%@include file="index_header_menu.jsp" %>
        

        
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-7">
                        <h1>Specialization</h1>
                    </div> -->
                    <div class="col-md-12">
                        <ul class="breadcrumb" style="padding: 2px 0;margin-bottom:5px;">
                            <li>Home</li>
                            <li>Specialization</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        
        
        <div id="content">
            <div class="container">

                <section>
                    
                    <div class="row">
                        
                        
                        
                        
                        <div class="col-md-3">
                        <!-- *** MENUS AND WIDGETS *** -->

                             <%@include file="index_side_search_div.jsp" %>


                        <!-- *** MENUS AND FILTERS END *** -->
                    </div>
                    
                     
                     
                     
                     <div class="col-md-9 rightContentDiv" id="categoryResultHeading">

                          <div class="heading">
                              <h2>Specialization</h2>
                          </div>
						
						  
						 <div class="panel-group accordion" id="accordionThree">
                            
                            
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title accordianHead">

                                            <a data-toggle="collapse" data-parent="#accordionThree" href="#collapse3a">
												Oncology
                                            </a>

                                        </h4>
                                </div>
                                <div id="collapse3a" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <!-- <div class="col-md-4">
                                                <img src="img/template-easy-customize.png" alt="" class="img-responsive">
                                            </div> -->
                                            <div class="col-md-12">
												<p>Oncology is a branch of medicine that deals with the
													prevention, diagnosis and treatment of cancer. A medical
													professional who practices oncology is an oncologist.
													The name's etymological origin is the Greek word, meaning
													"tumor", "volume" or "mass".</p>
												
												<p>The three components which have improved survival in cancer are:</p>
												
												<p>
												   <ul>
												   	    <li>Prevention - This is by reduction of risk factors like tobacco and alcohol consumption</li>
												   	    <li>Early diagnosis - Screening of common cancers and comprehensive diagnosis and staging</li>
												   	    <li>Treatment - Multimodality management by discussion in tumor board and treatment in a comprehensive cancer centre.</li>
												   </ul>
											    </p>				
    
												<p>Cancers are often managed through discussion on
													multi-disciplinary cancer conferences where medical
													oncologist, surgical oncologist, radiation oncologist,
													pathologist, radiologist and organ specific oncologists
													meet to find the best possible management for an individual
													patient considering the physical, social, psychological,
													emotional and financial status of the patients. It is very
													important for oncologists to keep updated of the latest
													advancements in oncology, as changes in management of
													cancer are quite common. All eligible patients in whom
													cancer progresses and for whom no standard of care
													treatment options are available should be enrolled in a
													clinical trial.</p>

											</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title accordianHead">

                                            <a data-toggle="collapse" data-parent="#accordionThree" href="#collapse3b">
												Orthopaedics
                                            </a>

                                        </h4>
                                </div>
                                <div id="collapse3b" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <!-- <div class="col-md-4">
                                                <img src="img/template-easy-code.png" alt="" class="img-responsive">
                                            </div> -->
                                            <div class="col-md-12">

												<p>Orthopedic surgery or orthopedics (alternatively
													spelled orthopaedic surgery and orthopaedics) is the branch
													of surgery concerned with conditions involving the
													musculoskeletal system. Orthopedic surgeons use both
													surgical and nonsurgical means to treat musculoskeletal
													trauma, spine diseases, sports injuries, degenerative
													diseases, infections, tumors, and congenital disorders.</p>

											</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title accordianHead">

                                            <a data-toggle="collapse" data-parent="#accordionThree" href="#collapse3c">
												Heart
                                            </a>

                                        </h4>
                                </div>
                                <div id="collapse3c" class="panel-collapse collapse">
                                    <div class="panel-body">

										<p>Cardiovascular (heart) surgery is surgery on the heart
											or great vessels performed by cardiac surgeons. Frequently,
											it is done to treat complications of ischemic heart disease
											(for example, coronary artery bypass grafting), correct
											congenital heart disease, or treat valvular heart disease
											from various causes including endocarditis, rheumatic heart
											disease and atherosclerosis. It also includes heart
											transplantation.</p>

									</div>
                                </div>
                            </div>
                            
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title accordianHead">

                                            <a data-toggle="collapse" class="" data-parent="#accordionThree" href="#collapse4c">
												Neurology
                                            </a>

                                        </h4>
                                </div>
                                <div id="collapse4c" class="panel-collapse collapse">
                                    <div class="panel-body">

										<p>Neurology is a branch of medicine dealing
											with disorders of the nervous system. Neurology deals with
											the diagnosis and treatment of all categories of conditions
											and disease involving the central and peripheral nervous
											system (and its subdivisions, the autonomic nervous system
											and the somatic nervous system); including their coverings,
											blood vessels, and all effector tissue, such as muscle.
											Neurological practice relies heavily on the field of
											neuroscience, which is the scientific study of the nervous
											system.</p>

									</div>
                                </div>
                            </div>
                            
                            
                            
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title accordianHead">

                                            <a data-toggle="collapse" data-parent="#accordionThree" href="#collapse5c">
												Bariatric
                                            </a>

                                        </h4>
                                </div>
                                <div id="collapse5c" class="panel-collapse collapse">
                                    <div class="panel-body">

										<p>Bariatric surgery (weight loss surgery) includes a
											variety of procedures performed on people who have obesity.
											Weight loss is achieved by reducing the size of the stomach
											with a gastric band or through removal of a portion of the
											stomach (sleeve gastrectomy or biliopancreatic diversion with
											duodenal switch) or by resecting and re-routing the small
											intestine to a small stomach pouch (gastric bypass surgery).</p>

										<p>Long-term studies show the procedures cause significant
											long-term loss of weight, recovery from diabetes, improvement
											in cardiovascular risk factors, and a reduction in mortality
											of 23% from 40%.[1] However, a study in Veterans Affairs (VA)
											patients has found no survival benefit associated with
											bariatric surgery among older, severely obese people when
											compared with usual care, at least out to seven years.</p>


									</div>
                                </div>
                            </div>
                            
                            
                            
                            
                        </div>
							
							
							
							
							
							
							
							
                      </div>
                      
                      
                    </div>

                    
                    
                </section>
                
            </div>
            <!-- /#contact.container -->

            
           

            

            <%@include file="index_hospital.jsp" %>

            	
            	<div id="get-it" style="padding: 0 0 3px ! important;">
		            <div class="container">
		                <div class="col-md-8 col-sm-12">
		                   
		                </div>
		                <div class="col-md-4 col-sm-12">
		                
		                </div>
		            </div>
		        </div>
		

        </div>
        <!-- /#content -->

        

         <%@include file="index_footer.jsp" %>


    </div>
    <!-- /#all -->

    
    <%@include file="index_common_script.jsp" %>
    

</body>
</html>