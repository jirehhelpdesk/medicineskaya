<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<body>


<div class="col-md-10" id="blog-listing-medium" >
                       
      <% int i = 1;%>
                       
      <c:forEach items="${touristDetails}" var="list">	 
      
      		
  		<c:set var="fileName" value="${list.place_photo}"/>
        <% String fileName = (String)pageContext.getAttribute("fileName"); %>


        <section class="post" style="border-bottom: 1px solid rgb(206, 206, 206); margin-bottom: 25px;" id="section<%=i%>">
            
            <div class="row" style="margin-bottom:10px;" >
                
                <div class="col-md-3">
                    <div class="image imageOuterDiv">
                        <a href="#">  
                         	
                         <img class="img-responsive categoryImgae" alt="<%=fileName%>" src="${pageContext.request.contextPath}<%="/showImages?fileName="+fileName+"&directoryType=Portal&subDirectory=TouristPlaces"%>"   />				  
                             
                      </a>
                    </div>
                </div>
                
                <div class="col-md-9">
                    
                    <h2><a href="#">${list.place_name}</a></h2>
                    
                    <div class="clearfix">
                        
                        <p class="author-category"><a href="#">${list.location}</a>
                        </p>
                        
                        
                    </div>
                    
                    
                    <div class="intro" id="aboutUs<%=i%>" style="height:183px;overflow:hidden;">
                    
                    			${list.about_place}
                    
                    </div>
                    
                    <span id="seeMore<%=i%>" class="seemore" style="float:right;display:none;color:#38a7bb;cursor:pointer;" onclick="showMoreAboutUs('${list.place_id}','<%=i%>')">See more</span>
                    
                </div>
                
            </div>
            
        </section>
       
           <% i = i + 1; %>
           
      </c:forEach> 
      
      
   </div>
                    
                    
</body>
</html>