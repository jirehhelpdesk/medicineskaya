<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Doctor Details</title>


   <%@include file="admin_header_css.jsp" %>
  
  
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<script type="text/javascript" src="static/adminResources/editor/jscripts/tiny_mce/tiny_mce.js"></script>

<script type="text/javascript">
tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,		
		template_replace_values : 
		{
		 username : "Some User",
		 staffid : "991234"
		}
	});
	
</script>


<body class="hold-transition skin-blue sidebar-mini" onload="activeSideMenu('menu3','2')">

<div class="wrapper">

  
  
  <%@include file="admin_header_bar.jsp" %>
  
  
  
  
  <%@include file="admin_sidemenu_bar.jsp" %>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    
    
    <section class="content-header">
      <h1>
        Doctor Information
        <small>MedicinesKaya</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li >Portal Data</li><li class="active">Doctor Details</li>
      </ol>
    </section>

   
   
   
   
    <!-- Main content -->
    
        <section class="content">
          
          <div class="row">
            
            <!-- left column -->
            
            <div class="col-md-2">
            
            </div>
            
            
            <!-- right column -->
            
            <div class="col-md-8">
              <!-- general form elements disabled -->
              
              
              <!-- Input addon -->
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Add Doctor Information</h3>
                </div>
                
                
                <div class="box-body">
                 
                 <form:form  id="doctorForm" modelAttribute="doctorModel" method="post" action="saveDoctorDetails" >
                  
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
		                    <input type="text" name="doctor_name" id="doctor_name" class="form-control" placeholder="Doctor full name">
		                  	<span class="errorMsg" id="errName"></span>
		                  </div>
		                  
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
		                    <input type="text" name="doctor_degree" id="doctor_degree" class="form-control" placeholder="Doctor degree">
		                  	<span class="errorMsg" id="errDegree"></span>
		                  </div>
		                  
		                  <div class="input-group" id="specialist">
		                  	
		                  	<span class="input-group-addon"><i class="fa fa-stethoscope"></i></span>
		                    <select class="form-control" name="doctor_specialization" id="doctor_specialization" >
		                    		<option value="Select Specialist">Select Specialist</option>
		                    		<c:forEach items="${specialist_list}" var="val">
		                    			<option value="${val.specialist_type}">${val.specialist_type}</option>
		                    		</c:forEach>
		                    </select>
		                    <span class="errorMsg" id="errSpecialization"></span>
		                  </div>
		                  
                  
		                  <div class="input-group" style="width:98%;" >		
		                    <label>About Doctor</label>                  	
		                  	<textarea  class="form-control" id="aboutUs" name="about_doctor" placeholder="About doctor"></textarea>
		                  	<span class="errorMsg" id="errAboutUs"></span>
		                  </div>
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-link"></i></span>
		                    <input type="text" name="doctor_website" path="doctor_website" id="doctor_website" class="form-control" placeholder="Doctor web site">
		                  	<span class="errorMsg" id="errWebsite"></span>
		                  </div>
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
		                    <input type="text" name="doctor_email" path="doctor_email" id="doctor_email" class="form-control" placeholder="Doctor email id">
		                  	<span class="errorMsg" id="errEmail"></span>
		                  </div>
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
		                    <input type="text" name="doctor_phone" path="doctor_phone" id="doctor_phone" class="form-control" placeholder="Doctor phone number">
		                  	
		                  </div>
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-facebook-official"></i></span>
		                    <input type="text" name="doctor_facebook" path="doctor_facebook" id="doctor_facebook" class="form-control" placeholder="Doctor facebook link">
		                  	
		                  </div>
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-skype"></i></span>
		                    <input type="text" name="doctor_skype" path="doctor_skype" id="doctor_skype" class="form-control" placeholder="Doctor skype id">
		                  </div>
		                  
		                  <div class="input-group">
		                    <span class="input-group-addon"><i class="fa fa-photo"></i></span>
		                    <input type="file"  class="form-control" id="file" placeholder="Doctor's photo" name="file">
		                  </div>
		                  
		                  
                  </form:form> 
                  
                  	
						  <div class="box-footer" style="float:right;margin-top:10px;">
		                    <button  class="btn btn-primary" onclick="saveDoctorDetails()">Submit</button>
		                  </div>
		                  
		                  
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              
              
            </div><!--/.col (right) -->
            
            
            <div class="col-md-2">
            
            </div>
            
            
          </div>   <!-- /.row -->
          
          
        </section><!-- /.content -->
        
        
        
        
        
        
        
        
  </div>
  <!-- /.content-wrapper -->
 
 
 
  <%@include file="admin_footer.jsp" %>

 
 
</div>
<!-- ./wrapper -->



  <%@include file="admin_script.jsp" %>

</body>


</html>