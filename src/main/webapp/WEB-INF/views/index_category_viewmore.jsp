<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<body>



	<div class="col-md-12 categoryViewMoreDiv" id="customer-orders" >
		
		
		<c:forEach items="${viewMoreResult}" var="list">
		
			<div class="heading" style="margin-bottom: 12px;">
	            <%-- <h2 style="font-size: 19px;font-weight: 300;">About ${list.name}</h2> --%>
	        </div>
		
      		
	  		<c:set var="fileName" value="${list.photo}"/>
	        <% String fileName = (String)pageContext.getAttribute("fileName"); %>
	        
	        <c:set var="type" value="${list.type}"/>
	        <% String type = (String)pageContext.getAttribute("type"); %>


	        <section class="post" style="border-bottom: 1px solid rgb(206, 206, 206); margin-bottom: 25px;">
	            
	            <div class="row" style="margin-bottom:10px;">
	                
	                <div class="col-md-4">
	                    <div class="image imageOuterDiv" >
	                        <a href="#">  
	                         	
	                         <img class="img-responsive categoryImgae" alt="<%=fileName%>" src="${pageContext.request.contextPath}<%="/showImages?fileName="+fileName+"&directoryType=Category&subDirectory="+type+""%>"   />				  
	                             
	                      </a>
	                    </div>
	                </div>
	                
	                <div class="col-md-8">
	                    
	                    <h2 style="color:#38a7bb;margin-bottom: 8px;margin-top: 0;" >${list.name}</h2>
	                    
	                    <div class="clearfix">
	                        
	                        <p class="author-category">in <a href="#">${list.city}</a>
	                        </p>
	                        
	                    </div>
	                    
	                    <table>
	                    		<tr>
	                    		    <td class="tableRowAlign"><span style="color:#38a7bb;">Address </span></td>
	                    		    <td>:&nbsp;&nbsp;&nbsp;</td>
	                    		    <td>${list.address}</td>
	                    		</tr>
	                    		
	                    		<tr>
	                    		    <td class="tableRowAlign"><span style="color:#38a7bb;">Email id </span></td>
	                    		    <td>:&nbsp;&nbsp;&nbsp;</td>
	                    		    <td>${list.email_id}</td>
	                    		</tr>
	                    		
	                    		<tr>
	                    		    <td class="tableRowAlign"><span style="color:#38a7bb;">Phone </span></td>
	                    		    <td>:&nbsp;&nbsp;&nbsp;</td>
	                    		    <td>${list.phone}</td>
	                    		</tr>
	                    		
	                    		<c:if test="${list.gender !=null}">
	                    		
		                    		<tr>
		                    		    <td class="tableRowAlign"><span style="color:#38a7bb;">Gender </span></td>
		                    		    <td>:&nbsp;&nbsp;&nbsp;</td>
		                    		    <td>${list.gender}</td>
		                    		</tr>
	                    		</c:if>
	                    		
	                    		<c:if test="${list.language_known !=null}">
	                    		
	                    		
		                    		<tr>
		                    		    <td class="tableRowAlign"><span style="color:#38a7bb;">Language  </span></td>
		                    		    <td>:&nbsp;&nbsp;&nbsp;</td>
		                    		    <td>${list.language_known}</td>
		                    		</tr>
	                    		</c:if>
	                    		
	                    		<c:if test="${list.specialization !=null}">
	                    		
		                    		<tr>
		                    		    <td class="tableRowAlign"><span style="color:#38a7bb;">Specialization </span></td>
		                    		    <td>:&nbsp;&nbsp;&nbsp;</td>
		                    		    <td>${list.specialization}</td>
		                    		</tr>
		                    		
	                    		</c:if>
	                    		
	                    		<c:if test="${list.type_of_food !=null}">
	                    		
		                    		<tr>
		                    		    <td class="tableRowAlign"><span style="color:#38a7bb;">Food type </span></td>
		                    		    <td>:&nbsp;&nbsp;&nbsp;</td>
		                    		    <td>${list.type_of_food}</td>
		                    		</tr>
		                    		
	                    		</c:if>
	                    		
	                    		<c:if test="${list.sub_type !=null}">
	                    		
		                    		<tr>
		                    		    <td class="tableRowAlign"><span style="color:#38a7bb;">Type </span></td>
		                    		    <td>:&nbsp;&nbsp;&nbsp;</td>
		                    		    <td>${list.sub_type}</td>
		                    		</tr>
		                    		
	                    		</c:if>
	                    		
	                    		<c:if test="${list.urlLink !=null}">
	                    		
		                    		<tr>
		                    		    <td class="tableRowAlign"><span style="color:#38a7bb;">Website </span></td>
		                    		    <td>:&nbsp;&nbsp;&nbsp;</td>
		                    		    <td>${list.urlLink}</td>
		                    		</tr>
		                    		
	                    		</c:if>
	                    
	                    </table>
	                    
	                    
	                    
	                    <%--  <p class="intro"><span style="color:#38a7bb;">Email id &nbsp;-&nbsp;</span>${list.email_id}</p>
	                    
	                    <p class="intro"><span style="color:#38a7bb;">Phone &nbsp;-&nbsp;</span>${list.phone}</p>
	                    
	                    <c:if test="${list.gender !=null}">
						 
						 <p class="intro"><span style="color:#38a7bb;">Gender &nbsp;-&nbsp;</span>${list.gender}</p>
	                    
						</c:if>
						
						<c:if test="${list.language_known !=null}">
						 
						 <p class="intro"><span style="color:#38a7bb;">Language Known &nbsp;-&nbsp;</span>${list.language_known}</p>
	                    
						</c:if>
						
						<c:if test="${list.specialization !=null}">
						 
						 <p class="intro"><span style="color:#38a7bb;">Specialization &nbsp;-&nbsp;</span>${list.specialization}</p>
	                    
						</c:if>
						
						<c:if test="${list.type_of_food !=null}">
						 
						 <p class="intro"><span style="color:#38a7bb;">Food type &nbsp;-&nbsp;</span>${list.type_of_food}</p>
	                    
						</c:if>
	                    
	                    <c:if test="${list.urlLink !=null}">
						 
						 <p class="intro"><span style="color:#38a7bb;">Website &nbsp;-&nbsp;</span>${list.urlLink}</p>
	                    
						</c:if> --%>
	                    
	                    
	                </div>
	                
	            </div>
	            
	        </section>
       
       
        </c:forEach> 
      
		<!-- /.box -->

	</div>
	<!-- /.col-md-9 -->



</body>
</html>