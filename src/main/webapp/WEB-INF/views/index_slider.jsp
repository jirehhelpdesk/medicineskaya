


 <!-- Start slider section -->  
 	
 	<script src="static/indexResources/slider/js/jssor.slider-21.1.6.min.js" type="text/javascript"></script>
    <script src="static/indexResources/slider/slider.js" type="text/javascript"></script>
    
    
     <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height:530px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('static/indexResources/slider/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        
        
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1300px; height:530px; overflow: hidden;">
           
            
            <div data-p="225.00">
                
                <img data-u="image" src="static/indexResources/slider/img/slide1.jpg" />
                
                <div style="position: absolute; top: 30px; left: 30px; width:auto; height: 120px; font-size:66px; color: #fff; line-height: 60px;">Medical Tourism</div>
                
                <div style="position: absolute; top: 116px; left: 30px; width:auto; height: 120px; font-size:40px; color: #ffffff; line-height: 38px;">High value health care solution </div>
                
            </div>
            
            <div data-p="225.00" style="display: none;">
                
                <img data-u="image" src="static/indexResources/slider/img/slide2.jpg" />
                
                <div style="position: absolute; top:237px; left:160px; width:auto; height: 120px; font-size:32px; color: #006ca6; line-height: 60px;">Partnered with</div>
                
                <div style="position: absolute; top: 331px; left:30px; width:auto; height: 120px; font-size: 48px; color: #006ca6; line-height: 38px;">Best Specialist Doctors </div>
                
                <div style="position: absolute; top: 388px; left:231px; width:auto; height: 120px; font-size: 48px; color: #006ca6; line-height: 38px;text-align:center;">&</div>
                
                <div style="position: absolute; top: 451px; left:30px; width:auto; height: 120px; font-size:48px; color: #006ca6; line-height: 38px;">High Quality Hospitals </div>
                
            </div>
            
            
            <div data-p="225.00" data-po="80% 55%" style="display: none;">
                
                <img data-u="image" src="static/indexResources/slider/img/slide5.jpg" />
                
                <div style="position: absolute; top:-5px; left: 30px; width:auto; height: 120px; font-size:34px; color: #fff; line-height: 60px;"></div>
                
                <div style="position: absolute;top:277px; left: 562px; width:auto; height: 120px; font-size:30px; color: #fff; line-height: 38px;">Visit our places</div>
                
            </div>
            
            <a data-u="any" href="http://www.jssor.com" style="display:none">Full Width Slider</a>
            
        </div>
        
        
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:22px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        
        
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:8px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:8px;width:40px;height:58px;" data-autocenter="2"></span>
    
    
    </div>
   
    <script type="text/javascript">jssor_1_slider_init();</script>
    
    <!-- #endregion Jssor Slider End -->
 		
 <!-- End slider section -->

