
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<c:if test="${!empty userBean}">

	<button class="btn btn-template-main searchBtn"
		style="margin-top: 10px;"
		onclick="searchRequiredDetails('${userBean.user_id}')">
		<i class="fa fa-search"></i> Search
	</button>

</c:if>

<c:if test="${empty userBean}">

	<button class="btn btn-template-main searchBtn"
		style="margin-top: 10px;" onclick="searchRequiredDetails('0')">
		<i class="fa fa-search"></i> Search
	</button>

</c:if>