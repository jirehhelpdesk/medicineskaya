<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>About Us</title>

    <%@include file="index_header_css.jsp" %>
    
    
</head>

<body onload="activeHeaderMenu('menu2')">


<div id="all">
        
        
        <%@include file="index_header_menu.jsp" %>
        

        
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-7">
                        <h1>About us</h1>
                    </div> -->
                    <div class="col-md-12">
                        <ul class="breadcrumb" style="padding: 2px 0;margin-bottom:5px;">
                            <li>Home                            </li>
                            <li>About us</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        
        
        <div id="content">
            <div class="container">

                <section>
                    
                    <div class="row">
                        
                      
                      <%-- <%@include file="index_header_menu.jsp" %> --%>
                      
                      <div class="col-md-3">
                        <!-- *** MENUS AND WIDGETS *** -->

                        <%@include file="index_side_search_div.jsp" %>

                        <!-- *** MENUS AND FILTERS END *** -->
                    </div>
                    
                     
                     
                     
                     <div class="col-md-9 rightContentDiv" id="categoryResultHeading">

                          <div class="heading">
                              <h2>About Us</h2>
                          </div>

						   <p class="lead">Medical Tourism being an integral part of the
							world right now hasbeen a key growth sector for more than a
							decade. Hosting about 1.27 million tourists India has been
							rapidly emerging as a wellness hub due to world class Medical
							Infrastructure at comparatively fractional cost. Health Tourism
							holds immense hope for India due to modern medical amenities,
							alternative systems of medical treatment, cost-effective
							treatment, strong pharma Industry, less language barrier. Hence,
							Various medical tourism companies which facilitate the healthcare
							of tourists in India are catering to their needs.</p>


						   <p class="lead">Established in 2009, MedicinesKaya India is
							based out of Delhi. Since 7 years we are growing Medical Tourism
							facilitator with footprints in CIS, Africa & SAARC countries. We
							are involved in providing our customers High value Health care
							Solutions. Our motto is to facilitate customized, caring and
							cost-effective treatment.</p>


						    <p class="lead">We have partnered with best specialist
							Doctors, high quality hospitals and professionals to earn
							customer's trust and build reputation. We have been gaining ample
							experience in Medical Tourism sector with a great satisfaction
							among our international customers. The positive feedback has
							always encouraged us to expand our outreach to serve more number
							of patients who are in need of right medical care solutions.</p>


						    <p class="lead">MedicinesKaya India facilitates healthcare
							needs of patients from all over the world. We provide end to end
							solution right from medical visa processing, travel assistance,
							accommodation, tours, pre and post operative care or any other
							aspect. We are young as a company, but yes we are keen in finding
							right solutions for medical and health related issues across the
							world. We are here to serve every one across the globe for their
							medical needs. No wonder, we have achieved a reputed position as
							the most reliable medical tourism company in today's date. Care,
							Quality, Honesty, Punctuality, Ethical Behaviour and transparency
							are the core values which have brought this to us. </p>


					</div>
                      
                      
                    </div>

                    
                    
                </section>
                
            </div>
            <!-- /#contact.container -->

            
           

            

            <%@include file="index_hospital.jsp" %>

            
            
            <div id="get-it" style="padding: 0 0 3px ! important;">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                   
                </div>
                <div class="col-md-4 col-sm-12">
                
                </div>
            </div>
        </div>
        


        </div>
        <!-- /#content -->

        

         <%@include file="index_footer.jsp" %>


    </div>
    <!-- /#all -->

    
    <%@include file="index_common_script.jsp" %>
    

</body>
</html>