
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<c:if test="${!empty userBean}">

	<div class="col-sm-12 text-center"
		style="margin-bottom: 10px; margin-top: 12px;">
		<button class="btn btn-template-main"
			onclick="saveSecondOpinion('${userBean.user_id}')">
			<i class="fa fa-envelope-o"></i>Submit
		</button>
	</div>

</c:if>

<c:if test="${empty userBean}">

	<div class="col-sm-12 text-center"
		style="margin-bottom: 10px; margin-top: 12px;">
		<button class="btn btn-template-main" onclick="saveSecondOpinion('0')">
			<i class="fa fa-envelope-o"></i>Submit
		</button>
	</div>

</c:if>