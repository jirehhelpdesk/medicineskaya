
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div class="col-md-12">
	
	<c:if test="${!empty contactDetails}"> 
			
			<c:forEach items="${contactDetails}" var="list">	
			
				<div class="box box-primary" style="background-color: #fff ! important;">
			
					<div class="box-header with-border">
						<h3 class="box-title">Message</h3>
			
					</div>
					<!-- /.box-header -->
			
					<div class="box-body no-padding">
			
						<div class="mailbox-read-info">
							<h3>${list.contact_subject}</h3>
                    		<h5>From: ${list.contact_email_id} <span class="mailbox-read-time pull-right"><fmt:formatDate pattern="dd/MM/yyyy hh:mm:ss" value="${list.contact_date}" /></span></h5>
							
						</div>
						<!-- /.mailbox-read-info -->
			
						<div class="mailbox-read-message">
			
							<p>${list.contact_message}</p>
			
						</div>
						<!-- /.mailbox-read-message -->
					</div>
					<!-- /.box-body -->
			
					<!-- <div class="box-footer">
			                  <div class="pull-right">
			                    <button class="btn btn-default"><i class="fa fa-reply"></i> Reply</button>
			                    <button class="btn btn-default"><i class="fa fa-share"></i> Forward</button>
			                  </div>
			                  <button class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</button>
			                  <button class="btn btn-default"><i class="fa fa-print"></i> Print</button>
			                </div> -->
			
				</div>
				<!-- /. box -->
				
			</c:forEach>
			
	</c:if>
	
	
	
	
	<c:if test="${!empty opinionDetails}"> 
			
			<c:forEach items="${opinionDetails}" var="list">	
			
				<div class="box box-primary" style="background-color: #fff ! important;">
			
					<div class="box-header with-border">
						<h3 class="box-title">For Second Opinion on &nbsp;<b>${list.treatment_on}</b></h3>
			
					</div>
					<!-- /.box-header -->
			
					<div class="box-body no-padding">
			
						<div class="mailbox-read-info">
							<h3 id="heading">${list.doctor_name}</h3>
							<h5>Appointment Date: <fmt:formatDate pattern="dd/MM/yyyy" value="${list.appointment_date}" />
								<span class="mailbox-read-time pull-right"><fmt:formatDate pattern="dd/MM/yyyy hh:mm:ss" value="${list.cr_date}" /></span>
							</h5>
						</div>
						<!-- /.mailbox-read-info -->
			
						<div class="mailbox-read-message">
			
							<p>${list.remark}</p>
			
						</div>
						<!-- /.mailbox-read-message -->
					</div>
					<!-- /.box-body -->
			
					<!-- <div class="box-footer">
			                  <div class="pull-right">
			                    <button class="btn btn-default"><i class="fa fa-reply"></i> Reply</button>
			                    <button class="btn btn-default"><i class="fa fa-share"></i> Forward</button>
			                  </div>
			                  <button class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</button>
			                  <button class="btn btn-default"><i class="fa fa-print"></i> Print</button>
			                </div> -->
			
				</div>
				<!-- /. box -->
				
			</c:forEach>
			
	</c:if>
	
</div>
<!-- /.col -->

