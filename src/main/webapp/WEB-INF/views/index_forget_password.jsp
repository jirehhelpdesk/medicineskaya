<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Forget Password</title>

    <%@include file="index_header_css.jsp" %>
    
    
</head>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<body onload="activeHeaderMenu('menu6')">


<div id="all">
        
        
        <%@include file="index_header_menu.jsp" %>
        

        
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-7">
                        <h1>Forget Password</h1>
                    </div> -->
                    <div class="col-md-12">
                        <ul class="breadcrumb" style="padding: 2px 0;margin-bottom:5px;">
                            <li><a href="index">Sign In</a>
                            </li>
                            <li>Forget password</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        
        
        
        
        <div id="content">
            <div class="container">

                <div class="row">
                    <div class="col-md-6">
                        <div class="box">
                            <h2 class="text-uppercase"></h2>
							
							<img src="static/indexResources/img/3.png" style="width:480px;height: 324px;"/>
							
                            <!-- <p class="lead">Already our user?</p>
                            <p class="text-muted">Please login with your credential and search or find the relevant things which you need in your journey to make a healthiest and safest tour.</p>

                            
                            <div class="text-center">
                                <button style="margin-top: 10px;" class="btn btn-template-main" onclick="redirectUrl('signIn')"><i class="fa fa-sign-in"></i> Go to Sign In</button>
                            </div> -->
                                
                         <!--    <hr> -->
                                
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="box">
                            <h2 class="text-uppercase" style="margin-bottom: 8px; margin-top: 1px;">Forget Password</h2>

                            <p class="text-muted">Please enter your registered email id we will send you the link to reset the password.</p>

                            <hr>
							
        					<span class="alertMsg"></span>
        					
                            <form class="login-form"   >
						                    
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" name="user_emailid" id="user_emailid">
                                    <span class="errorMsg" id="errEmailId"></span>
                                </div>
                                
                            </form>
                            	
                            	
                                <div class="text-center">
                                    <button  style="margin-top:10px;" class="btn btn-template-main" onclick="genForgetPasswordPattern()"><i class="fa fa-sign-in"></i>Send Link</button>
                                </div>
                            
                        </div>
                    </div>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
        
        

        

         <%@include file="index_footer.jsp" %>


    </div>
    <!-- /#all -->

    
    <%@include file="index_common_script.jsp" %>
    

</body>
</html>