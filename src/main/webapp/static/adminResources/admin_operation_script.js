function activeSideMenu(menuType,subMenuCounter)
{ 		
	if(menuType=='menu1')
		{		
		     $("#menu1").addClass('active');
		}
	else if(menuType=='menu2')
		{
			 $("#menu2").addClass('active');
			 
			 if(subMenuCounter==1)
				 {
				 	$("#menu2_1").addClass('active_sidemenu');
				 }
			 else
				 {
				    $("#menu2_2").addClass('active_sidemenu');
				 }			
		}
	else if(menuType=='menu3')
		{		
			 $("#menu3").addClass('active');
			 
			 if(subMenuCounter==1)
				 {
				 	$("#menu3_1").addClass('active_sidemenu');
				 }	
			 else
				 {
				 	$("#menu3_2").addClass('active_sidemenu');
				 }	 
		}
	else if(menuType=='menu4')
		{
		  
			 $("#menu4").addClass('active');
			 
			 if(subMenuCounter==1)
				 {
				 	$("#menu4_1").addClass('active_sidemenu');
				 }	
			 else
				 {
				 	$("#menu4_2").addClass('active_sidemenu');
				 }	 
		}
	else if(menuType=='menu5')
		{
			 $("#menu5").addClass('active');
			 
			 if(subMenuCounter==1)
				 {
				 	$("#menu5_1").addClass('active_sidemenu');
				 }	
			 else  if(subMenuCounter==2)
			     {
				 	$("#menu5_2").addClass('active_sidemenu');
				 }	
			 else
			     {
				 	$("#menu5_3").addClass('active_sidemenu');
				 }	
		}
	else if(menuType=='menu6')
		{
			
		     $("#menu6").addClass('active');
		 
			 if(subMenuCounter==1)
				 {
				 	$("#menu6_1").addClass('active_sidemenu');
				 }	
			 else  if(subMenuCounter==2)
			     {
				 	$("#menu6_2").addClass('active_sidemenu');
				 }	
			 else
			     {
				 	$("#menu6_3").addClass('active_sidemenu');
				 }
			 
		}
	else 
	{
		
	     $("#menu7").addClass('active');;
	 
		 if(subMenuCounter==1)
			 {
			 	$("#menu7_1").addClass('active_sidemenu');
			 }	
		 else  if(subMenuCounter==2)
		     {
			 	$("#menu7_2").addClass('active_sidemenu');
			 }	
		 else
		     {
			 	$("#menu7_3").addClass('active_sidemenu');
			 }
		 
	}
	
	
}


//////////////////////  ADMIN CATEGORY SCRIPT   /////////////////////////////////////


function showHiddenComponet(value)
{	
	if(value=="Translator")
	{
		$("#gender").slideDown();
		$("#language").slideDown();
		
		$("#website").slideUp();		
		$("#cusiene").slideUp();
		$("#specialist").slideUp();
		$("#taxiType").slideUp();
		$("#hotelTypeDiv").slideUp();
		
	}
	else if(value=="Attender and Nurse")
	{
		$("#gender").slideDown();
		
		$("#website").slideUp();
		$("#language").slideUp();
		$("#cusiene").slideUp();
		$("#specialist").slideUp();
		$("#taxiType").slideUp();
		$("#hotelTypeDiv").slideUp();
	}
	else if(value=="Doctor")
	{
		$("#gender").slideDown();
		$("#specialist").slideDown();
		
		$("#website").slideUp();
		$("#language").slideUp();
		$("#cusiene").slideUp();
		$("#taxiType").slideUp();
		$("#hotelTypeDiv").slideUp();
	}
	else if(value=="Taxi Service")
	{
		$("#website").slideDown();
		$("#taxiType").slideDown();
		
		$("#gender").slideUp();		
		$("#language").slideUp();
		$("#cusiene").slideUp();
		$("#specialist").slideUp();
		$("#hotelTypeDiv").slideUp();
	}
	else if(value=="Hotel and Lodge")
	{
		$("#website").slideDown();
		$("#cusiene").slideDown();
		$("#hotelTypeDiv").slideDown();
		
		$("#specialist").slideUp();		
		$("#gender").slideUp();		
		$("#language").slideUp();
		$("#taxiType").slideUp();
	}
	else if(value=="Restaurent")
	{
		$("#website").slideDown();
		$("#cusiene").slideDown();
		
		$("#specialist").slideUp();		
		$("#gender").slideUp();		
		$("#language").slideUp();
		$("#taxiType").slideUp();
		$("#hotelTypeDiv").slideUp();
	}
	else
	{
		$("#gender").slideUp();
		$("#website").slideUp();
		$("#language").slideUp();	
		$("#cusiene").slideUp();
		$("#specialist").slideUp();
		$("#taxiType").slideUp();
		$("#hotelTypeDiv").slideUp();
	}

}


function saveCategoryDetails()
{
	var flag = 'true';
	
	var category = $("#category_type").val();
	
	var name = $("#name").val();
	var email = $("#email").val();
	var phone = $("#phone").val();
	var address = $("#address").val();
	var city_name = $("#city_name").val();
	
	if(category=="Select Category")
	{
		flag = 'false';
		$("#errCategory").html("please select a category.");
		$("#errCategory").show();		
	}
	
	if(category!="Select Category")
	{
		$("#errCategory").hide();		
	}
	
	if(name=="")
	{
		flag = 'false';
		$("#errName").html("Enter the name.");
		$("#errName").show();		
	}
	
	if(name!="")
	{
		$("#errName").hide();		
	}
	
	if(email=="")
	{
		flag = 'false';
		$("#errEmail").html("Enter the email id.");
		$("#errEmail").show();		
	}
	
	if(email!="")
	{
		$("#errEmail").hide();		
	}
	
	/*var category = $("#category_type").val();
	var category = $("#category_type").val();*/
	
	
	if(flag=='true')
	{		
		 $("#bgModel").show();
		 $("#loaderId").show();
		 	
		 var formData = new FormData($("#categoryForm")[0]);
		
		 formData.append("file", file.files[0]);
		 
		 if(category=="Translator")
		 {
			var click = [];
			$(".language:checked").each(function() {
		        click.push($(this).val());
		    });
		
			if(click.length!=0)
			{
				formData.append("language_name",click);
			}
		 }
		 	
		 $.ajax({
					type : "POST",
					data : formData,
					url : "saveCategoryDetails",								
					success : function(response) {
			            
						$("#bgModel").hide();
						$("#loaderId").hide();
						 
						if(response=="success")
			      		{		
						 	alert("Record has been saved successfully.");
						 	window.location = "adminService";
			      		}
				      	else
			      		{
			      			alert("Failed due to some issue please try again later.");	
			      			window.location = "adminService";
			      		}
						
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});			   	
	}
}



//////////////////////   END OF ADMIN CATEGORY SCRIPT   /////////////////////////////////////



//////////////////////  ADMIN PORTAL DATA SCRIPT   /////////////////////////////////////


function saveTouristPlaceDetails()
{
	var flag = 'true';
	
	var place_name = $("#place_name").val();
	var location = $("#location").val();
	var aboutUs = $("#aboutUs").val();
	
	if(place_name=="")
	{
		flag = 'false';
		$("#errName").html("Enter place name.");
		$("#errName").show();		
	}
	
	if(place_name!="")
	{
		$("#errName").hide();		
	}
	
	if(location=="")
	{
		flag = 'false';
		$("#errLocation").html("Enter location details.");
		$("#errLocation").show();		
	}
	
	if(location!="")
	{
		$("#errLocation").hide();		
	}
	
	if(aboutUs=="")
	{
		flag = 'false';
		$("#errAboutUs").html("Write something about the place.");
		$("#errAboutUs").show();		
	}
	
	if(aboutUs!="")
	{
		$("#errAboutUs").hide();		
	}
	
	if(flag=='true')
	{		
		 $("#bgModel").show();
		 $("#loaderId").show();
		 
		 var divvalue = tinymce.get("aboutUs").getContent();
		 
		 var formData = new FormData($("#touristPlaceForm")[0]);
		
		 formData.append("textAreaContent",divvalue);
		 
		 formData.append("file", file.files[0]);
		 
		 $.ajax({
					type : "POST",
					data : formData,
					url : "saveTouristPlaceDetails",								
					success : function(response) {
			            
						$("#bgModel").hide();
						$("#loaderId").hide();
						 
						if(response=="success")
			      		{		
						 	alert("Record has been saved successfully.");
						 	window.location = "touristPlace";
			      		}
				      	else
			      		{
			      			alert("Failed due to some issue please try again later.");	
			      			window.location = "touristPlace";
			      		}
						
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});			   	
	}
}


function saveDoctorDetails()
{
	var flag = 'true';
	
	var doctor_name = $("#doctor_name").val();
	var doctor_degree = $("#doctor_degree").val();
	var doctor_specialization = $("#doctor_specialization").val();
	var aboutUs = $("#aboutUs").val();
	
	if(doctor_name=="")
	{
		flag = 'false';
		$("#errName").html("Enter doctor name.");
		$("#errName").show();		
	}
	
	if(doctor_name!="")
	{
		$("#errName").hide();		
	}
	
	if(doctor_degree=="")
	{
		flag = 'false';
		$("#errDegree").html("Enter details about doctor degree.");
		$("#errDegree").show();		
	}
	
	if(doctor_degree!="")
	{
		$("#errDegree").hide();		
	}
	
	if(doctor_specialization=="Select Specialist")
	{
		flag = 'false';
		$("#errSpecialization").html("Select doctor's specialization.");
		$("#errSpecialization").show();		
	}
	
	if(doctor_specialization!="Select Specialist")
	{
		$("#errSpecialization").hide();		
	}
	
	if(aboutUs=="")
	{
		flag = 'false';
		$("#errAboutUs").html("Write something about doctor.");
		$("#errAboutUs").show();		
	}
	
	if(aboutUs!="")
	{
		$("#errAboutUs").hide();		
	}
	
	if(flag=='true')
	{		
		 $("#bgModel").show();
		 $("#loaderId").show();
		 
		 var divvalue = tinymce.get("aboutUs").getContent();
	      
		 var formData = new FormData($("#doctorForm")[0]);
		
		 formData.append("textAreaContent",divvalue);
		 
		 formData.append("file", file.files[0]);
		 
		 $.ajax({
					type : "POST",
					data : formData,
					url : "saveDoctorDetails",								
					success : function(response) {
			            
						$("#bgModel").hide();
						$("#loaderId").hide();
						 
						if(response=="success")
			      		{		
						 	alert("Record has been saved successfully.");
						 	window.location = "priorityDoctor";
			      		}
				      	else
			      		{
			      			alert("Failed due to some issue please try again later.");	
			      			window.location = "priorityDoctor";
			      		}
						
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});			   	
	}
}





//////////////////////  END OF ADMIN PORATL DATA SCRIPT   /////////////////////////////////////



///////////////////////   ADMIN SETTINGS   ////////////////////////////////////////////////////


function saveAdminDetails()
{
    var flag = 'true';
	
    if(flag=='true')
	{
    	$("#bgModel").show();
		$("#loaderId").show();
 	 	
  		$.ajax({  
		 			
		     type : "post",   
		     url : "saveAdminDetails", 
		     data :$('#adminDetailForm').serialize(),	     	     	     
		     success : function(response) 
		     {  		
		    	 $("#bgModel").hide();
				 $("#loaderId").hide();
		    	 
				 if(response=="success")
	      		 {		
				 	alert("Record has been updated successfully.");
				 	window.location = "adminSetting";
	      		 }
		      	 else
	      		 {
	      			alert("Failed due to some issue please try again later.");	
	      			window.location = "adminSetting";
	      		 }
		     },  
	     }); 
	 }
   	
}


function saveAdminPassword()
{
    var flag = 'true';
	
    var password  = $("#password").val();
    var newPassword  = $("#new_password").val();
    var cnfPassword  = $("#cnf_password").val();
    
    if(password=="")
    {
    	flag = 'false';
    	$("#errPassword").show();
    	$("#errPassword").html("Enter current admin password.");
    }
    
    if(password!="")
    {
    	$("#errPassword").hide();
    }
    
    if(newPassword=="")
    {
    	flag = 'false';
    	$("#errNewPassword").show();
    	$("#errNewPassword").html("Enter admin new password.");
    }
    
    if(newPassword!="")
    {
    	$("#errNewPassword").hide();
    }
    
    if(cnfPassword=="")
    {
    	flag = 'false';
    	$("#errCnfPassword").show();
    	$("#errCnfPassword").html("Confirm admin new password.");
    }
    
    if(cnfPassword!="")
    {
    	$("#errCnfPassword").hide();
    }
    
    if(cnfPassword!=newPassword)
    {
    	flag = 'false';
    	$("#errCnfPassword").show();
    	$("#errCnfPassword").html("Confirm admin new password.");
    }
    
	if(flag=='true')
	{		
		 $("#bgModel").show();
		 $("#loaderId").show();
		
		 $.ajax({
					type : "post",
					url : "saveAdminPassword",	
					data :$('#adminPasswordForm').serialize(),	     	
					success : function(response) {
			            
						$("#bgModel").hide();
						$("#loaderId").hide();
						 
						if(response=="success")
			      		{		
						 	alert("Record has been updated successfully.");
						 	window.location = "adminSetting";
			      		}
						else if(response=="NotExist")
			      		{		
							$("#errPassword").show();
					    	$("#errPassword").html("Current password not matched.");
			      		}
				      	else
			      		{
			      			alert("Failed due to some issue please try again later.");	
			      			window.location = "adminSetting";
			      		}
						
					},					
			});			   	
	}
}


//////////////////////////   END OF ADMIN SETTINGS   ///////////////////////////////////////




///////////////////    ADMIN REPORT SCRIPT   ////////////////////////////////////////////


function reportNotifyMessage(value)
{		
    if(value=="Second opinion")
    {
    	$("#notifyId").html("Date represent the duration when user requested for second opinion.");
    	$("#rowDiv").slideDown();    	
    }
    else if(value=="User List")
    {
    	$("#notifyId").html("Date represent the duration when user registered in the portal.");
    	$("#rowDiv").slideDown();    	
    }
    else if(value=="Contact us")
    {
    	$("#notifyId").html("Date represent the duration when the users contact us.");
    	$("#rowDiv").slideDown();    	
    }
    else
    {
    	$("#notifyId").html("");
    	$("#rowDiv").slideUp();   
    }
     
    
}


function retrivePortalRecord()
{
    var flag = 'true';
	
    var report_type  = $("#report_type").val();
    var fromDate  = $("#some_class_1").val();
    var toDate  = $("#some_class_2").val();
    
    if(report_type=="Select report type")
    {
    	flag = 'false';
    	$("#errReportType").show();
    	$("#errReportType").html("Select report type.");
    }
    
    if(report_type!="Select report type")
    {
    	$("#errReportType").hide();
    }
    
    if(fromDate=="")
    {
    	flag = 'false';
    	$("#errFromDate").show();
    	$("#errFromDate").html("Choose a date from where you need.");
    }
    
    if(fromDate!="")
    {
    	$("#errFromDate").hide();
    }
    
    if(toDate=="")
    {
    	flag = 'false';
    	$("#errToDate").show();
    	$("#errToDate").html("Choose a date upto where you need.");
    }
    
    if(toDate!="")
    {
    	$("#errToDate").hide();
    }
    
	if(flag=='true')
	{		
		 $("#bgModel").show();
		 $("#loaderId").show();
		
		 $.ajax({
					type : "post",
					url : "retrivePortalRecord",	
					data :$('#reportDetailForm').serialize(),	     	
					success : function(response) {
			            
						$("#bgModel").hide();
						$("#loaderId").hide();
						 
						$("#portReportCard").html(response);
						
					},					
			});			   	
	}
}


function downloadReport()
{
	 var flag = 'true';
		
	    var report_type  = $("#report_type").val();
	    var fromDate  = $("#some_class_1").val();
	    var toDate  = $("#some_class_2").val();
	    
	    if(report_type=="Select report type")
	    {
	    	flag = 'false';
	    	$("#errReportType").show();
	    	$("#errReportType").html("Select report type.");
	    }
	    
	    if(report_type!="Select report type")
	    {
	    	$("#errReportType").hide();
	    }
	    
	    if(fromDate=="")
	    {
	    	flag = 'false';
	    	$("#errFromDate").show();
	    	$("#errFromDate").html("Choose a date from where you need.");
	    }
	    
	    if(fromDate!="")
	    {
	    	$("#errFromDate").hide();
	    }
	    
	    if(toDate=="")
	    {
	    	flag = 'false';
	    	$("#errToDate").show();
	    	$("#errToDate").html("Choose a date upto where you need.");
	    }
	    
	    if(toDate!="")
	    {
	    	$("#errToDate").hide();
	    }
	    
		if(flag=='true')
		{	
			window.location = "downloadReport?report_type="+report_type+"&from_date="+fromDate+"&to_date="+toDate+"";
			
		}
}


function viewMessage(uniqueId,type)
{
	$("#bgModel").show();
	$("#loaderId").show();
	
	 $.ajax({
				type : "post",
				url : "viewMessage",	
				data :"uniqueId="+uniqueId+"&type="+type,	     	
				success : function(response) {
		            
					$("#bgModel").hide();
					$("#loaderId").hide();
					 
					$("#messageDivId").html(response);
					
				},					
		});	
}


///////////////////    END OF ADMIN REPORT SCRIPT   ////////////////////////////////////////////
