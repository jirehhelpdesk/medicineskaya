function activeHeaderMenu(menuType)
{ 		
	if(menuType=='menu1')
	{		
		 $("#menu1").addClass('active');
	}
	else if(menuType=='menu2')
	{
		 $("#menu2").addClass('active');				
	}
	else if(menuType=='menu3')
	{		
		 $("#menu3").addClass('active');			 
	}
	else if(menuType=='menu4')
	{	  
		 $("#menu4").addClass('active');			 
	}
	else if(menuType=='menu5')
	{
		 $("#menu5").addClass('active');	
	}
	else if(menuType=='menu6')
	{			
		 $("#menu6").addClass('active');		 
	}
	else if(menuType=='menu7')
	{
	     $("#menu7").addClass('active');;		 
	}
	else 
	{
	     $("#menu8").addClass('active');;		 
	}
	
}

function redirectUrl(url)
{
	window.location = url;
}

function showStateNames(countryName)
{
	
	if(countryName=='India')
	{
		$("#bgModel").show(); 
		$("#loaderId").show();
		
		 $.ajax({  
	 			
		     type : "post",   
		     url : "getStateNames", 
		     data :"countryName="+countryName,	     	     	     
		     success : function(response) 
		     {  		
		    	 $("#bgModel").hide(); 
		    	 $("#loaderId").hide();
		    	 
		    	 var resultvalues = response.split(",");
		    	 
		    	 $("#user_state").html("");
		    	 
		    	 $("#user_state").append("<option value='Select State'>Select State</option>");
		    	 
		    	 for(var i=0;i<resultvalues.length;i++)
	    		 {
	    		 	$("#user_state").append("<option value='"+resultvalues[i]+"'>"+resultvalues[i]+"</option>");
	    		 }
		    	 
		    	 $("#user_state").prop('disabled',false);
		     },  
	     }); 
	}
	else
	{
		$("#user_state").html("");   	 
   	    $("#user_state").append("<option value='Select State'>Select State</option>");
		$("#user_state").prop('disabled',true);
	}
}


function saveUserRegistrartion()
{
	var flag='true';
	
	var alfanumeric = /^[a-zA-Z0-9]*$/;        	
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var phNo = /^[0-9]{10}$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	
    var fname = $("#user_full_name").val();    
    var emailId = $("#user_emailid").val();
    var mobileNo = $("#user_mobile").val();
    var password = $("#user_password").val();
    var confirmPassword = $("#cnfpassword").val();
	
    var country = $("#user_country").val();
    
    if(fname=="")
	{
    	flag='false';
		$("#errNameId").show();
        $("#errNameId").html("Please enter your full name.");	
	}
    
    if(fname!="")
	{
		if(!fname.match(nameWithSpace))  
        {  		
			flag='false';
			$("#errNameId").show();
	        $("#errNameId").html("Only alphabets are allowed.");		              
        }
		else
		{
			$("#errNameId ").hide();			
		}
	}
    
    if(emailId=="")
	{
    	flag='false';
		$("#errEmailId").show();
        $("#errEmailId").html("Please enter your email id.");
	}
    
    if(emailId!="")
	{
		if(!emailId.match(email))  
        {  		
			flag='false';
			$("#errEmailId").show();
	        $("#errEmailId").html("Please enter a valid email id.");	       
        }
		else
		{
			$("#errEmailId").hide();
		}
	}
    
    if(mobileNo=="")
	{
    	flag='false';
		$("#errMobileId").show();
        $("#errMobileId").html("Please enter your mobile no.");	   
	}
    
    if(mobileNo!="")
	{
		if(!mobileNo.match(phNo))  
        {  		
			flag='false';
			$("#errMobileId").show();
	        $("#errMobileId").html("Please enter a valid mobile no.");	      
        }
		else
		{
			$("#errMobileId").hide();
		}
	}
	
    if(country=="Select Country")
    {
    	flag='false';
    	$("#errCountryId").show();
    	$("#errCountryId").html("Select your country.");
    }
    
    if(country!="Select Country")
    {
    	$("#errCountryId").hide();
    	
    	if(country=='India')
    	{
    		var state = $("#user_state").val();
    		if(state=="")
    		{
    			flag='false';
    	    	$("#errStateId").show();
    	    	$("#errStateId").html("Enter your State.");
    		}
    		else
    		{
    			$("#errStateId").hide();
    		}
    	}
    }
    
    
    
    
    if(password=="")
	{
    	flag='false';
		$("#errPasswordId").show();
        $("#errPasswordId").html("Please define a password.");	
	}
    
    if(password!="")
	{
		$("#errPasswordId").hide();
	}
   
    if(password=="")
	{
    	flag='false';
		$("#errCnfPasswordId").show();
        $("#errCnfPasswordId").html("Please confirm your password.");	
	}
    
    if(confirmPassword=="")
	{
    	flag='false';
		$("#errCnfPasswordId").show();
        $("#errCnfPasswordId").html("Confirm password not matched with password.");
	}
    
    if(confirmPassword!="")
	{
		$("#errCnfPasswordId").hide();
	}
   
    if(password!=confirmPassword)
	{
    	flag='false';
		$("#errCnfPasswordId").show();
        $("#errCnfPasswordId").html("Confirm password not matched with password.");
	}
    
    
    if(flag=='true')
	{  	
    	
    	 $("#bgModel").show(); 
    	 $("#loaderId").show();
    	     	 
    	 $.ajax({  
	 			
		     type : "post",   
		     url : "checkUserEmailId", 
		     data :$('#userRegForm').serialize(),	     	     	     
		     success : function(response) 
		     {  		
		    	 $("#bgModel").hide(); 
		    	 $("#loaderId").hide();
		    	 
				 if(response=="NotExist")
					 {			
					    
					     $.ajax({  
					 			
						     type : "post",   
						     url : "checkUserPhno", 
						     data :$('#userRegForm').serialize(),	     	     	     
						     success : function(response) 
						     {  		
						    	 $("#bgModel").hide(); 
						    	 $("#loaderId").hide();
						    	 
								 if(response=="NotExist")
									 {			
									 	$("#bgModel").show(); 
									 	$("#loaderId").show();
										    	  
											     $.ajax({  
											 			
												     type : "post",   
												     url : "saveUserRegisterDetails", 
												     data :$('#userRegForm').serialize(),	     	     	     
												     success : function(response) 
												     {  		
												    	 $("#bgModel").hide(); 
												    	 $("#loaderId").hide();
												    	 
												    	 if(response=="success")
												      		{		
												    		    $("#popUpBgModel").show(); 
												    			$("#popContentId").show();
												    			$("#messageDivId").html("Your have been successfully registered.");
															 						    
												      		}
												      	else
												      		{
													      		$("#popUpBgModel").show(); 
												    			$("#popContentId").show();
												    			$("#messageDivId").html("There is some problem arise,Please try again later.");
															 								    								    
												      		}	
												    	 
												     },  
											     }); 
							      		
									 }
								 else
									 {		
										$("#bgModel").hide(); 
										$("#loaderId").hide();
								    	
										$("#errMobileId").show();
								        $("#errMobileId").html("Given mobile no is already register.");	     
										
									 }
						     },  
					     }); 
					 }
				 else
					 {							 	
					 	$("#bgModel").hide(); 
					 	$("#loaderId").hide();
				    	 
					 	$("#errEmailId").show();
				        $("#errEmailId").html("Given emailId is already registered.");	     
						
					 }
		     },  
	     }); 
    	 
	}
    
}


function closePopDiv()
{	  
	$("#popUpBgModel").hide(); 
	$("#popContentId").hide();
	$("#messageDivId").html("");
	
	location.reload();
}

function showCityNames(categoryName)
{
	var flag = 'true';
	
	
	$("#bgModel").show(); 
 	$("#loaderId").show();
 	
 	if(flag=='true')
	 {
   		 $.ajax({  
		 			
			     type : "post",   
			     url : "showCityNames", 
			     data :$('#searchForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#bgModel").hide(); 
			    	 $("#loaderId").hide();
			    	 
			    	 if(response.length>2)
			    	 {
			    		 var cityNames = response.split(",");
				    	 
				    	 $("#city").html("");
				    			 
				    	 $("#city").append("<option value='Select City'>Select City</option>")
				    	 
				    	 for(var i=0;i<cityNames.length;i++)
			    		 {
			    		 	$("#city").append("<option value='"+cityNames[i]+"'>"+cityNames[i]+"</option>");
			    		 }
			    	 }
			    	 else
			    	 {
			    		 
			    	 }
			    	 
			     },  
		     }); 
	 }
 	
}

function showValueList(CityName)
{
	var flag = 'true';
	
	var categoryName = $("#category").val();
	
	if(categoryName=="Select Category")
	{
		flag = "false";
		alert("Please select the category before choose city !");
	}
		
 	
 	if(flag=='true')
	{
 		
 		 $("#bgModel").show(); 
 	 	 $("#loaderId").show();
 	 	
 	 	
   		 $.ajax({  
		 			
			     type : "post",   
			     url : "showValueList", 
			     data :$('#searchForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#bgModel").hide(); 
			    	 $("#loaderId").hide();
			    	 
			    	 var resultvalues = response.split(",");
			    	 
			    	 $("#value").html("");
			    	 
			    	 $("#value").append("<option value='Select from the list'>Select form the list</option>");
			    	 $("#value").append("<option value='All>All</option>");
			    	 
			    	 for(var i=0;i<resultvalues.length;i++)
		    		 {
		    		 	$("#value").append("<option value='"+resultvalues[i]+"'>"+resultvalues[i]+"</option>");
		    		 }
			    	 
			     },  
		     }); 
	 }
}

function searchRequiredDetails(userStatus)
{
	var flag = 'true';
	
	if(userStatus!=0)
	{
		var categoryName = $("#category").val();
		
		if(categoryName=="Select Category")
		{
			flag = 'false';
			alert("Please select the category before choose city !");
		}
		
		if(categoryName!="Select Category")
		{
			var city = $("#city").val();
			
			if(city=="Select City")
			{
				flag = 'false';
				alert("Please select the category before choose city !");
			}
		}
		
	}
	else
	{
		flag = 'false';
		
		$("#authPopDiv").show();
		$("#popUpBgModel").show(); 
		$("#loginContentId").show();
		
	}
	
	if(flag=='true')
	{
 		$("#bgModel").show(); 
 	 	$("#loaderId").show();
 	 	
  		$.ajax({  
		 			
		     type : "post",   
		     url : "searchRequiredDetails", 
		     data :$('#searchForm').serialize(),	     	     	     
		     success : function(response) 
		     {  		
		    	 $("#bgModel").hide(); 
		    	 $("#loaderId").hide();
		    	 
		    	 $("#categoryResultHeading").html(response);
		     },  
	     }); 
	 }
	
 	
}



function viewMoreAboutCategory(id,type)
{
	$("#bgModel").show(); 
	$("#loaderId").show();
	 	
		 $.ajax({  
	 			
		     type : "post",   
		     url : "viewMoreAboutCategory", 
		     data : "id="+id+"&type="+type,	     	     	     
		     success : function(response) 
		     {  		
		    	 $("#bgModel").hide(); 
		    	 $("#loaderId").hide();
		    	 
		    	 $("#viewMoreDiv").html(response);
		    	 $("#categoryResultHeading")[0].scrollIntoView(true);
		     },  
	     }); 
}


function getSearchTouristPlace(placeId,count,uniId)
{
	$("#bgModel").show(); 
	$("#loaderId").show();
	 	
		 $.ajax({  
	 			
		     type : "post",   
		     url : "getSearchTouristPlace", 
		     data :"placeId="+placeId,	     	     	     
		     success : function(response) 
		     {  		
		    	 $("#bgModel").hide(); 
		    	 $("#loaderId").hide();
		    	 
		    	 $("#searchPlaceId").html(response);
		    	 
		    	 var element = document.querySelector("#aboutUs1");
			 		
		 		 if( (element.offsetHeight < element.scrollHeight) || (element.offsetWidth < element.scrollWidth)){
		 		   
		 			// your element have overflow		  
		 		    $("#seeMore1").show();
		 		 }	
		 		
		 		 
		 		 
		    	 $("#place"+uniId).addClass("active");
		    	 
		    	 for(var i=0;i<=count;i++)
		    	 {
		    		 if(uniId==i)
		    		 {
		    			 
		    		 }	
		    		 else
		    		 {		    			 
		    			 $("#place"+i).removeClass("active");
		    		 }
		    	 }
		    	 
		    	 
		     },  
	     });
}


function getSearchDoctor(doctorId,count,uniId)
{
	$("#bgModel").show(); 
	$("#loaderId").show();
	 	
		 $.ajax({  
	 			
		     type : "post",   
		     url : "getSearchDoctor", 
		     data :"doctorId="+doctorId,	     	     	     
		     success : function(response) 
		     {  		
		    	 $("#bgModel").hide(); 
		    	 $("#loaderId").hide();
		    	 
		    	 
		    	 
		    	 $("#searchDoctorId").html(response);
		    	 
		    	 var element = document.querySelector("#aboutUs1");
		 		
		 		 if( (element.offsetHeight < element.scrollHeight) || (element.offsetWidth < element.scrollWidth)){
		 		   
		 			// your element have overflow		  
		 		    $("#seeMore1").show();
		 		 }	
		 		
		 		 
		 		  
		 		 
		    	 $("#place"+uniId).addClass("active");
		    	 
		    	 for(var i=0;i<=count;i++)
		    	 {
		    		 if(uniId==i)
		    		 {
		    			 
		    		 }	
		    		 else
		    		 {		    			 
		    			 $("#place"+i).removeClass("active");
		    		 }
		    	 }
		    	 
		    	 
		     },  
	     });
}

function saveContactDetails()
{
	var flag = 'true';
	
	var alfanumeric = /^[a-zA-Z0-9]*$/;        	
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var phNo = /^[0-9]{10}$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	
    var fname = $("#contact_name").val();    
    var emailId = $("#contact_email_id").val();
    var mobileNo = $("#contact_phone").val();
    var reason = $("#contact_reason").val();    
    var subject = $("#contact_subject").val();
    var message = $("#contact_message").val();
    
    if(fname=="")
    {
    	flag = 'false';
    	$("#errNameId").show();
    	$("#errNameId").html("Please enter your full name")
    }
    
    if(fname!="")
    {
    	if(!fname.match(nameWithSpace))  
        {  		
			flag='false';
			$("#errNameId").show();
	        $("#errNameId").html("Only alphabets are allowed");		              
        }
		else
		{
			$("#errNameId ").hide();			
		}
    }
    
    if(emailId=="")
    {
    	flag = 'false';
    	$("#errEmailId").show();
    	$("#errEmailId").html("Please enter your email id")
    }
    
    if(emailId!="")
    {
    	if(!emailId.match(email))  
        {  		
			flag='false';
			$("#errEmailId").show();
	        $("#errEmailId").html("Enter a valid email id");		              
        }
		else
		{
			$("#errEmailId ").hide();			
		}
    }
    
    if(mobileNo=="")
    {
    	flag = 'false';
    	$("#errPhoneId").show();
    	$("#errPhoneId").html("Please enter your phone number")
    }
    
    if(mobileNo!="")
    {
    	$("#errPhoneId ").hide();					
    }
    
    if(reason=="Select reason for contact")
    {
    	flag = 'false';
    	$("#errReasonId").show();
    	$("#errReasonId").html("Please select reason for contact")
    }
    
    if(reason!="Select reason for contact")
    {
    	$("#errReasonId ").hide();					
    }
    
    if(subject=="")
    {
    	flag = 'false';
    	$("#errSubjectId").show();
    	$("#errSubjectId").html("Please provide a subject for contact")
    }
    
    if(subject!="")
    {
    	$("#errSubjectId ").hide();					
    }
    
    if(message=="")
    {
    	flag = 'false';
    	$("#errMessageId").show();
    	$("#errMessageId").html("Please describe your message for contact")
    }
    
    if(message!="")
    {
    	$("#errMessageId ").hide();					
    }
    
    if(flag=='true')
    {
    	$("#bgModel").show(); 
    	$("#loaderId").show();
    	 	
    		 $.ajax({  
    	 			
    		     type : "post",   
    		     url : "saveContactDetails",                                                                  
    		     data :$('#contactForm').serialize(),	     	     	     
    		     success : function(response) 
    		     {  		
    		    	 $("#bgModel").hide(); 
    		    	 $("#loaderId").hide();
    		    	 
    		    	 if(response=="success")
    		      		{		
    		    		    $("#popUpBgModel").show(); 
    		    			$("#popContentId").show();
    		    			$("#messageDivId").html("Your application have been successfully submitted.");
    		    							    
    		      		}
    		      	 else
    		      		{
    			      		$("#popUpBgModel").show(); 
    		    			$("#popContentId").show();
    		    			$("#messageDivId").html("There is some problem arise,Please try again later.");
    					 								    								    
    		      		}
    		    	
    		     },  
    	     });
    }
    
	
}



function getDoctorList(specialization)
{
	var flag = 'true';
	
	if(specialization=="Select Category")
	{
		flag = "false";
		alert("Please select the treatment on before choose doctor !");
	}
		
 	if(flag=='true')
	{
 		
 		$("#bgModel").show(); 
 	 	$("#loaderId").show();
 	 	
 	 	
   		 $.ajax({  
		 			
			     type : "post",   
			     url : "getDoctorList", 
			     data :"specialization="+specialization,	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#bgModel").hide(); 
			    	 $("#loaderId").hide();
			    	 
			    	 var resultvalues = response.split(",");
			    	 
			    	 $("#doctor_id").html("");
			    	 
			    	 $("#doctor_id").append("<option value='Select Doctor'>Select Doctor</option>");
			    	 
			    	 for(var i=0;i<resultvalues.length;i++)
		    		 {
		    		 	$("#doctor_id").append("<option value='"+resultvalues[i]+"'>"+resultvalues[i]+"</option>");
		    		 }
			     },  
		     }); 
	 }
}



function saveSecondOpinion(access)
{
	var flag = 'true';

	var specialization = $("#treatment_on").val();
	var doctor = $("#doctor_id").val();
	var date = $("#some_class_1").val();
	var remark = $("#remark").val();
	
	if(specialization=="Select Treatment On")
	{
		flag = 'false';
		$("#errSpecializationId").show();
		$("#errSpecializationId").html("Select medial specialization")
	}
	
	if(specialization!="Select Treatment On")
	{
		$("#errSpecializationId").hide();
	}
	
	if(doctor=="Select Doctor")
	{
		flag = 'false';
		$("#errDoctorId").show();
		$("#errDoctorId").html("Select a doctor")
	}
	
	if(doctor!="Select Doctor")
	{
		$("#errDoctorId").hide();
	}
	
	if(date=="")
	{
		flag = 'false';
		$("#errDateId").show();
		$("#errDateId").html("Select an appointment date")
	}
	
	if(date!="")
	{
		$("#errDateId").hide();
	}
	
	if(remark=="")
	{
		flag = 'false';
		$("#errRemarkId").show();
		$("#errRemarkId").html("Write remark")
	}
	
	if(remark!="")
	{
		$("#errRemarkId").hide();
	}
	
	

	if(flag=='true')
	{
		if(access==0)
		{			
			$("#authPopDiv").show();
			$("#popUpBgModel").show(); 
			$("#loginContentId").show();
			
			/*alert("Please sign in to submit your opinion application !");*/
		}
		else
		{
			$("#bgModel").show(); 
			$("#loaderId").show();
			 	
				 $.ajax({  
			 			
				     type : "post",   
				     url : "saveSecondOpinion", 
				     data :$('#opinionForm').serialize(),	     	     	     
				     success : function(response) 
				     {  		
				    	 $("#bgModel").hide(); 
				    	 $("#loaderId").hide();
				    	 
				    	 if(response=="success")
				      		{		
				    		    $("#popUpBgModel").show(); 
				    			$("#popContentId").show();
				    			$("#messageDivId").html("Your application have been successfully submitted.");
				    							    
				      		}
				      	 else
				      		{
					      		$("#popUpBgModel").show(); 
				    			$("#popContentId").show();
				    			$("#messageDivId").html("There is some problem arise,Please try again later.");
							 								    								    
				      		}
				    	
				     },  
			     });
		}
		
		
	}
	

}


function changePassword()
{
	var flag = 'true';

	var curPassword = $("#exist_password").val();
	var newPassword = $("#new_password").val();
	var cnfPassword = $("#cnf_password").val();
	
	if(curPassword=="")
	{
		flag = 'false';
		$("#errCurPasswordId").show();
		$("#errCurPasswordId").html("Enter your current password.");
	}
	
	if(curPassword!="")
	{
		$("#errCurPasswordId").hide();		
	}
	
	if(newPassword=="")
	{
		flag = 'false';
		$("#errPasswordId").show();
		$("#errPasswordId").html("Enter your new password.");
	}
	
	if(newPassword!="")
	{
		$("#errPasswordId").hide();		
	}
	
	if(cnfPassword=="")
	{
		flag = 'false';
		$("#errCnfPasswordId").show();
		$("#errCnfPasswordId").html("Please confirm your new password.");
	}
	
	if(cnfPassword!="")
	{
		$("#errCnfPasswordId").hide();		
	}
	
	if(newPassword!=cnfPassword)
	{
		flag = 'false';
		$("#errCnfPasswordId").show();
		$("#errCnfPasswordId").html("Confirm password is not matched with new password.");
	}
	
	
	if(flag=='true')
	{
		$("#bgModel").show(); 
		$("#loaderId").show();
		 	
			 $.ajax({  
		 			
			     type : "post",   
			     url : "updatePassword", 
			     data :$('#changePasswordForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#bgModel").hide(); 
			    	 $("#loaderId").hide();
			    	 
			    	 if(response=="success")
			      		{		
			    		    $("#popUpBgModel").show(); 
			    			$("#popContentId").show();
			    			$("#messageDivId").html("Your password have been successfully updated.");			    							    
			      		}
			    	 else if(response=="NotMatched")
			    		{
			    		    flag = 'false';
			    			$("#errCurPasswordId").show();
			    			$("#errCurPasswordId").html("Incorrect current password");
			    	    }
			      	 else
			      		{
				      		$("#popUpBgModel").show(); 
			    			$("#popContentId").show();
			    			$("#messageDivId").html("There is some problem arise,Please try again later.");
						 								    								    
			      		}
			    	
			     },  
		     });
	}
	
}


function genForgetPasswordPattern()
{
	var flag = 'true';

	var emailId = $("#user_emailid").val();
	
	if(emailId=="")
	{
		flag = 'false';
		$("#errEmailId").show();
		$("#errEmailId").html("Enter your registered email id.");
	}
	
	if(emailId!="")
	{
		$("#errEmailId").hide();		
	}
	
	if(flag=='true')
	{
		$("#bgModel").show(); 
		$("#loaderId").show();
		 	
			 $.ajax({  
		 			
			     type : "post",   
			     url : "genForgetPasswordPattern", 
			     data :"emailId="+emailId,	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#bgModel").hide(); 
			    	 $("#loaderId").hide();
			    	 
			    	 if(response=="success")
			      		{		
			    		    $("#popUpBgModel").show(); 
			    			$("#popContentId").show();
			    			$("#messageDivId").html("We have sent a link please follow to reset the password.");			    							    
			      		}
			    	 else if(response=="NotExist")
			    		{
			    			$("#errEmailId").show();
			    			$("#errEmailId").html("This email id is not registered yet.");
			    	    }
			      	 else
			      		{
				      		$("#popUpBgModel").show(); 
			    			$("#popContentId").show();
			    			$("#messageDivId").html("There is some problem arise,Please try again later.");
						 								    								    
			      		}
			    	
			     },  
		     });
	}
}



function saveResetPassword()
{
	var flag = 'true';

	var newPassword = $("#new_password").val();
	var cnfPassword = $("#cnf_password").val();
	
	
	if(newPassword=="")
	{
		flag = 'false';
		$("#errPasswordId").show();
		$("#errPasswordId").html("Enter your new password.");
	}
	
	if(newPassword!="")
	{
		$("#errPasswordId").hide();		
	}
	
	if(cnfPassword=="")
	{
		flag = 'false';
		$("#errCnfPasswordId").show();
		$("#errCnfPasswordId").html("Please confirm your new password.");
	}
	
	if(cnfPassword!="")
	{
		$("#errCnfPasswordId").hide();		
	}
	
	if(newPassword!=cnfPassword)
	{
		flag = 'false';
		$("#errCnfPasswordId").show();
		$("#errCnfPasswordId").html("Confirm password is not matched with new password.");
	}
	
	
	if(flag=='true')
	{
		$("#bgModel").show(); 
		$("#loaderId").show();
		 	
			 $.ajax({  
		 			
			     type : "post",   
			     url : "saveUserResetPassword", 
			     data :$('#resetPasswordForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#bgModel").hide(); 
			    	 $("#loaderId").hide();
			    	 
			    	 if(response=="success")
			      		{		
			    		    $("#popUpBgModel").show(); 
			    			$("#popContentId").show();
			    			$("#messageDivId").html("Your password have been successfully reseted.");			    							    
			      		}			    	 
			      		{
				      		$("#popUpBgModel").show(); 
			    			$("#popContentId").show();
			    			$("#messageDivId").html("There is some problem arise,Please try again later.");
						 								    								    
			      		}
			    	
			     },  
		     });
	}
}



function showRegistrationDiv()
{	
	$("#loginContentId").hide();
	$("#registrationContentId").show();
}

function showLoginDiv()
{	
	$("#loginContentId").show();
	$("#registrationContentId").hide();
}

function closeAuthPop()
{
	$("#authPopDiv").hide();
	$("#popUpBgModel").hide();
	
}


function closeAlertDiv()
{
	showLoginDiv();
}


function checkPopLogin()
{
	var flag='true';
	
	var emailId = $("#email_id").val();   
    var password = $("#password").val();
    
    if(emailId=="")
    {
    	flag = 'false';
    	$("#errLoginEmailId").show();
    	$("#errLoginEmailId").html("Please enter your email id.");
    }
    
    if(emailId!="")
    {
    	$("#errLoginEmailId").hide();
    }
    
    if(password=="")
    {
    	flag = 'false';
    	$("#errLoginPasswordId").show();
    	$("#errLoginPasswordId").html("Please enter password.");
    }
    
    if(password!="")
    {
    	$("#errLoginPasswordId").hide();
    }
    
    if(flag=='true')
    {
    	$("#bgModel").show(); 
	 	$("#loaderId").show();
		    	  
			     $.ajax({  
			 			
				     type : "post",   
				     url : "checkPopLogin", 
				     data :$('#loginForm').serialize(),	     	     	     
				     success : function(response) 
				     {  		
				    	 $("#bgModel").hide(); 
				    	 $("#loaderId").hide();
				    	 
				    	 var resultPattern = response.split("/");
				    	 
				    	 if(resultPattern[1]=="Active")
				      		{	
				    		 	$("#authPopDiv").hide();
				    			$("#popUpBgModel").hide(); 
				    			$("#loginContentId").hide();
				    			
				    			refreshHeaderDiv(resultPattern[0]);
				    			
				      		}
				    	 else if(resultPattern[1]=="Invalid")
				      		{					    		    
				    		 	$("#authMessage").show();
				    		 	$("#authMessage").html("Invalid credential !");				    		    							 	
				      		}	
				      	 else 
				      		{
				      		 	$("#authMessage").show();
				      		 	$("#authMessage").html("Account was blocked !");												 								    								    
				      		}	
				    	 
				     },  
			     }); 
    }
    
    
}

function refreshHeaderDiv(userId)
{
	$("#bgModel").show(); 
 	$("#loaderId").show();
	    	  
     $.ajax({  
 			
	     type : "post",   
	     url : "refreshHeaderDiv", 
	     data : "userId="+userId,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#bgModel").hide(); 
	    	 $("#loaderId").hide();
	    	 
	    	 $("#headerDiv").html(response);
	    	 
	    	 refreshSideDiv(userId)
	     },  
     }); 
}


function refreshSideDiv(userId)
{
	$("#bgModel").show(); 
 	$("#loaderId").show();
	    	  
     $.ajax({  
 			
	     type : "post",   
	     url : "refreshSideDiv", 
	     data : "userId="+userId,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#bgModel").hide(); 
	    	 $("#loaderId").hide();
	    	 
	    	 $("#sideDiv").html(response);
	    	 
	    	 refreshLowerDiv(userId);
	     },  
     }); 
}


function refreshLowerDiv(userId)
{
	$("#bgModel").show(); 
 	$("#loaderId").show();
	    	  
     $.ajax({  
 			
	     type : "post",   
	     url : "refreshLowerDiv", 
	     data : "userId="+userId,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#bgModel").hide(); 
	    	 $("#loaderId").hide();
	    	 
	    	 $("#lowerDiv").html(response);
	    	 
	     },  
     }); 
}



function regPopUpReg()
{
	var flag='true';
	
	var alfanumeric = /^[a-zA-Z0-9]*$/;        	
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var phNo = /^[0-9]{10}$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	
    var fname = $("#user_full_name").val();    
    var emailId = $("#user_emailid").val();
    var mobileNo = $("#user_mobile").val();
    var password = $("#user_password").val();
    var confirmPassword = $("#cnfpassword").val();
	
    if(fname=="")
	{
    	flag='false';
		$("#errNameId").show();
        $("#errNameId").html("Please enter your full name.");	
	}
    
    if(fname!="")
	{
		if(!fname.match(nameWithSpace))  
        {  		
			flag='false';
			$("#errNameId").show();
	        $("#errNameId").html("Only alphabets are allowed.");		              
        }
		else
		{
			$("#errNameId ").hide();			
		}
	}
    
    if(emailId=="")
	{
    	flag='false';
		$("#errEmailId").show();
        $("#errEmailId").html("Please enter your email id.");
	}
    
    if(emailId!="")
	{
		if(!emailId.match(email))  
        {  		
			flag='false';
			$("#errEmailId").show();
	        $("#errEmailId").html("Please enter a valid email id.");	       
        }
		else
		{
			$("#errEmailId").hide();
		}
	}
    
    if(mobileNo=="")
	{
    	flag='false';
		$("#errMobileId").show();
        $("#errMobileId").html("Please enter your mobile no.");	   
	}
    
    if(mobileNo!="")
	{
		if(!mobileNo.match(phNo))  
        {  		
			flag='false';
			$("#errMobileId").show();
	        $("#errMobileId").html("Please enter a valid mobile no.");	      
        }
		else
		{
			$("#errMobileId").hide();
		}
	}
	
    if(country=="Select Country")
    {
    	flag='false';
    	$("#errCountryId").show();
    	$("#errCountryId").html("Select your country.");
    }
    
    if(country!="Select Country")
    {
    	$("#errCountryId").hide();
    }
    
    if(password=="")
	{
    	flag='false';
		$("#errPasswordId").show();
        $("#errPasswordId").html("Please define a password.");	
	}
    
    if(password!="")
	{
		$("#errPasswordId").hide();
	}
   
    if(confirmPassword=="")
	{
    	flag='false';
		$("#errCnfPasswordId").show();
        $("#errCnfPasswordId").html("Confirm password not matched with password.");
	}
    
    if(confirmPassword!="")
	{
		$("#errCnfPasswordId").hide();
	}
   
    if(password!=confirmPassword)
	{
    	flag='false';
		$("#errCnfPasswordId").show();
        $("#errCnfPasswordId").html("Confirm password not matched with password.");
	}
    
    
    if(flag=='true')
	{  	
    	
    	 $("#bgModel").show(); 
    	 $("#loaderId").show();
    	     	 
    	 $.ajax({  
	 			
		     type : "post",   
		     url : "checkUserEmailId", 
		     data :$('#userRegForm').serialize(),	     	     	     
		     success : function(response) 
		     {  		
		    	 $("#bgModel").hide(); 
		    	 $("#loaderId").hide();
		    	 
				 if(response=="NotExist")
					 {			
					    
					     $.ajax({  
					 			
						     type : "post",   
						     url : "checkUserPhno", 
						     data :$('#userRegForm').serialize(),	     	     	     
						     success : function(response) 
						     {  		
						    	 $("#bgModel").hide(); 
						    	 $("#loaderId").hide();
						    	 
								 if(response=="NotExist")
								 {			
									 	$("#bgModel").show(); 
									 	$("#loaderId").show();
										    	  
											     $.ajax({  
											 			
												     type : "post",   
												     url : "saveUserRegisterPopUp", 
												     data :$('#regForm').serialize(),	     	     	     
												     success : function(response) 
												     {  		
												    	 $("#bgModel").hide(); 
												    	 $("#loaderId").hide();
												    	 
												    	 if(response=="success")
												      		{	
												    		 
												    		    $("#popUpBgModel").show(); 
												    			$("#popAlertId").show();
												    			$("#alertDivId").html("Your have been successfully registered.");
															 	
												      		}
												      	else
												      		{
													      		$("#popUpBgModel").show(); 
												    			$("#popAlertId").show();
												    			$("#alertDivId").html("There is some problem arise,Please try again later.");												 								    								    
												      		}	
												    	 
												     },  
											     }); 
							      		
									 }
								 else
									 {		
										$("#bgModel").hide(); 
										$("#loaderId").hide();
								    	
										$("#errMobileId").show();
								        $("#errMobileId").html("Given mobile no is already register.");	     
										
									 }
						     },  
					     }); 
					 }
				 else
					 {							 	
					 	$("#bgModel").hide(); 
					 	$("#loaderId").hide();
				    	 
					 	$("#errEmailId").show();
				        $("#errEmailId").html("Given emailId is already registered.");	     
						
					 }
		     },  
	     }); 
    	 
	}
    
}


function loadAboutUs(count,uniId)
{
	
	for(var i=1;i<=count;i++)
	{
		
		var element = document.querySelector('#aboutUs'+i);
		
		if( (element.offsetHeight < element.scrollHeight) || (element.offsetWidth < element.scrollWidth)){
		   
			// your element have overflow		  
		    $("#seeMore"+i).show();
		}	
		else
		{			 
			//your element don't have overflow
		}
	}
	
}



function showMoreAboutUs(serialId,uniId)
{
	/*$('#section'+uniId)[0].scrollIntoView(true);*/
	
	var element = document.querySelector('#aboutUs'+uniId);
	
	var condition = element.style.height;
	
	if(condition=="183px")
	{
		element.style.height = "auto";
		$('#seeMore'+uniId).html("Close")
	}
	else
	{
		element.style.height = "183px";
		$('#seeMore'+uniId).html("See more")
	}
	
	
}

