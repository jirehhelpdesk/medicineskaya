package com.medicineskaya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="restaurent_details")
public class RestaurentModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="restaurent_id")
	private int restaurent_id;
	
	@Column(name="restaurent_name")
	private String restaurent_name;
	
	@Column(name="restaurent_address")
	private String restaurent_address;
	
	@Column(name="restaurent_email_id")
	private String restaurent_email_id;
	
	@Column(name="restaurent_phone")
	private String restaurent_phone;
	
	@Column(name="restaurent_photo")
	private String restaurent_photo;
	
	@Column(name="restaurent_city")
	private String restaurent_city;
	
	@Column(name="type_of_food")
	private String type_of_food;
	
	@Column(name="cr_date")
	private Date cr_date;

	
	
	
	public int getRestaurent_id() {
		return restaurent_id;
	}

	public void setRestaurent_id(int restaurent_id) {
		this.restaurent_id = restaurent_id;
	}

	public String getRestaurent_name() {
		return restaurent_name;
	}

	public void setRestaurent_name(String restaurent_name) {
		this.restaurent_name = restaurent_name;
	}

	public String getRestaurent_address() {
		return restaurent_address;
	}

	public void setRestaurent_address(String restaurent_address) {
		this.restaurent_address = restaurent_address;
	}

	public String getRestaurent_email_id() {
		return restaurent_email_id;
	}

	public void setRestaurent_email_id(String restaurent_email_id) {
		this.restaurent_email_id = restaurent_email_id;
	}

	public String getRestaurent_phone() {
		return restaurent_phone;
	}

	public void setRestaurent_phone(String restaurent_phone) {
		this.restaurent_phone = restaurent_phone;
	}

	public String getRestaurent_photo() {
		return restaurent_photo;
	}

	public void setRestaurent_photo(String restaurent_photo) {
		this.restaurent_photo = restaurent_photo;
	}

	public String getRestaurent_city() {
		return restaurent_city;
	}

	public void setRestaurent_city(String restaurent_city) {
		this.restaurent_city = restaurent_city;
	}

	public String getType_of_food() {
		return type_of_food;
	}

	public void setType_of_food(String type_of_food) {
		this.type_of_food = type_of_food;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

}
