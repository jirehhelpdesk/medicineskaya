package com.medicineskaya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="second_opinion_details")
public class OpinionDetails {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="opinion_id")
	private int opinion_id;
	
	@Column(name="treatment_on")
	private String treatment_on;
	
	@Column(name="doctor_id")
	private String doctor_id;
	
	@Column(name="appointment_date")
	private Date appointment_date;
	
	@Column(name="remark")
	private String remark;
	
	@Column(name="cr_date")
	private Date cr_date;

	@Column(name="user_id")
	private int user_id;
	
	
	public int getOpinion_id() {
		return opinion_id;
	}

	public void setOpinion_id(int opinion_id) {
		this.opinion_id = opinion_id;
	}

	public String getTreatment_on() {
		return treatment_on;
	}

	public void setTreatment_on(String treatment_on) {
		this.treatment_on = treatment_on;
	}

	public String getDoctor_id() {
		return doctor_id;
	}

	public void setDoctor_id(String doctor_id) {
		this.doctor_id = doctor_id;
	}

	public Date getAppointment_date() {
		return appointment_date;
	}

	public void setAppointment_date(Date appointment_date) {
		this.appointment_date = appointment_date;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	
	
}
