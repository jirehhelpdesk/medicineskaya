package com.medicineskaya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="hospital_details")
public class HospitalModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="hospital_id")
	private int hospital_id;
	
	@Column(name="hospital_name")
	private String hospital_name;
	
	@Column(name="hospital_address")
	private String hospital_address;
	
	@Column(name="hospital_email_id")
	private String hospital_email_id;
	
	@Column(name="hospital_phone")
	private String hospital_phone;
	
	@Column(name="hospital_photo")
	private String hospital_photo;
	
	@Column(name="hospital_city")
	private String hospital_city;
	
	@Column(name="cr_date")
	private Date cr_date;

	
	
	
	public int getHospital_id() {
		return hospital_id;
	}

	public void setHospital_id(int hospital_id) {
		this.hospital_id = hospital_id;
	}

	public String getHospital_name() {
		return hospital_name;
	}

	public void setHospital_name(String hospital_name) {
		this.hospital_name = hospital_name;
	}

	public String getHospital_address() {
		return hospital_address;
	}

	public void setHospital_address(String hospital_address) {
		this.hospital_address = hospital_address;
	}

	public String getHospital_email_id() {
		return hospital_email_id;
	}

	public void setHospital_email_id(String hospital_email_id) {
		this.hospital_email_id = hospital_email_id;
	}

	public String getHospital_phone() {
		return hospital_phone;
	}

	public void setHospital_phone(String hospital_phone) {
		this.hospital_phone = hospital_phone;
	}

	public String getHospital_photo() {
		return hospital_photo;
	}

	public void setHospital_photo(String hospital_photo) {
		this.hospital_photo = hospital_photo;
	}

	public String getHospital_city() {
		return hospital_city;
	}

	public void setHospital_city(String hospital_city) {
		this.hospital_city = hospital_city;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	
}
