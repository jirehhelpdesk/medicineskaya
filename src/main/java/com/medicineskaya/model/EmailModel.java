package com.medicineskaya.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sent_email_records")
public class EmailModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="email_sl_id")
	private int email_sl_id;
	
	@Column(name="full_name")
	private String full_name;
	
	@Column(name="email_id")
	private String email_id;
	
	@Column(name="email_subject")
	private String email_subject;
	
	@Column(name="email_content")
	private String email_content;
	
	@Column(name="email_status")
	private String email_status;
	
	@Column(name="cr_date")
	private Date cr_date;

	
	public int getEmail_sl_id() {
		return email_sl_id;
	}

	public void setEmail_sl_id(int email_sl_id) {
		this.email_sl_id = email_sl_id;
	}

	
	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getEmail_id() {
		return email_id;
	}

	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}

	public String getEmail_subject() {
		return email_subject;
	}

	public void setEmail_subject(String email_subject) {
		this.email_subject = email_subject;
	}

	public String getEmail_content() {
		return email_content;
	}

	public void setEmail_content(String email_content) {
		this.email_content = email_content;
	}

	public String getEmail_status() {
		return email_status;
	}

	public void setEmail_status(String email_status) {
		this.email_status = email_status;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

}
