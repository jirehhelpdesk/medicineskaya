package com.medicineskaya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="doctor_details")
public class DoctorsModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="doctor_id")
	private int doctor_id;
	
	@Column(name="doctor_name")
	private String doctor_name;
	
	@Column(name="doctor_address")
	private String doctor_address;
	
	@Column(name="doctor_email_id")
	private String doctor_email_id;
	
	@Column(name="doctor_phone")
	private String doctor_phone;
	
	@Column(name="doctor_photo")
	private String doctor_photo;
	
	@Column(name="doctor_city")
	private String doctor_city;
	
	@Column(name="gender")
	private String gender;
	
	@Column(name="doctor_specialist")
	private String doctor_specialist;
	
	@Column(name="cr_date")
	private Date cr_date;

	
	
	
	public int getDoctor_id() {
		return doctor_id;
	}

	public void setDoctor_id(int doctor_id) {
		this.doctor_id = doctor_id;
	}

	public String getDoctor_name() {
		return doctor_name;
	}

	public void setDoctor_name(String doctor_name) {
		this.doctor_name = doctor_name;
	}

	public String getDoctor_address() {
		return doctor_address;
	}

	public void setDoctor_address(String doctor_address) {
		this.doctor_address = doctor_address;
	}

	public String getDoctor_email_id() {
		return doctor_email_id;
	}

	public void setDoctor_email_id(String doctor_email_id) {
		this.doctor_email_id = doctor_email_id;
	}

	public String getDoctor_phone() {
		return doctor_phone;
	}

	public void setDoctor_phone(String doctor_phone) {
		this.doctor_phone = doctor_phone;
	}

	public String getDoctor_photo() {
		return doctor_photo;
	}

	public void setDoctor_photo(String doctor_photo) {
		this.doctor_photo = doctor_photo;
	}

	public String getDoctor_city() {
		return doctor_city;
	}

	public void setDoctor_city(String doctor_city) {
		this.doctor_city = doctor_city;
	}
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDoctor_specialist() {
		return doctor_specialist;
	}

	public void setDoctor_specialist(String doctor_specialist) {
		this.doctor_specialist = doctor_specialist;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	
}
