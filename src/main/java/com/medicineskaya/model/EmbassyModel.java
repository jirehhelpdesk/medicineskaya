package com.medicineskaya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="embassy_details")
public class EmbassyModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="embassy_id")
	private int embassy_id;
	
	@Column(name="embassy_name")
	private String embassy_name;
	
	@Column(name="embassy_address")
	private String embassy_address;
	
	@Column(name="embassy_email_id")
	private String embassy_email_id;
	
	@Column(name="embassy_phone")
	private String embassy_phone;
	
	@Column(name="embassy_photo")
	private String embassy_photo;
	
	@Column(name="embassy_city")
	private String embassy_city;
	
	@Column(name="cr_date")
	private Date cr_date;

	
	
	
	public int getEmbassy_id() {
		return embassy_id;
	}

	public void setEmbassy_id(int embassy_id) {
		this.embassy_id = embassy_id;
	}

	public String getEmbassy_name() {
		return embassy_name;
	}

	public void setEmbassy_name(String embassy_name) {
		this.embassy_name = embassy_name;
	}

	public String getEmbassy_address() {
		return embassy_address;
	}

	public void setEmbassy_address(String embassy_address) {
		this.embassy_address = embassy_address;
	}

	public String getEmbassy_email_id() {
		return embassy_email_id;
	}

	public void setEmbassy_email_id(String embassy_email_id) {
		this.embassy_email_id = embassy_email_id;
	}

	public String getEmbassy_phone() {
		return embassy_phone;
	}

	public void setEmbassy_phone(String embassy_phone) {
		this.embassy_phone = embassy_phone;
	}

	public String getEmbassy_photo() {
		return embassy_photo;
	}

	public void setEmbassy_photo(String embassy_photo) {
		this.embassy_photo = embassy_photo;
	}

	public String getEmbassy_city() {
		return embassy_city;
	}

	public void setEmbassy_city(String embassy_city) {
		this.embassy_city = embassy_city;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	
}
