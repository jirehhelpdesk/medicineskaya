package com.medicineskaya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="service_details")
public class TaxiServiceModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="service_id")
	private int service_id;
	
	@Column(name="service_name")
	private String service_name;
	
	@Column(name="service_type")
	private String service_type;
	
	@Column(name="service_address")
	private String service_address;
	
	@Column(name="service_email_id")
	private String service_email_id;
	
	@Column(name="service_phone")
	private String service_phone;
	
	@Column(name="service_photo")
	private String service_photo;
	
	@Column(name="service_city")
	private String service_city;
	
	@Column(name="service_website")
	private String service_website;
	
	@Column(name="cr_date")
	private Date cr_date;

	
	
	
	public int getService_id() {
		return service_id;
	}

	public void setService_id(int service_id) {
		this.service_id = service_id;
	}

	public String getService_name() {
		return service_name;
	}

	public void setService_name(String service_name) {
		this.service_name = service_name;
	}
	
	public String getService_type() {
		return service_type;
	}

	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	public String getService_address() {
		return service_address;
	}

	public void setService_address(String service_address) {
		this.service_address = service_address;
	}

	public String getService_email_id() {
		return service_email_id;
	}

	public void setService_email_id(String service_email_id) {
		this.service_email_id = service_email_id;
	}

	public String getService_phone() {
		return service_phone;
	}

	public void setService_phone(String service_phone) {
		this.service_phone = service_phone;
	}

	public String getService_photo() {
		return service_photo;
	}

	public void setService_photo(String service_photo) {
		this.service_photo = service_photo;
	}

	public String getService_city() {
		return service_city;
	}

	public void setService_city(String service_city) {
		this.service_city = service_city;
	}

	public String getService_website() {
		return service_website;
	}

	public void setService_website(String service_website) {
		this.service_website = service_website;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	
}
