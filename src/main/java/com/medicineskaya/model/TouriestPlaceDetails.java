package com.medicineskaya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tourist_place_details")
public class TouriestPlaceDetails {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="place_id")
	private int place_id;
	
	@Column(name="place_name")
	private String place_name;
	
	@Column(name="location")
	private String location;
	
	@Column(name="about_place")
	private String about_place;
	
	@Column(name="place_photo")
	private String place_photo;
	
	@Column(name="cr_date")
	private Date cr_date;

	@Column(name="other_info1")
	private String other_info1;

	
	
	
	public int getPlace_id() {
		return place_id;
	}

	public void setPlace_id(int place_id) {
		this.place_id = place_id;
	}

	public String getPlace_name() {
		return place_name;
	}

	public void setPlace_name(String place_name) {
		this.place_name = place_name;
	}

	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAbout_place() {
		return about_place;
	}

	public void setAbout_place(String about_place) {
		this.about_place = about_place;
	}

	public String getPlace_photo() {
		return place_photo;
	}

	public void setPlace_photo(String place_photo) {
		this.place_photo = place_photo;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getOther_info1() {
		return other_info1;
	}

	public void setOther_info1(String other_info1) {
		this.other_info1 = other_info1;
	}
	
}
