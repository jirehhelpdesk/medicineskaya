package com.medicineskaya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="category_details")
public class CategoryDetails {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="category_id")
	private int category_id;
	
	@Column(name="category_name")
	private String category_name;
	
	@Column(name="cr_date")
	private Date cr_date;

	@Column(name="category_access_level")
	private String category_access_level;
	
	@Column(name="category_info_1")
	private String category_info_1;
	
	@Column(name="category_info_2")
	private String category_info_2;
	
	
	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getCategory_access_level() {
		return category_access_level;
	}

	public void setCategory_access_level(String category_access_level) {
		this.category_access_level = category_access_level;
	}

	public String getCategory_info_1() {
		return category_info_1;
	}

	public void setCategory_info_1(String category_info_1) {
		this.category_info_1 = category_info_1;
	}

	public String getCategory_info_2() {
		return category_info_2;
	}

	public void setCategory_info_2(String category_info_2) {
		this.category_info_2 = category_info_2;
	}

	
}
