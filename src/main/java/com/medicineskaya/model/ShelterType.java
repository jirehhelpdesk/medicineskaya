package com.medicineskaya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="shelter_type")
public class ShelterType {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="shelter_id")
	private int shelter_id;
	
	@Column(name="shelter_type")
	private String shelter_type;
	
	@Column(name="cr_date")
	private Date cr_date;

	@Column(name="info_1")
	private String info_1;
	
	@Column(name="info_2")
	private String info_2;

	
	
	public int getShelter_id() {
		return shelter_id;
	}

	public void setShelter_id(int shelter_id) {
		this.shelter_id = shelter_id;
	}

	public String getShelter_type() {
		return shelter_type;
	}

	public void setShelter_type(String shelter_type) {
		this.shelter_type = shelter_type;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getInfo_1() {
		return info_1;
	}

	public void setInfo_1(String info_1) {
		this.info_1 = info_1;
	}

	public String getInfo_2() {
		return info_2;
	}

	public void setInfo_2(String info_2) {
		this.info_2 = info_2;
	}
	
	
	
}
