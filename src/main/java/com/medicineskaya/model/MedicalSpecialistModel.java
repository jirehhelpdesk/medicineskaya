package com.medicineskaya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="medical_specialist_details")
public class MedicalSpecialistModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="specialist_id")
	private int specialist_id;
	
	@Column(name="specialist_type")
	private String specialist_type;
	
	@Column(name="cr_date")
	private Date cr_date;

	@Column(name="specialist_info_1")
	private String specialist_info_1;
	
	@Column(name="specialist_info_2")
	private String specialist_info_2;

	
	
	
	public int getSpecialist_id() {
		return specialist_id;
	}

	public void setSpecialist_id(int specialist_id) {
		this.specialist_id = specialist_id;
	}

	public String getSpecialist_type() {
		return specialist_type;
	}

	public void setSpecialist_type(String specialist_type) {
		this.specialist_type = specialist_type;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getSpecialist_info_1() {
		return specialist_info_1;
	}

	public void setSpecialist_info_1(String specialist_info_1) {
		this.specialist_info_1 = specialist_info_1;
	}

	public String getSpecialist_info_2() {
		return specialist_info_2;
	}

	public void setSpecialist_info_2(String specialist_info_2) {
		this.specialist_info_2 = specialist_info_2;
	}
	
	
}
