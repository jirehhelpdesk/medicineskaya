package com.medicineskaya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="attender_nurse_details")
public class AttenderNurseModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="attender_id")
	private int attender_id;
	
	@Column(name="attender_name")
	private String attender_name;
	
	@Column(name="attender_address")
	private String attender_address;
	
	@Column(name="attender_email_id")
	private String attender_email_id;
	
	@Column(name="attender_phone")
	private String attender_phone;
	
	@Column(name="attender_photo")
	private String attender_photo;
	
	@Column(name="attender_city")
	private String attender_city;
	
	@Column(name="attender_gender")
	private String attender_gender;
	
	@Column(name="cr_date")
	private Date cr_date;

	
	
	
	
	public int getAttender_id() {
		return attender_id;
	}

	public void setAttender_id(int attender_id) {
		this.attender_id = attender_id;
	}

	public String getAttender_name() {
		return attender_name;
	}

	public void setAttender_name(String attender_name) {
		this.attender_name = attender_name;
	}

	public String getAttender_address() {
		return attender_address;
	}

	public void setAttender_address(String attender_address) {
		this.attender_address = attender_address;
	}

	public String getAttender_email_id() {
		return attender_email_id;
	}

	public void setAttender_email_id(String attender_email_id) {
		this.attender_email_id = attender_email_id;
	}

	public String getAttender_phone() {
		return attender_phone;
	}

	public void setAttender_phone(String attender_phone) {
		this.attender_phone = attender_phone;
	}

	public String getAttender_photo() {
		return attender_photo;
	}

	public void setAttender_photo(String attender_photo) {
		this.attender_photo = attender_photo;
	}

	public String getAttender_city() {
		return attender_city;
	}

	public void setAttender_city(String attender_city) {
		this.attender_city = attender_city;
	}

	public String getAttender_gender() {
		return attender_gender;
	}

	public void setAttender_gender(String attender_gender) {
		this.attender_gender = attender_gender;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	
}
