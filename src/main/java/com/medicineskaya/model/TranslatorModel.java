package com.medicineskaya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="translator_details")
public class TranslatorModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="translator_id")
	private int translator_id;
	
	@Column(name="translator_name")
	private String translator_name;
	
	@Column(name="translator_address")
	private String translator_address;
	
	@Column(name="translator_email_id")
	private String translator_email_id;
	
	@Column(name="translator_phone")
	private String translator_phone;
	
	@Column(name="translator_photo")
	private String translator_photo;
	
	@Column(name="translator_city")
	private String translator_city;
	
	@Column(name="translator_gender")
	private String translator_gender;
	
	@Column(name="known_language")
	private String known_language;
	
	@Column(name="cr_date")
	private Date cr_date;

	
	
	
	public int getTranslator_id() {
		return translator_id;
	}

	public void setTranslator_id(int translator_id) {
		this.translator_id = translator_id;
	}

	public String getTranslator_name() {
		return translator_name;
	}

	public void setTranslator_name(String translator_name) {
		this.translator_name = translator_name;
	}

	public String getTranslator_address() {
		return translator_address;
	}

	public void setTranslator_address(String translator_address) {
		this.translator_address = translator_address;
	}

	public String getTranslator_email_id() {
		return translator_email_id;
	}

	public void setTranslator_email_id(String translator_email_id) {
		this.translator_email_id = translator_email_id;
	}

	public String getTranslator_phone() {
		return translator_phone;
	}

	public void setTranslator_phone(String translator_phone) {
		this.translator_phone = translator_phone;
	}

	public String getTranslator_photo() {
		return translator_photo;
	}

	public void setTranslator_photo(String translator_photo) {
		this.translator_photo = translator_photo;
	}

	public String getTranslator_city() {
		return translator_city;
	}

	public void setTranslator_city(String translator_city) {
		this.translator_city = translator_city;
	}

	
	public String getTranslator_gender() {
		return translator_gender;
	}

	public void setTranslator_gender(String translator_gender) {
		this.translator_gender = translator_gender;
	}
	
	public String getKnown_language() {
		return known_language;
	}

	public void setKnown_language(String known_language) {
		this.known_language = known_language;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	
}
