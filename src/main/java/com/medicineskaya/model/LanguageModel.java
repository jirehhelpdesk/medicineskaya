package com.medicineskaya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="language_details")
public class LanguageModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="language_id")
	private int language_id;
	
	@Column(name="language_name")
	private String language_name;
	
	@Column(name="cr_date")
	private Date cr_date;

	@Column(name="language_info_1")
	private String language_info_1;
	
	@Column(name="language_info_2")
	private String language_info_2;

	
	
	public int getLanguage_id() {
		return language_id;
	}

	public void setLanguage_id(int language_id) {
		this.language_id = language_id;
	}

	public String getLanguage_name() {
		return language_name;
	}

	public void setLanguage_name(String language_name) {
		this.language_name = language_name;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getLanguage_info_1() {
		return language_info_1;
	}

	public void setLanguage_info_1(String language_info_1) {
		this.language_info_1 = language_info_1;
	}

	public String getLanguage_info_2() {
		return language_info_2;
	}

	public void setLanguage_info_2(String language_info_2) {
		this.language_info_2 = language_info_2;
	}
	
}
