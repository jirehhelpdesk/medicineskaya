package com.medicineskaya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="hotel_lodge_details")
public class HotelLodgeModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="hotel_id")
	private int hotel_id;
	
	@Column(name="hotel_name")
	private String hotel_name;
	
	@Column(name="hotel_type")
	private String hotel_type;
	
	@Column(name="hotel_address")
	private String hotel_address;
	
	@Column(name="hotel_email_id")
	private String hotel_email_id;
	
	@Column(name="hotel_phone")
	private String hotel_phone;
	
	@Column(name="hotel_photo")
	private String hotel_photo;
	
	@Column(name="hotel_city")
	private String hotel_city;
	
	@Column(name="type_of_food")
	private String type_of_food;
	
	@Column(name="cr_date")
	private Date cr_date;

	
	public int getHotel_id() {
		return hotel_id;
	}

	public void setHotel_id(int hotel_id) {
		this.hotel_id = hotel_id;
	}

	public String getHotel_name() {
		return hotel_name;
	}

	public void setHotel_name(String hotel_name) {
		this.hotel_name = hotel_name;
	}

	
	
	public String getHotel_type() {
		return hotel_type;
	}

	public void setHotel_type(String hotel_type) {
		this.hotel_type = hotel_type;
	}

	public String getHotel_address() {
		return hotel_address;
	}

	public void setHotel_address(String hotel_address) {
		this.hotel_address = hotel_address;
	}

	public String getHotel_email_id() {
		return hotel_email_id;
	}

	public void setHotel_email_id(String hotel_email_id) {
		this.hotel_email_id = hotel_email_id;
	}

	public String getHotel_phone() {
		return hotel_phone;
	}

	public void setHotel_phone(String hotel_phone) {
		this.hotel_phone = hotel_phone;
	}

	public String getHotel_photo() {
		return hotel_photo;
	}

	public void setHotel_photo(String hotel_photo) {
		this.hotel_photo = hotel_photo;
	}

	public String getHotel_city() {
		return hotel_city;
	}

	public void setHotel_city(String hotel_city) {
		this.hotel_city = hotel_city;
	}

	public String getType_of_food() {
		return type_of_food;
	}

	public void setType_of_food(String type_of_food) {
		this.type_of_food = type_of_food;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	
}
