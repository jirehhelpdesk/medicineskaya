package com.medicineskaya.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="priority_doctor_details")
public class PriorityDoctorDetails {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="priority_doctor_id")
	private int priority_doctor_id;
	
	@Column(name="doctor_name")
	private String doctor_name;
	
	@Column(name="doctor_degree")
	private String doctor_degree;
	
	@Column(name="doctor_website")
	private String doctor_website;
	
	@Column(name="doctor_email")
	private String doctor_email;
	
	@Column(name="doctor_phone")
	private String doctor_phone;
	
	@Column(name="doctor_facebook")
	private String doctor_facebook;
	
	@Column(name="doctor_skype")
	private String doctor_skype;
	
	@Column(name="doctor_specialization")
	private String doctor_specialization;
	
	@Column(name="about_doctor")
	private String about_doctor;
	
	@Column(name="doctor_photo")
	private String doctor_photo;
	
	@Column(name="cr_date")
	private Date cr_date;

	@Column(name="doctor_info1")
	private String doctor_info1;

	
	
	public int getPriority_doctor_id() {
		return priority_doctor_id;
	}

	public void setPriority_doctor_id(int priority_doctor_id) {
		this.priority_doctor_id = priority_doctor_id;
	}

	public String getDoctor_name() {
		return doctor_name;
	}

	public void setDoctor_name(String doctor_name) {
		this.doctor_name = doctor_name;
	}

	public String getDoctor_degree() {
		return doctor_degree;
	}

	public void setDoctor_degree(String doctor_degree) {
		this.doctor_degree = doctor_degree;
	}
	
	public String getDoctor_website() {
		return doctor_website;
	}

	public void setDoctor_website(String doctor_website) {
		this.doctor_website = doctor_website;
	}

	public String getDoctor_email() {
		return doctor_email;
	}

	public void setDoctor_email(String doctor_email) {
		this.doctor_email = doctor_email;
	}

	public String getDoctor_phone() {
		return doctor_phone;
	}

	public void setDoctor_phone(String doctor_phone) {
		this.doctor_phone = doctor_phone;
	}

	public String getDoctor_facebook() {
		return doctor_facebook;
	}

	public void setDoctor_facebook(String doctor_facebook) {
		this.doctor_facebook = doctor_facebook;
	}

	public String getDoctor_skype() {
		return doctor_skype;
	}

	public void setDoctor_skype(String doctor_skype) {
		this.doctor_skype = doctor_skype;
	}

	public String getDoctor_specialization() {
		return doctor_specialization;
	}

	public void setDoctor_specialization(String doctor_specialization) {
		this.doctor_specialization = doctor_specialization;
	}

	
	public String getAbout_doctor() {
		return about_doctor;
	}

	public void setAbout_doctor(String about_doctor) {
		this.about_doctor = about_doctor;
	}

	public String getDoctor_photo() {
		return doctor_photo;
	}

	public void setDoctor_photo(String doctor_photo) {
		this.doctor_photo = doctor_photo;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getDoctor_info1() {
		return doctor_info1;
	}

	public void setDoctor_info1(String doctor_info1) {
		this.doctor_info1 = doctor_info1;
	}
	
	
	
}
