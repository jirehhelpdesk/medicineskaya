package com.medicineskaya.bean;

import java.util.Date;

public class ResultCategoryBean {

	
    private int id;
    private String type;
    private String sub_type;
	private String name;
	private String address;
	private String email_id;
	private String phone;
	private String photo;
	private String city;
	
	
	private String type_of_food;
	private String gender;
	private String language_known;
	private String urlLink;
	private String specialization;
	
	
	private Date cr_date;

	

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	
	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getSub_type() {
		return sub_type;
	}


	public void setSub_type(String sub_type) {
		this.sub_type = sub_type;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getEmail_id() {
		return email_id;
	}


	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getType_of_food() {
		return type_of_food;
	}


	public void setType_of_food(String type_of_food) {
		this.type_of_food = type_of_food;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getLanguage_known() {
		return language_known;
	}


	public void setLanguage_known(String language_known) {
		this.language_known = language_known;
	}


	public String getUrlLink() {
		return urlLink;
	}


	public void setUrlLink(String urlLink) {
		this.urlLink = urlLink;
	}


	public String getSpecialization() {
		return specialization;
	}


	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}


	public Date getCr_date() {
		return cr_date;
	}


	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}
	
	
	
}
