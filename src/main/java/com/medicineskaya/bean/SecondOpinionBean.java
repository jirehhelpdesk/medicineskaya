package com.medicineskaya.bean;

import java.util.Date;


public class SecondOpinionBean {

	private int opinion_id;
	private String treatment_on;
	private String doctor_name;
	private Date appointment_date;
	private String remark;
	private Date cr_date;
	private int user_id;
	
	private String user_name;
	private String user_emailId;
	private String user_phone;
	
	
	
	
	public int getOpinion_id() {
		return opinion_id;
	}
	public void setOpinion_id(int opinion_id) {
		this.opinion_id = opinion_id;
	}
	public String getTreatment_on() {
		return treatment_on;
	}
	public void setTreatment_on(String treatment_on) {
		this.treatment_on = treatment_on;
	}
	public String getDoctor_name() {
		return doctor_name;
	}
	public void setDoctor_name(String doctor_name) {
		this.doctor_name = doctor_name;
	}
	public Date getAppointment_date() {
		return appointment_date;
	}
	public void setAppointment_date(Date appointment_date) {
		this.appointment_date = appointment_date;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCr_date() {
		return cr_date;
	}
	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_emailId() {
		return user_emailId;
	}
	public void setUser_emailId(String user_emailId) {
		this.user_emailId = user_emailId;
	}
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	
	
	
	
}
