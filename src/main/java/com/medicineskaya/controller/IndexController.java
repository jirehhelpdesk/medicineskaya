package com.medicineskaya.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.medicineskaya.bean.UserBean;
import com.medicineskaya.model.ContactUsModel;
import com.medicineskaya.model.OpinionDetails;
import com.medicineskaya.model.PriorityDoctorDetails;
import com.medicineskaya.model.TouriestPlaceDetails;
import com.medicineskaya.model.UserRegistrationDetails;
import com.medicineskaya.service.AdminService;
import com.medicineskaya.service.UserService;


@Controller
@RequestMapping("/")
public class IndexController {

	
	@Autowired
	UserService userService;
	
	@Autowired
	private AdminService adminService;
	
	
	@RequestMapping(value = {"/"}, method = RequestMethod.GET)
	public ModelAndView index(HttpSession session) {

		if (session.getAttribute("userId") != null)
		{			
			int userId = (int) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("categoryList",userService.getCategoryDetails());
			
        	return new ModelAndView("index",model);
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("categoryList",userService.getCategoryDetails());
			
			return new ModelAndView("index",model);
		}				
	}
	
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView index(HttpSession session,HttpServletRequest request) {
				
		if (session.getAttribute("userId") != null)
		{			
			int userId = (int) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("categoryList",userService.getCategoryDetails());
			
        	return new ModelAndView("index",model);
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("categoryList",userService.getCategoryDetails());
			
			return new ModelAndView("index",model);
		}				
	}
	
	@RequestMapping(value = "/aboutUs", method = RequestMethod.GET)
	public ModelAndView aboutUs(HttpServletRequest request,HttpSession session) {
		
		if(session.getAttribute("userId") != null)
		{			
			int userId = (int) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("categoryList",userService.getCategoryDetails());
			
			return new ModelAndView("index_aboutUs",model);	
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("categoryList",userService.getCategoryDetails());
			
			return new ModelAndView("index_aboutUs",model);
		}	
					
	}
	
	@RequestMapping(value = "/services", method = RequestMethod.GET)
	public ModelAndView service(HttpServletRequest request,HttpSession session) {
		
		if(session.getAttribute("userId") != null)
		{			
			int userId = (int) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("categoryList",userService.getCategoryDetails());
			
			return new ModelAndView("index_services",model);	
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("categoryList",userService.getCategoryDetails());
			
			return new ModelAndView("index_services",model);
		}
			
	}
	
	@RequestMapping(value = "/specialization", method = RequestMethod.GET)
	public ModelAndView portfolio(HttpServletRequest request,HttpSession session) {
			
		if(session.getAttribute("userId") != null)
		{			
			int userId = (int) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("categoryList",userService.getCategoryDetails());
			
			return new ModelAndView("index_specialization",model);	
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("categoryList",userService.getCategoryDetails());
			
			return new ModelAndView("index_specialization",model);
		}		
	}
	
	@RequestMapping(value = "/secOpinion", method = RequestMethod.GET)
	public ModelAndView secOpinion(@ModelAttribute("opinionModel") OpinionDetails opinionModel,HttpServletRequest request,HttpSession session) {
			
		if(session.getAttribute("userId") != null)
		{			
			int userId = (int) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("categoryList",userService.getCategoryDetails());
			
			model.put("specialist_list",adminService.getDoctorSpecialist());
			
			return new ModelAndView("index_secOpinion",model);	
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("categoryList",userService.getCategoryDetails());
			
			model.put("specialist_list",adminService.getDoctorSpecialist());
			
			return new ModelAndView("index_secOpinion",model);
		}					
	}
	
	@RequestMapping(value = "/contactUs", method = RequestMethod.GET)
	public ModelAndView contactUs(@ModelAttribute("contactModel") ContactUsModel contactModel,HttpServletRequest request,HttpSession session) {
						
		if(session.getAttribute("userId") != null)
		{			
			int userId = (int) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("categoryList",userService.getCategoryDetails());
			
			return new ModelAndView("index_contactUs",model);	
        }
		else
		{
			return new ModelAndView("index_contactUs");
		}					
	}
	
	
	
	@RequestMapping(value = "/recomendDoctor", method = RequestMethod.GET)
	public ModelAndView recomendDoctor(HttpServletRequest request,HttpSession session) {
			
		if(session.getAttribute("userId") != null)
		{			
			int userId = (int) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			List<PriorityDoctorDetails> searchedDoctor = userService.getPriorityDoctorDetails();
					
			model.put("doctorDetails",searchedDoctor);
			
			request.setAttribute("noOfDoctor",searchedDoctor.size());
			
			return new ModelAndView("index_priority_doctor_list",model);
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();	
			
			List<PriorityDoctorDetails> searchedDoctor = userService.getPriorityDoctorDetails();
			
			model.put("doctorDetails",searchedDoctor);
			
			request.setAttribute("noOfDoctor",searchedDoctor.size());
			
			return new ModelAndView("index_priority_doctor_list",model);
		}						
	}
	
	
	@RequestMapping(value = "/touristPlaces", method = RequestMethod.GET)
	public ModelAndView touristPlaces(HttpServletRequest request,HttpSession session) {
		
		if(session.getAttribute("userId") != null)
		{			
			int userId = (int) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			List<TouriestPlaceDetails> searchedPlaced = userService.getTouriestPlaceDetails();
			
			model.put("touristDetails",searchedPlaced);
			
			request.setAttribute("noOfPlace",searchedPlaced.size());
			
			return new ModelAndView("index_tourist_places",model);
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();	
			
			List<TouriestPlaceDetails> searchedPlaced = userService.getTouriestPlaceDetails();
			
			model.put("touristDetails",searchedPlaced);
			
			request.setAttribute("noOfPlace",searchedPlaced.size());
			
			return new ModelAndView("index_tourist_places",model);
		}				
	}
	
	
	@RequestMapping(value = "/getSearchTouristPlace", method = RequestMethod.POST)
	@ResponseBody public ModelAndView getSearchTouristPlace(HttpSession session,HttpServletRequest request) {
	
		int placeId = Integer.parseInt(request.getParameter("placeId"));		
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		List<TouriestPlaceDetails> searchedPlaced = userService.getSearchTouriestPlaceDetails(placeId);
		
		model.put("touristDetails",searchedPlaced);
		
	    return new ModelAndView("index_search_tourist_places",model);
	}
	
	
	@RequestMapping(value = "/getSearchDoctor", method = RequestMethod.POST)
	@ResponseBody public ModelAndView getSearchDoctor(HttpSession session,HttpServletRequest request) {
	
		int doctorId = Integer.parseInt(request.getParameter("doctorId"));		
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		List<PriorityDoctorDetails> searchedDoctor = userService.getSearchDoctorDetails(doctorId);
		
		model.put("doctorDetails",searchedDoctor);
		
	    return new ModelAndView("index_search_priority_doctor",model);
	}
	
	
	
}
