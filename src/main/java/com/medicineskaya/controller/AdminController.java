package com.medicineskaya.controller;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.medicineskaya.bean.SecondOpinionBean;
import com.medicineskaya.bean.UserBean;
import com.medicineskaya.model.AdminDetails;
import com.medicineskaya.model.ContactUsModel;
import com.medicineskaya.model.PriorityDoctorDetails;
import com.medicineskaya.model.ShelterType;
import com.medicineskaya.model.TouriestPlaceDetails;
import com.medicineskaya.service.AdminService;
import com.medicineskaya.util.ReportinExcel;


@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class AdminController {

	
	@Autowired
	private AdminService adminService;
	
	
	@RequestMapping(value = "/adminLogout", method = RequestMethod.GET)
	public String adminLogout(HttpSession session,HttpServletRequest request) {
					
		session.removeAttribute("adminId");			 
		
		adminService.setAdminAccess("offline");
		
		session.setAttribute("actionMessage", "offline");
		
		return "admin_login";				
	}
	
	
	
	@RequestMapping(value ="/adminLogin", method = RequestMethod.GET)
	public String adminLogin(@ModelAttribute("adminDetails") AdminDetails adminDetails,HttpSession session) {

		if (session.getAttribute("adminId") == null)
		{
			adminService.setAdminAccess("offline");
			
			session.setAttribute("actionMessage", "offline");
			
			return "admin_login";
		}
		else
		{
			return "redirect:/adminHome";
		}
		
	}
	
	
	@RequestMapping(value = "/authAdminCredential", method = RequestMethod.GET)
	public String getValidateAdminWithGet(HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{		
			session.setAttribute("actionMessage", "offline");
			
			adminService.setAdminAccess("offline");
			
        	return "admin_login";
        }
		else
		{			
			return "redirect:/adminHome";		
		}	
		
	}
	
	
	@RequestMapping(value = "/authAdminCredential", method = RequestMethod.POST)
	public String getValidateAdmin(@ModelAttribute("adminDetails") AdminDetails adminDetails,HttpSession session,HttpServletRequest request) {
		
		String admin_user_id = adminDetails.getAdmin_user_id();
        String admin_password = adminDetails.getAdmin_password();
        
        String authenticateStatus = adminService.getValidateAdmin(admin_user_id,admin_password);
        
        if(authenticateStatus.equals("correct"))
        {
        	session.setAttribute("adminId", admin_user_id);
        	session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
        	
        	return "redirect:/adminHome";	
        }
        else if(authenticateStatus.equals("online"))
        {
        	session.setAttribute("actionMessage", "online");
        	
        	return "admin_login";	
        }
        else
        {
        	session.setAttribute("actionMessage", "incorrect");
        	
        	return "admin_login";
        }	
        
	}
	
	
	
	@RequestMapping(value = "/adminHome", method = RequestMethod.GET)
	public ModelAndView adminHome(HttpSession session) {
			
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			/*model.put("portalStatus",adminService.getUpdateOnPortalStatus());*/
			
			return new ModelAndView("admin_dashboard",model);				
		}				
	}
	
	
	
	////////////////////////////////////////   USER MANAGEMENT   /////////////////////////////////////////
	
	
	
	@RequestMapping(value ="/userReport", method = RequestMethod.GET)
	public ModelAndView userRecord(HttpSession session,HttpServletRequest request) {

		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_portal_report",model);				
		}			
	}
	
	
	@RequestMapping(value = "/retrivePortalRecord", method = RequestMethod.POST)
	@ResponseBody public ModelAndView retrivePortalRecord(HttpSession session,HttpServletRequest request) {
	
		String report_type = request.getParameter("report_type");
		String fromDate = request.getParameter("from_date");
		String toDate = request.getParameter("to_date");
		
		Map<String, Object> model = new HashMap<String, Object>();
		
		if(report_type.equals("Second opinion"))
		{
			model.put("recordDetails",adminService.getOpenionReport(fromDate,toDate));
			
			
			return new ModelAndView("admin_opinion_report",model);
		}
		else if(report_type.equals("User List"))
		{
			model.put("recordDetails",adminService.getUserReport(fromDate,toDate));
			
			
			return new ModelAndView("admin_user_report",model);
		}
		else
		{
			 model.put("recordDetails",adminService.getContactReport(fromDate,toDate));
			
			 
			 return new ModelAndView("admin_contactus_report",model);
		}	   
	}
	
	
	@RequestMapping(value = "/viewMessage", method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewMessage(HttpSession session,HttpServletRequest request) {
		
		
		Map<String, Object> model = new HashMap<String, Object>();
		
		String tableType = request.getParameter("type");
		
		if(tableType.equals("Contact"))
		{
			int  patternId = Integer.parseInt(request.getParameter("uniqueId"));
			
		    model.put("contactDetails",adminService.getContactDetails(patternId));
		}
		else
		{
			int  patternId = Integer.parseInt(request.getParameter("uniqueId"));
			
		    model.put("opinionDetails",adminService.getOpinionDetails(patternId));
		}
		
		return new ModelAndView("admin_view_message",model);
	}
	
	
	
	@RequestMapping(value = "/downloadReport", method = RequestMethod.GET)
	public String downloadReport(HttpSession session,HttpServletRequest request) {
		
		String report_type = request.getParameter("report_type");
		String fromDate = request.getParameter("from_date");
		String toDate = request.getParameter("to_date");
		
		ReportinExcel report = new ReportinExcel();
		
		String fileName = "";
		String reportDirectory = "";
		
		if(report_type.equals("Second opinion"))
		{
			List<SecondOpinionBean> opinionBean = adminService.getOpenionReport(fromDate,toDate);
			
			fileName = report.generateOpinionReportInExcel(opinionBean);
			
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/MKFileDirectories");
		 	reportDirectory = fileResource.getString("opinionReport");		
			
		 	File fld = new File(reportDirectory);
	    	if(!fld.exists())
	    	{
			    fld.mkdirs();
	    	}
			
		}
		else if(report_type.equals("User List"))
		{
			List<UserBean> userbean = adminService.getUserReport(fromDate,toDate);
			
			fileName = report.generateUserReportInExcel(userbean);
			
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/MKFileDirectories");
		 	reportDirectory = fileResource.getString("userReport");		
			
		 	File fld = new File(reportDirectory);
	    	if(!fld.exists())
	    	{
			    fld.mkdirs();
	    	}
		}
		else
		{
			List<ContactUsModel> contactUs = adminService.getContactReport(fromDate,toDate);
			
			fileName = report.generateCotactReportInExcel(contactUs);
			
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/MKFileDirectories");
		 	reportDirectory = fileResource.getString("contactReport");		
			
		 	File fld = new File(reportDirectory);
	    	if(!fld.exists())
	    	{
			    fld.mkdirs();
	    	}
		}
		
		
		request.setAttribute("rootDirectory",reportDirectory);
		request.setAttribute("fileName",fileName);
		
		return "commonDownload";
	}
	
	////////////////////////////////////////    END OF USER MANAGE   ////////////////////////////////////////
	
	
	
	
	
	
	////////////////////////////////////////    ADMIN SERVICE ////////////////////////////////////////
	
	
	@RequestMapping(value ="/adminService", method = RequestMethod.GET)
	public ModelAndView adminService(HttpSession session,HttpServletRequest request) {

		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("categoryList",adminService.getCategoryList());
			
			model.put("cityList",adminService.getCityList());
			
			model.put("languageList",adminService.getLanguageList());
			
			model.put("foodTypeList",adminService.getFoodTypeList());
			
			model.put("specialist_list",adminService.getDoctorSpecialist());
			
			model.put("shelterTypeList",adminService.getShelterTypeList());
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_service",model);				
		}			
	}
	
	
	@RequestMapping(value = "/saveCategoryDetails", method = RequestMethod.POST)
	@ResponseBody public String saveCategoryDetails(HttpSession session,MultipartHttpServletRequest request) {
	
		
		String status = adminService.saveCategoryDetails(request);
		
	    return status;
	}
	
	
	
	//////////////////////   END OF ADMIN SERVICE  /////////////////////////////////////////////
	
	
	//////////////////////////      START OF PORTAL DATA ///////////////////////////////////////
	
	
	
	
	@RequestMapping(value ="/touristPlace", method = RequestMethod.GET)
	public ModelAndView touristPlace(@ModelAttribute("placeModel") TouriestPlaceDetails placeModel,HttpSession session,HttpServletRequest request) {

		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			/*model.put("categoryList",adminService.getCategoryList());*/
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_tourist_place",model);				
		}			
	}
	
	
	@RequestMapping(value ="/priorityDoctor", method = RequestMethod.GET)
	public ModelAndView priorityDoctor(@ModelAttribute("doctorModel") PriorityDoctorDetails doctorModel,HttpSession session,HttpServletRequest request) {

		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("specialist_list",adminService.getDoctorSpecialist());
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_priority_doctor",model);				
		}			
	}
	
	
	
	@RequestMapping(value = "/saveTouristPlaceDetails", method = RequestMethod.POST)
	@ResponseBody public String saveTouristPlaceDetails(@ModelAttribute("placeModel") TouriestPlaceDetails placeModel,HttpSession session,MultipartHttpServletRequest request) {
	
		String status = "";
		
		placeModel.setAbout_place(request.getParameter("textAreaContent"));
		
		status = adminService.saveTouristPlaceDetails(request,placeModel);
		
	    return status;
	}
	
	@RequestMapping(value = "/saveDoctorDetails", method = RequestMethod.POST)
	@ResponseBody public String saveDoctorDetails(@ModelAttribute("doctorModel") PriorityDoctorDetails doctorModel,HttpSession session,MultipartHttpServletRequest request) {
	
		String status = "";
		
		doctorModel.setAbout_doctor(request.getParameter("textAreaContent"));
		
		status = adminService.saveDoctorDetails(request,doctorModel);
		
	    return status;
	}
	
	
	
	///////////////////////////////      END OF PORTAL DATA   ////////////////////////////  
	
	
	
	
	
	
	@RequestMapping(value ="/adminSetting", method = RequestMethod.GET)
	public ModelAndView adminSetting(HttpSession session,HttpServletRequest request) {

		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_setting",model);				
		}			
	}
	
	
	
	@RequestMapping(value = "/saveAdminDetails", method = RequestMethod.POST)
	@ResponseBody public String saveAdminDetails(HttpSession session,HttpServletRequest request) {
	
		String status = "";
		
		status = adminService.saveAdminDetails(request.getParameter("admin_name"),request.getParameter("admin_designation"));
		
	    return status;
	}
	
	
	@RequestMapping(value = "/saveAdminPassword", method = RequestMethod.POST)
	@ResponseBody public String saveAdminPassword(HttpSession session,HttpServletRequest request) {
	
		String status = "";
		
		status = adminService.saveAdminPassword(request.getParameter("password"),request.getParameter("new_password"));
		
	    return status;
	}
	
}
