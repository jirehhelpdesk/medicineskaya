package com.medicineskaya.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.medicineskaya.bean.UserBean;
import com.medicineskaya.model.AdminDetails;
import com.medicineskaya.model.CityModel;
import com.medicineskaya.model.ContactUsModel;
import com.medicineskaya.model.OpinionDetails;
import com.medicineskaya.model.StateModel;
import com.medicineskaya.model.TouriestPlaceDetails;
import com.medicineskaya.model.UserRegistrationDetails;
import com.medicineskaya.service.UserService;

@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class UserController {

	@Autowired
	UserService userService;
	
	
	/**
	 * This method will list all existing users.
	 */
	
	@RequestMapping(value = "/serviceNotfound", method = RequestMethod.GET)
	public String serviceNotfound(HttpSession session) {
						
		return "error_page";				
	}
	
	@RequestMapping(value = "/userLogout", method = RequestMethod.GET)
	public String adminLogout(HttpSession session,HttpServletRequest request) {
					
		session.removeAttribute("userId");			 
		request.setAttribute("actionMessage", "success");
        
		return "redirect:/index";				
	}
	
	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public ModelAndView registration(@ModelAttribute("userDetails") UserRegistrationDetails userDetails,HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("userId") != null)
		{			
			int userId = (int) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
        	return new ModelAndView("index",model);
        }
		else
		{
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("countryList",userService.getCountryList());
			
			return new ModelAndView("index_registration",model);
		}					
					
	}
	
	@RequestMapping(value = "/getStateNames", method = RequestMethod.POST)
	public @ResponseBody String getStateNames(HttpSession session,HttpServletRequest request) {
	
		String stateNames = "";
		
		String countryName = request.getParameter("countryName");
		
		stateNames = userService.getStateNames(countryName);
		
		return stateNames;
	}
	
	
	@RequestMapping(value = "/signIn", method = RequestMethod.GET)
	public ModelAndView signIn(@ModelAttribute("userDetails") UserRegistrationDetails userDetails,HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("userId") != null)
		{			
			int userId = (int) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			request.setAttribute("actionMessage", "NA");
			
        	return new ModelAndView("index",model);
        }
		else
		{
			request.setAttribute("actionMessage", "NA");
			
			return new ModelAndView("index_signin");
		}						
						
	}
	
	
	@RequestMapping(value = "/forgetPassword", method = RequestMethod.GET)
	public String forgetPassword(HttpServletRequest request,HttpSession session) {
	
			
		return "index_forget_password";
	}
	
	
	@RequestMapping(value = "/genForgetPasswordPattern", method = RequestMethod.POST)
	public @ResponseBody String genForgetPasswordPattern(HttpSession session,HttpServletRequest request) {
		
		String status = "";
		
		String emailId = request.getParameter("emailId");
		
		UserRegistrationDetails userDetails = new UserRegistrationDetails();
		
		userDetails.setUser_emailid(emailId);
		
		status = userService.checkUserAmbiguity(userDetails,"Email Id");
		
		if(status.equals("Exist"))
		{
			status = userService.sendLinkForgetPassword(emailId);
			
			return status;
		}
		else
		{			
			return "NotExist";
		}
		
	}
	
	
	
	@RequestMapping(value = "/resetPassword", method = RequestMethod.GET)
	public String resetPassword(HttpSession session,HttpServletRequest request) {
	
		String uniqueId = request.getParameter("requnIkedij");
		
		String requestedEmailID = userService.getEmailIdFromForgetPassword(uniqueId);
		
		if(requestedEmailID.equals("Not Exist"))
		{
			request.setAttribute("requestMesage", "Given Link is expired please try again.");
			
			return "index_reset_password";
		}
		else
		{
			request.setAttribute("requestedEmailID", requestedEmailID);
			request.setAttribute("uniqueId", uniqueId);	
			
			return "index_reset_password";
		}
		
	}
	
	
	@RequestMapping(value = "/saveUserResetPassword", method = RequestMethod.POST)
	public @ResponseBody String saveUserResetPassword(HttpSession session,HttpServletRequest request) {
	
		String status = "";
		
		String emailId = request.getParameter("requestedEmailID");
		String password = request.getParameter("new_password");
		
		status = userService.getChangePassword(emailId,password);
		
		return status;
	}
	
	
	
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.GET)
	public ModelAndView changePassword(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("userId") != null)
		{			
			int userId = (int) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
        	return new ModelAndView("index_change_password",model);
        }
		else
		{
			return new ModelAndView("index");
		}						
						
	}
	
	
	@RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
	@ResponseBody public String updatePassword(HttpSession session,HttpServletRequest request) {
	
		if (session.getAttribute("userId") != null)
		{		
			int userId = (int) session.getAttribute("userId");
			
			String currentPw =  request.getParameter("exist_password");	
			
			String newPw =  request.getParameter("new_password");	
			
			String status = userService.updatePassword(userId,currentPw,newPw);
			
		    return status;        	
        }
		else
		{	
			return "failed";
		}	
	}
	
	
	@RequestMapping(value = "/checkUserEmailId", method = RequestMethod.POST)
	@ResponseBody public String checkUserEmailId(@ModelAttribute("userDetails") UserRegistrationDetails userDetails,HttpSession session,HttpServletRequest request) {
	
		String status = "";
		
		status = userService.checkUserAmbiguity(userDetails,"Email Id");
		
	    return status;
	}
	
	
	@RequestMapping(value = "/checkUserPhno", method = RequestMethod.POST)
	@ResponseBody public String checkUserPhno(@ModelAttribute("userDetails") UserRegistrationDetails userDetails,HttpSession session,HttpServletRequest request) {
	
		String status = "";
		
		status = userService.checkUserAmbiguity(userDetails,"Mobile");
		
	    return status;
	}
	
	
	@RequestMapping(value = "/saveUserRegisterDetails", method = RequestMethod.POST)
	@ResponseBody public String saveUserRegisterDetails(@ModelAttribute("userDetails") UserRegistrationDetails userDetails,HttpSession session,HttpServletRequest request) {
	
		String status = "";
		
		status = userService.saveUserRegisterDetails(userDetails);
		
	    return status;
	}
	
	
	@RequestMapping(value = "/saveUserRegisterPopUp", method = RequestMethod.POST)
	@ResponseBody public String saveUserRegisterPopUp(HttpSession session,HttpServletRequest request) {
	
		String status = "";
		
		UserRegistrationDetails userDetails = new UserRegistrationDetails();
		
		userDetails.setUser_full_name(request.getParameter("user_full_name"));
		userDetails.setUser_emailid(request.getParameter("user_emailid"));
		userDetails.setUser_mobile(request.getParameter("user_mobile"));
		userDetails.setUser_password(request.getParameter("user_password"));
		
		status = userService.saveUserRegisterDetails(userDetails);
		
	    return status;
	}
	
	
	
	@RequestMapping(value = "/authUserCredential", method = RequestMethod.GET)
	public String authUserCredential(HttpSession session) {
		
		if (session.getAttribute("userId") == null)
		{						
        	return "redirect:/index";
        }
		else
		{	
			return "redirect:/index";
		}		
	}
	
	
	@RequestMapping(value = "/authUserCredential", method = RequestMethod.POST)
	public ModelAndView authUserCredential(@ModelAttribute("userDetails") UserRegistrationDetails userDetails,HttpSession session,HttpServletRequest request) {
		
		String userName = userDetails.getUser_emailid();
        String password = userDetails.getUser_password();
       
        String authenticateStatus = userService.getValidateUser(userName,password);
        
        if(authenticateStatus.equals("Active"))
        {	
        	int userId = userService.getUserIdVieEmailId(userName);
        	session.setAttribute("userId", userId);
        	session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
        
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
        	
			model.put("categoryList",userService.getCategoryDetails());
			
        	return new ModelAndView("index",model);
        }
        else if(authenticateStatus.equals("Deactive"))
        {
        	request.setAttribute("userName", userName);
        	request.setAttribute("actionMessage", "Deactive");
        	
        	return new ModelAndView("index_signin");
        }	
        else   
        {
        	request.setAttribute("userName", userName);
        	request.setAttribute("actionMessage", "Invalid");
        	
        	return new ModelAndView("index_signin");
        }        
	}
	
	
	
	@RequestMapping(value = "/checkPopLogin", method = RequestMethod.POST)
	@ResponseBody public String checkPopLogin(HttpSession session,HttpServletRequest request) {
	
		
		String userName = request.getParameter("email_id");
        String password = request.getParameter("password");
       
        String authenticateStatus = userService.getValidateUser(userName,password);
        
        if(authenticateStatus.equals("Active"))
        {	
        	int userId = userService.getUserIdVieEmailId(userName);
        	session.setAttribute("userId", userId);
        	session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
        
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
        	
			model.put("categoryList",userService.getCategoryDetails());
			
			String resultPattern = userId+"/"+authenticateStatus;
			
        	return resultPattern;
        }
        else if(authenticateStatus.equals("Deactive"))
        {
        	
        	String resultPattern = 0+"/"+authenticateStatus;
			
        	return resultPattern;
        }	
        else   
        {
        	String resultPattern = 0+"/Invalid";
			
        	return resultPattern;
        }        
		
	}
	
	
	@RequestMapping(value = "/showCityNames", method = RequestMethod.POST)
	@ResponseBody public String showCityNames(HttpSession session,HttpServletRequest request) {
	
		String cityNames = "";
		
		List<CityModel> cityList = userService.getCityDetails();
		
		for(int i=0;i<cityList.size();i++)
		{
			cityNames +=cityList.get(i).getCity_name() + ",";
		}
		
		if(cityNames.length()>2)
		{
			cityNames = cityNames.substring(0,cityNames.length()-1);
		}
		
	    return cityNames;
	}
	
	
	
	
	@RequestMapping(value = "/showValueList", method = RequestMethod.POST)
	@ResponseBody public String showValueList(HttpSession session,HttpServletRequest request) {
	
		String category = request.getParameter("categoryName");		
		String cityName = request.getParameter("cityName");
		
		String result = userService.getCategoryValues(category,cityName);
		
	    return result;
	}
	
	
	
	@RequestMapping(value = "/searchRequiredDetails", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchRequiredDetails(HttpSession session,HttpServletRequest request) {
	
		String category = request.getParameter("categoryName");		
		String cityName = request.getParameter("cityName");
		String valueName = request.getParameter("valueName");
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		model.put("resultCategoryBean",userService.getSearchRequiredDetails(category,cityName,valueName));
		
	    return new ModelAndView("index_category_result",model);
	}
	
	
	@RequestMapping(value = "/viewMoreAboutCategory", method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewMoreAboutCategory(HttpSession session,HttpServletRequest request) {
	
		int id = Integer.parseInt(request.getParameter("id"));	
		
		String categoryType = request.getParameter("type");
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		model.put("viewMoreResult",userService.getViewMoreAboutCategory(id,categoryType));
		
	    return new ModelAndView("index_category_viewmore",model);
	}
	
	
	
	
	@RequestMapping(value = "/saveContactDetails", method = RequestMethod.POST)
	@ResponseBody public String saveContactDetails(@ModelAttribute("contactModel") ContactUsModel contactModel,HttpSession session,HttpServletRequest request) {
	
		String status = "";
		
		status = userService.saveContactDetails(contactModel);
		
	    return status;
	}
	
	
	
	@RequestMapping(value = "/getDoctorList", method = RequestMethod.POST)
	@ResponseBody public String getDoctorList(HttpSession session,HttpServletRequest request) {
	
		String result = "";
		
		String specialization = request.getParameter("specialization");
		
		result = userService.getDoctorList(specialization);
		
	    return result;
	}
	
	@RequestMapping(value = "/saveSecondOpinion", method = RequestMethod.POST)
	@ResponseBody public String saveSecondOpinion(@ModelAttribute("opinionModel") OpinionDetails opinionModel,HttpSession session,HttpServletRequest request) {
	
		if (session.getAttribute("userId") != null)
		{		
			int userId = (int) session.getAttribute("userId");
			
			opinionModel.setUser_id(userId);
			String appointmentDate = request.getParameter("appointment_date");
			
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

	        try {

	            Date date = formatter.parse(appointmentDate);
	            
	            opinionModel.setAppointment_date(date);
	            
	        } 
	        catch (ParseException e) {
	            e.printStackTrace();
	        }
			
			
			String status = "";
			
			status = userService.saveSecondOpinion(opinionModel);
			
		    return status;
        }
		else
		{	
			
			return "failed";
		}	
		
	}
	
	
	
	@RequestMapping(value = "/refreshHeaderDiv", method = RequestMethod.POST)
	@ResponseBody public ModelAndView refreshHeaderDiv(HttpSession session,HttpServletRequest request) {
	
		int userId = Integer.parseInt(request.getParameter("userId"));	
		
		UserBean userBean = userService.getUserDetails(userId);
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		model.put("userBean",userBean);
		
	    return new ModelAndView("index_header_menu",model);
	}
	
	
	
	
	
	@RequestMapping(value = "/refreshSideDiv", method = RequestMethod.POST)
	@ResponseBody public ModelAndView refreshSideDiv(HttpSession session,HttpServletRequest request) {
	
		int userId = Integer.parseInt(request.getParameter("userId"));	
		
		UserBean userBean = userService.getUserDetails(userId);
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		model.put("userBean",userBean);
		
	    return new ModelAndView("index_side_search_button",model);
	}
	
	
	@RequestMapping(value = "/refreshLowerDiv", method = RequestMethod.POST)
	@ResponseBody public ModelAndView refreshLowerDiv(HttpSession session,HttpServletRequest request) {
	
		int userId = Integer.parseInt(request.getParameter("userId"));	
		
		UserBean userBean = userService.getUserDetails(userId);
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		model.put("userBean",userBean);
		
	    return new ModelAndView("index_opinion_button",model);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/showImages", method = RequestMethod.GET)
	@ResponseBody public byte[] showImages(HttpServletRequest request,HttpSession session) throws IOException {
		
		
		String fileName = request.getParameter("fileName");
		String fileDirectoryCategory = request.getParameter("directoryType");
		
		String fileDir = "";
		// Required file Config for entire Controller 
		 
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/MKFileDirectories");
		
		if(fileDirectoryCategory.equals("Portal"))
		{
			fileDir = fileResource.getString("portalRoot");
			fileDir = fileDir + "/" + request.getParameter("subDirectory");
		}
		else
		{
			fileDir = fileResource.getString("categoryRoot");
			fileDir = fileDir + "/" + request.getParameter("subDirectory");
		}
		
		// End of Required file Config for entire Controller 
		 
		ServletContext servletContext = request.getSession().getServletContext();
		
		File ourPath = new File(fileDir);
		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
		String docCategory = fPath.toString();
		
		if(fileName.equals(""))
		{
			fPath.delete(0, fPath.length());				
			File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/image-not-found.png"));					
			fPath.append(targetFile.getPath());											
		}
		else
		{
			if(StringUtils.isNotBlank(docCategory))
			{				
				/* Checks that files are exist or not  */
				
					File exeFile=new File(fPath.toString());
					if(!exeFile.exists())
					{
						fPath.delete(0, fPath.length());				
						File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/image-not-found.png"));					
						fPath.append(targetFile.getPath());			
					}	
					
				/* Checks that files are exist or not  */
					
			}
		}
	
		InputStream in;
		
		try {					
				FileInputStream docdir = new FileInputStream(fPath.toString());													
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{							
					return null;
				}			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	

	
	
	
}
