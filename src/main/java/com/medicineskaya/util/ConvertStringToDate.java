package com.medicineskaya.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConvertStringToDate {

	public Date convertStringIntoDate(String date)
	{
		DateFormat formatter = null;
        
	    Date convertDate = null;
	   
	    try {
	    	
	        formatter = new SimpleDateFormat("dd/MM/yyyy");
	        convertDate = (Date) formatter.parse(date);
	        
	        
	    } catch (ParseException e) {
	        e.printStackTrace();
	    }	
	    
	    return  convertDate;
	}
	
	
}
