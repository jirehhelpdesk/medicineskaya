package com.medicineskaya.util;

import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailIdExistance {
	
	public String getEmailStatus()
	{		 
		    String resetStatus = "";
		  
		    ResourceBundle smsresource = ResourceBundle.getBundle("resources/emailConfig");
			
		    String auth=smsresource.getString("mail.smtp.auth");
			String starttls = smsresource.getString("mail.smtp.starttls.enable");
			String host = smsresource.getString("mail.smtp.host");			
			String port = smsresource.getString("mail.smtp.port");
			String emailusername = smsresource.getString("emailusername");
			String emailpassword = smsresource.getString("emailpassword");

			
		      
		      String from = emailusername;
		      final String username = from;//change accordingly
		      final String password = emailpassword;//change accordingly
		      Properties props = new Properties();
		      
		      
			props.put("mail.smtp.auth", auth);
			props.put("mail.smtp.starttls.enable", starttls);
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", port);
			
			Session session = Session.getInstance(props,
					  new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(username, password);
						}
					  });
	 
			
				try {
				
				//create the POP3 store object and connect with the pop server
				  Store store = session.getStore("pop3s");

				  store.connect(host, username, password);

				  //create the folder object and open it
				  Folder emailFolder = store.getFolder("INBOX");
				  emailFolder.open(Folder.READ_ONLY);

				  // retrieve the messages from the folder in an array and print it
				  Message[] messages = emailFolder.getMessages();
				  System.out.println("messages.length---" + messages.length);

				  for (int i = 0, n = messages.length; i < n; i++) {
				     
					 Message message1 = messages[i];
				     System.out.println("---------------------------------");
				     System.out.println("Email Number " + (i + 1));
				     System.out.println("Subject: " + message1.getSubject());
				     System.out.println("From: " + message1.getFrom()[0]);
				     System.out.println("Text: " + message1.getContent().toString());

				  }

				  //close the store and folder objects
				  emailFolder.close(false);
				  store.close();
				
				  } catch (NoSuchProviderException e) {
				     e.printStackTrace();
				  } catch (MessagingException e) {
				     e.printStackTrace();
				  } catch (Exception e) {
				     e.printStackTrace();
				  }
				
			
	      return resetStatus;
	}
	
	
	public static void main(String arg[])
	{
		EmailIdExistance obj = new EmailIdExistance();
	    String content = obj.getEmailStatus();
	
		System.out.println("Result Status=="+content);
	}
	  
	
}
