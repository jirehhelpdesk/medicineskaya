package com.medicineskaya.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;







import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.medicineskaya.bean.SecondOpinionBean;
import com.medicineskaya.bean.UserBean;
import com.medicineskaya.model.ContactUsModel;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


public class ReportinExcel {
	
	
	public String generateUserReportInExcel(List<UserBean> eModel)
	{
		String reportStatus = "";
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/MKFileDirectories");
	 	String reportDirectory = fileResource.getString("userReport");		
		
	 	String fileName = "UserReport.xls";
	 	
	 	
    	try {
    		 
             File exlDirectory = new File(reportDirectory);
             if(!exlDirectory.exists())
         	 {
            	 exlDirectory.mkdirs();
         	 }
             
             File excelReport = new File(reportDirectory + "/" + fileName);
             
             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
             
             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
        	 
             //Add the created Cells to the heading of the sheet
             
             writableSheet.addCell(new Label(0, 0, "SERIAL NO"));
             writableSheet.addCell(new Label(1, 0, "FULL NAME"));
             writableSheet.addCell(new Label(2, 0, "EMAIL ID"));
             writableSheet.addCell(new Label(3, 0, "PHONE NO"));
             writableSheet.addCell(new Label(4, 0, "COUNTRY"));
             writableSheet.addCell(new Label(5, 0, "STATE"));
             writableSheet.addCell(new Label(6, 0, "STATUS"));
             writableSheet.addCell(new Label(7, 0, "REGISTRATION DATE"));
             
             int j = 1;
             
             for(int i=0;i<eModel.size();i++)
             {  	
            	 writableSheet.addCell(new Number(0, j, j));
            	 writableSheet.addCell(new Label(1, j, eModel.get(i).getUser_full_name()));
            	 writableSheet.addCell(new Label(2, j, eModel.get(i).getUser_emailid()));
            	 writableSheet.addCell(new Label(3, j, eModel.get(i).getUser_mobile()));
            	 writableSheet.addCell(new Label(4, j, eModel.get(i).getUser_country()));
            	 writableSheet.addCell(new Label(5, j, eModel.get(i).getUser_state()));	            	 
            	 writableSheet.addCell(new Label(6, j, eModel.get(i).getUser_status()));           	 
            	 
            	 DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            	 Date regDate = eModel.get(i).getUser_reg_time();        
            	 String reportDate = df.format(regDate);

            	 writableSheet.addCell(new Label(7, j,reportDate));	 
            	
            	 j = j + 1;
             }
                	             
             writableWorkbook.write();
             writableWorkbook.close();
  
	         } catch (IOException e) {
	             e.printStackTrace();
	         } catch (RowsExceededException e) {
	             e.printStackTrace();
	         } catch (WriteException e) {
	             e.printStackTrace();
	         }
    	   	    
    	 
		
		return fileName;
	}
	
	public String generateOpinionReportInExcel(List<SecondOpinionBean> eModel)
	{		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/MKFileDirectories");
	 	String reportDirectory = fileResource.getString("opinionReport");		
		
	 	String fileName = "OpinionReport.xls";
	 	
    	try 
    	{   		 
             File exlDirectory = new File(reportDirectory);
             if(!exlDirectory.exists())
         	 {
            	 exlDirectory.mkdirs();
         	 }
             
             File excelReport = new File(reportDirectory + "/" + fileName);
             
             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
             
             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
        	 
             //Add the created Cells to the heading of the sheet
             
             writableSheet.addCell(new Label(0, 0, "SERIAL NO"));
             writableSheet.addCell(new Label(1, 0, "TREATMENT ON"));
             writableSheet.addCell(new Label(2, 0, "DOCTOR NAME"));
             writableSheet.addCell(new Label(3, 0, "APPOINTMENT DATE"));
             writableSheet.addCell(new Label(4, 0, "FULL NAME"));
             writableSheet.addCell(new Label(5, 0, "EMAIL ID"));
             writableSheet.addCell(new Label(6, 0, "PHONE"));
             writableSheet.addCell(new Label(7, 0, "CONTACT DATE"));
             writableSheet.addCell(new Label(8, 0, "REMARK"));
             
             int j = 1;
             
             for(int i=0;i<eModel.size();i++)
             {
            	 DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            	
            	 writableSheet.addCell(new Number(0, j, j));
            	 writableSheet.addCell(new Label(1, j, eModel.get(i).getTreatment_on()));
            	 writableSheet.addCell(new Label(2, j, eModel.get(i).getDoctor_name()));
            	 writableSheet.addCell(new Label(3, j, df.format(eModel.get(i).getAppointment_date())));
            	 writableSheet.addCell(new Label(4, j, eModel.get(i).getUser_name()));
            	 writableSheet.addCell(new Label(5, j, eModel.get(i).getUser_emailId()));	            	 
            	 writableSheet.addCell(new Label(6, j, eModel.get(i).getUser_phone()));           	 
            	 
            	 writableSheet.addCell(new Label(7, j,df.format(eModel.get(i).getCr_date())));	 
            	 writableSheet.addCell(new Label(8, j, eModel.get(i).getRemark()));           
            	 j = j + 1;
             }
                	             
             writableWorkbook.write();
             writableWorkbook.close();
  
	         } catch (IOException e) {
	             e.printStackTrace();
	         } catch (RowsExceededException e) {
	             e.printStackTrace();
	         } catch (WriteException e) {
	             e.printStackTrace();
	         }
    	   	    
    	 
		
		return fileName;
	}
	
	public String generateCotactReportInExcel(List<ContactUsModel> eModel)
	{		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/MKFileDirectories");
	 	String reportDirectory = fileResource.getString("contactReport");		
		
	 	String fileName = "ContactReport.xls";
	 	
	 	
    	try {
    		 
             File exlDirectory = new File(reportDirectory);
             if(!exlDirectory.exists())
         	 {
            	 exlDirectory.mkdirs();
         	 }
             
             File excelReport = new File(reportDirectory + "/" + fileName);
             
             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
             
             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
        	 
             //Add the created Cells to the heading of the sheet
             
             writableSheet.addCell(new Label(0, 0, "SERIAL NO"));
             writableSheet.addCell(new Label(1, 0, "FULL NAME"));
             writableSheet.addCell(new Label(2, 0, "EMAIL ID"));
             writableSheet.addCell(new Label(3, 0, "PHONE NO"));
             writableSheet.addCell(new Label(4, 0, "CONTACT FOR"));
             writableSheet.addCell(new Label(5, 0, "SUBJECT"));
             writableSheet.addCell(new Label(6, 0, "MESSAGE"));
             writableSheet.addCell(new Label(7, 0, "CONTACT DATE"));
             
             int j = 1;
             
             for(int i=0;i<eModel.size();i++)
             {  	
            	 writableSheet.addCell(new Number(0, j, j));
            	 writableSheet.addCell(new Label(1, j, eModel.get(i).getContact_name()));
            	 writableSheet.addCell(new Label(2, j, eModel.get(i).getContact_email_id()));
            	 writableSheet.addCell(new Label(3, j, eModel.get(i).getContact_phone()));
            	 writableSheet.addCell(new Label(4, j, eModel.get(i).getContact_reason()));
            	 writableSheet.addCell(new Label(5, j, eModel.get(i).getContact_subject()));	            	 
            	 writableSheet.addCell(new Label(6, j, eModel.get(i).getContact_message()));           	 
            	 
            	 DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            	 Date regDate = eModel.get(i).getContact_date();        
            	 String reportDate = df.format(regDate);

            	 writableSheet.addCell(new Label(7, j,reportDate));	 
            	
            	 j = j + 1;
             }
                	             
             writableWorkbook.write();
             writableWorkbook.close();
  
	         } catch (IOException e) {
	             e.printStackTrace();
	         } catch (RowsExceededException e) {
	             e.printStackTrace();
	         } catch (WriteException e) {
	             e.printStackTrace();
	         }
    	   	    
    	 
		
		return fileName;
	}
	
	
	/*public static void main(String[] args)
	{		
		generateConsolidatedReport obj = new generateConsolidatedReport();
		
	    System.out.println(obj.createConsolidatedReport());
	}*/

}
