package com.medicineskaya.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.ModelAndView;

import com.medicineskaya.model.AdminDetails;
import com.medicineskaya.model.AttenderNurseModel;
import com.medicineskaya.model.CategoryDetails;
import com.medicineskaya.model.CityModel;
import com.medicineskaya.model.ContactUsModel;
import com.medicineskaya.model.DoctorsModel;
import com.medicineskaya.model.EmailModel;
import com.medicineskaya.model.EmbassyModel;
import com.medicineskaya.model.FoodTypeModel;
import com.medicineskaya.model.HospitalModel;
import com.medicineskaya.model.HotelLodgeModel;
import com.medicineskaya.model.LanguageModel;
import com.medicineskaya.model.MedicalSpecialistModel;
import com.medicineskaya.model.OpinionDetails;
import com.medicineskaya.model.PriorityDoctorDetails;
import com.medicineskaya.model.RestaurentModel;
import com.medicineskaya.model.ShelterType;
import com.medicineskaya.model.TaxiServiceModel;
import com.medicineskaya.model.TouriestPlaceDetails;
import com.medicineskaya.model.TranslatorModel;
import com.medicineskaya.model.UserRegistrationDetails;

@SuppressWarnings("unused")
@Repository("adminDao")
public class AdminDaoImpl implements AdminDao  {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	@SuppressWarnings("unchecked")
	public List<AdminDetails> getAdminDetails()
	{	
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from AdminDetails where admin_id=1";
		List<AdminDetails> lsdata = session.createQuery(hql).list();	
		

		tx.commit();
		session.close();
		 
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getValidateAdmin(String user_id,String password)
	{	
		Session session = sessionFactory.openSession();
		
		String hql = "from AdminDetails where admin_id=1";
		List<AdminDetails> lsdata = session.createQuery(hql).list();
		
		session.close();
		
		if(lsdata.get(0).getAdmin_access().equals("online"))
		{
			return "online";
		}
		else
		{
			if(lsdata.get(0).getAdmin_user_id().equals(user_id) && lsdata.get(0).getAdmin_password().equals(password))
			{				
				Session updateSession = sessionFactory.openSession();
				Transaction tx = updateSession.beginTransaction();

				org.hibernate.Query updateHql = updateSession.createQuery("update AdminDetails set admin_access = :adminAccess , current_access_time = :currentAccessTime , last_access_time = :lastAccessTime where admin_id = :adminId");
				updateHql.setParameter("adminId", 1);
				
				updateHql.setParameter("adminAccess","online");
				updateHql.setParameter("currentAccessTime",new Date());
				updateHql.setParameter("lastAccessTime",lsdata.get(0).getCurrent_access_time());
				
			    int result  = updateHql.executeUpdate();
			    
			    tx.commit();
			    updateSession.close();
			    
				return "correct";
			}
			else
			{
				return "incorrect";
			}
		}
		
	}
	
	public String setAdminAccess(String access)
	{
		Session updateSession = sessionFactory.openSession();
		Transaction tx = updateSession.beginTransaction();

		org.hibernate.Query updateHql = updateSession.createQuery("update AdminDetails set admin_access = :adminAccess where admin_id = :adminId");
		updateHql.setParameter("adminId", 1);
		
		updateHql.setParameter("adminAccess",access);
		
	    int result  = updateHql.executeUpdate();
	    
	    tx.commit();
	    updateSession.close();
	    
	    if(result==1)
		{			
			return "success";	    
		}
		else
		{			
			return "failed";
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<CategoryDetails> getCategoryList()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from CategoryDetails where category_access_level='User/Admin'";
		List<CategoryDetails> lsdata = session.createQuery(hql).list();	
		
		session.close();
		 
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<CityModel> getCityList()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from CityModel";
		List<CityModel> lsdata = session.createQuery(hql).list();	
		
		session.close();
		 
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<LanguageModel> getLanguageList()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from LanguageModel";
		List<LanguageModel> lsdata = session.createQuery(hql).list();	
		
		session.close();
		 
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<FoodTypeModel> getFoodTypeList()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from FoodTypeModel";
		List<FoodTypeModel> lsdata = session.createQuery(hql).list();	
		
		session.close();
		 
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<ShelterType> getShelterTypeList()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from ShelterType";
		List<ShelterType> lsdata = session.createQuery(hql).list();	
		
		session.close();
		 
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<MedicalSpecialistModel> getDoctorSpecialist()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from MedicalSpecialistModel order by specialist_type";
		List<MedicalSpecialistModel> lsdata = session.createQuery(hql).list();	
		
		session.close();
		 
		return lsdata;
	}
	
	public String getSaveEmailDetails(EmailModel emailDetail)
	{
		String txStatus = "";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		
		try 
		{		
			tx=session.beginTransaction();
			session.saveOrUpdate(emailDetail);
			tx.commit();
			
			if(tx.wasCommitted())
			{
				txStatus="success";
			}
			else 
			{
				txStatus="failure";
			}
		}
		catch (Exception he) 
		{
			System.out.println(he.getMessage());
		} 
		finally 
		{
			session.close();
		}
		
		return txStatus;	
	}
	
	public String saveEmbassyModel(EmbassyModel eModel)
	{
		String txStatus = "";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		
		try 
		{		
			tx=session.beginTransaction();
			session.saveOrUpdate(eModel);
			tx.commit();
			
			if(tx.wasCommitted())
			{
				txStatus="success";
			}
			else 
			{
				txStatus="failure";
			}
		}
		catch (Exception he) 
		{
			System.out.println(he.getMessage());
		} 
		finally 
		{
			session.close();
		}
		
		return txStatus;		
	}
	
	
	
	public String saveHospitalModel(HospitalModel eModel)
	{
		String txStatus = "";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		
		try 
		{		
			tx=session.beginTransaction();
			session.saveOrUpdate(eModel);
			tx.commit();
			
			if(tx.wasCommitted())
			{
				txStatus="success";
			}
			else 
			{
				txStatus="failure";
			}
		}
		catch (Exception he) 
		{
			System.out.println(he.getMessage());
		} 
		finally 
		{
			session.close();
		}
		
		return txStatus;		
	}
	
	public String saveTranslatorModel(TranslatorModel eModel)
	{
		String txStatus = "";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		
		try 
		{		
			tx=session.beginTransaction();
			session.saveOrUpdate(eModel);
			tx.commit();
			
			if(tx.wasCommitted())
			{
				txStatus="success";
			}
			else 
			{
				txStatus="failure";
			}
		}
		catch (Exception he) 
		{
			System.out.println(he.getMessage());
		} 
		finally 
		{
			session.close();
		}
		
		return txStatus;		
	}
	
	
	public String saveHotelLodgeModel(HotelLodgeModel eModel)
	{
		String txStatus = "";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		
		try 
		{		
			tx=session.beginTransaction();
			session.saveOrUpdate(eModel);
			tx.commit();
			
			if(tx.wasCommitted())
			{
				txStatus="success";
			}
			else 
			{
				txStatus="failure";
			}
		}
		catch (Exception he) 
		{
			System.out.println(he.getMessage());
		} 
		finally 
		{
			session.close();
		}
		
		return txStatus;		
	}
	
	
	public String saveAttenderNurseModel(AttenderNurseModel eModel)
	{
		String txStatus = "";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		
		try 
		{		
			tx=session.beginTransaction();
			session.saveOrUpdate(eModel);
			tx.commit();
			
			if(tx.wasCommitted())
			{
				txStatus="success";
			}
			else 
			{
				txStatus="failure";
			}
		}
		catch (Exception he) 
		{
			System.out.println(he.getMessage());
		} 
		finally 
		{
			session.close();
		}
		
		return txStatus;		
	}
	
	public String saveDoctorsModel(DoctorsModel eModel)
	{
		String txStatus = "";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		
		try 
		{		
			tx=session.beginTransaction();
			session.saveOrUpdate(eModel);
			tx.commit();
			
			if(tx.wasCommitted())
			{
				txStatus="success";
			}
			else 
			{
				txStatus="failure";
			}
		}
		catch (Exception he) 
		{
			System.out.println(he.getMessage());
		} 
		finally 
		{
			session.close();
		}
		
		return txStatus;		
	}
	
	public String saveRestaurentModel(RestaurentModel eModel)
	{
		String txStatus = "";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		
		try 
		{		
			tx=session.beginTransaction();
			session.saveOrUpdate(eModel);
			tx.commit();
			
			if(tx.wasCommitted())
			{
				txStatus="success";
			}
			else 
			{
				txStatus="failure";
			}
		}
		catch (Exception he) 
		{
			System.out.println(he.getMessage());
		} 
		finally 
		{
			session.close();
		}
		
		return txStatus;		
	}
	
	public String saveTaxiServiceModel(TaxiServiceModel eModel)
	{
		String txStatus = "";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		
		try 
		{		
			tx=session.beginTransaction();
			session.saveOrUpdate(eModel);
			tx.commit();
			
			if(tx.wasCommitted())
			{
				txStatus="success";
			}
			else 
			{
				txStatus="failure";
			}
		}
		catch (Exception he) 
		{
			System.out.println(he.getMessage());
		} 
		finally 
		{
			session.close();
		}
		
		return txStatus;		
	}
	
	
	
	public String saveTouristPlaceDetails(TouriestPlaceDetails placeModel)
	{
		String txStatus = "";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		
		try 
		{		
			tx=session.beginTransaction();
			session.saveOrUpdate(placeModel);
			tx.commit();
			
			if(tx.wasCommitted())
			{
				txStatus="success";
			}
			else 
			{
				txStatus="failure";
			}
		}
		catch (Exception he) 
		{
			System.out.println(he.getMessage());
		} 
		finally 
		{
			session.close();
		}
		
		return txStatus;	
	}
	
	public String saveDoctorDetails(PriorityDoctorDetails doctorModel)
	{
		String txStatus = "";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		
		try 
		{		
			tx=session.beginTransaction();
			session.saveOrUpdate(doctorModel);
			tx.commit();
			
			if(tx.wasCommitted())
			{
				txStatus="success";
			}
			else 
			{
				txStatus="failure";
			}
		}
		catch (Exception he) 
		{
			System.out.println(he.getMessage());
		} 
		finally 
		{
			session.close();
		}
		
		return txStatus;	
	}
	
	
	public String saveAdminDetails(String name,String designation)
	{		
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update AdminDetails set admin_name = :name , admin_designation = :designation where admin_id = :adminId");
		hql.setParameter("adminId", 1);
		
		hql.setParameter("name",name);
		hql.setParameter("designation",designation);
		
	    int result  = hql.executeUpdate();
	    
	    tx.commit();
	    session.close();
	    
		if(result==1)
		{			
			return "success";	    
		}
		else
		{			
			return "failed";
		}
		
	}
	
	
	@SuppressWarnings("unchecked")
	public String validateCurrentPassword(String encCurPassword)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from AdminDetails where admin_id = 1 and admin_password = '"+encCurPassword+"'";
		List<AdminDetails> lsdata = session.createQuery(hql).list();	
		
		session.close();
		 
		if(lsdata.size()>0)
		{
			return "Exist";
		}
		else
		{
			return "NotExist";
		}
		
	}
	
	public String updatePassword(String encNewPassword)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update AdminDetails set admin_password = :encNewPassword where admin_id = :adminId");
		hql.setParameter("adminId", 1);
		
		hql.setParameter("encNewPassword",encNewPassword);
		
	    int result  = hql.executeUpdate();
	    
	    tx.commit();
	    session.close();
	    
		if(result==1)
		{			
			return "success";	    
		}
		else
		{			
			return "failed";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<OpinionDetails> getOpenionReport(Date from_date,Date to_date)
	{
		Session session = sessionFactory.openSession();
		
		Criteria criteria = session.createCriteria(OpinionDetails.class);
		
		criteria.add(Restrictions.ge("cr_date",from_date));
		criteria.add(Restrictions.le("cr_date",to_date));		
		criteria.addOrder(Order.desc("cr_date"));
		
		List<OpinionDetails> eModel = criteria.list();
		
		return eModel;
	}
	
	@SuppressWarnings("unchecked")
	public List<ContactUsModel> getContactReport(Date from_date,Date to_date)
	{
		Session session = sessionFactory.openSession();
		
		Criteria criteria = session.createCriteria(ContactUsModel.class);
		
		criteria.add(Restrictions.ge("contact_date",from_date));
		criteria.add(Restrictions.le("contact_date",to_date));		
		criteria.addOrder(Order.desc("contact_date"));
		
		List<ContactUsModel> eModel = criteria.list();
		
		session.close();
		
		return eModel;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserRegistrationDetails> getUserReport(Date from_date,Date to_date)
	{
		Session session = sessionFactory.openSession();
		
		Criteria criteria = session.createCriteria(UserRegistrationDetails.class);
		
		criteria.add(Restrictions.ge("user_reg_time",from_date));
		criteria.add(Restrictions.le("user_reg_time",to_date));		
		criteria.addOrder(Order.desc("user_reg_time"));
		
		List<UserRegistrationDetails> eModel = criteria.list();
		
		session.close();
		
		return eModel;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserRegistrationDetails> getUserDetailsById(int userId)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from UserRegistrationDetails where user_id="+userId+"";
		List<UserRegistrationDetails> lsdata = session.createQuery(hql).list();	
		

		tx.commit();
		session.close();
		 
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ContactUsModel> getContactDetails(int patternId)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from ContactUsModel where contact_id="+patternId+"";
		List<ContactUsModel> lsdata = session.createQuery(hql).list();	
		

		tx.commit();
		session.close();
		 
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<OpinionDetails> getOpinionDetails(int patternId)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from OpinionDetails where opinion_id="+patternId+"";
		List<OpinionDetails> lsdata = session.createQuery(hql).list();	
		

		tx.commit();
		session.close();
		 
		return lsdata;
	}
	
}
