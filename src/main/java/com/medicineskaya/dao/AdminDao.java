package com.medicineskaya.dao;

import java.util.Date;
import java.util.List;

import com.medicineskaya.model.AdminDetails;
import com.medicineskaya.model.AttenderNurseModel;
import com.medicineskaya.model.CategoryDetails;
import com.medicineskaya.model.CityModel;
import com.medicineskaya.model.ContactUsModel;
import com.medicineskaya.model.DoctorsModel;
import com.medicineskaya.model.EmailModel;
import com.medicineskaya.model.EmbassyModel;
import com.medicineskaya.model.FoodTypeModel;
import com.medicineskaya.model.HospitalModel;
import com.medicineskaya.model.HotelLodgeModel;
import com.medicineskaya.model.LanguageModel;
import com.medicineskaya.model.MedicalSpecialistModel;
import com.medicineskaya.model.OpinionDetails;
import com.medicineskaya.model.PriorityDoctorDetails;
import com.medicineskaya.model.RestaurentModel;
import com.medicineskaya.model.ShelterType;
import com.medicineskaya.model.TaxiServiceModel;
import com.medicineskaya.model.TouriestPlaceDetails;
import com.medicineskaya.model.TranslatorModel;
import com.medicineskaya.model.UserRegistrationDetails;


public interface AdminDao {

	
	public List<AdminDetails> getAdminDetails();
	
	public String getValidateAdmin(String user_id,String password);
	
	public List<CategoryDetails> getCategoryList();
	
	public List<CityModel> getCityList();
	
	public List<LanguageModel> getLanguageList();
	
	public List<FoodTypeModel> getFoodTypeList();
	
	public List<MedicalSpecialistModel> getDoctorSpecialist();
	
	public String saveEmbassyModel(EmbassyModel eModel);
	
	public String saveAttenderNurseModel(AttenderNurseModel eModel);
	
	public String saveHospitalModel(HospitalModel eModel);
	
	public String saveTaxiServiceModel(TaxiServiceModel eModel);
	
	public String saveTranslatorModel(TranslatorModel eModel);
	
	public String saveHotelLodgeModel(HotelLodgeModel eModel);
	
	public String saveDoctorsModel(DoctorsModel eModel);
	
	public String saveRestaurentModel(RestaurentModel eModel);
	
	public String saveTouristPlaceDetails(TouriestPlaceDetails placeModel);
	
	public String saveDoctorDetails(PriorityDoctorDetails doctorModel);
	
	
	public String getSaveEmailDetails(EmailModel emailDetail);
	
	public List<ShelterType> getShelterTypeList();
	
	public String saveAdminDetails(String name,String designation);
	
	public String validateCurrentPassword(String encCurPassword);
	
	public String updatePassword(String encNewPassword);
	
	public String setAdminAccess(String access);
	
	public List<OpinionDetails> getOpenionReport(Date from_date,Date to_date);
	
	public List<ContactUsModel> getContactReport(Date from_date,Date to_date);
	
	public List<UserRegistrationDetails> getUserReport(Date from_date,Date to_date);
	
	public List<UserRegistrationDetails> getUserDetailsById(int userId);
	
	public List<ContactUsModel> getContactDetails(int patternId);
	
	public List<OpinionDetails> getOpinionDetails(int patternId);
	
	
	
	
	
}
