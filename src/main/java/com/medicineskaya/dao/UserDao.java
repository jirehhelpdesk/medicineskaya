package com.medicineskaya.dao;

import java.util.List;

import com.medicineskaya.model.AttenderNurseModel;
import com.medicineskaya.model.CategoryDetails;
import com.medicineskaya.model.CityModel;
import com.medicineskaya.model.ContactUsModel;
import com.medicineskaya.model.CountryModel;
import com.medicineskaya.model.DoctorsModel;
import com.medicineskaya.model.EmbassyModel;
import com.medicineskaya.model.HospitalModel;
import com.medicineskaya.model.HotelLodgeModel;
import com.medicineskaya.model.LinkValidationModel;
import com.medicineskaya.model.OpinionDetails;
import com.medicineskaya.model.PriorityDoctorDetails;
import com.medicineskaya.model.RestaurentModel;
import com.medicineskaya.model.StateModel;
import com.medicineskaya.model.TaxiServiceModel;
import com.medicineskaya.model.TouriestPlaceDetails;
import com.medicineskaya.model.TranslatorModel;
import com.medicineskaya.model.UserRegistrationDetails;


public interface UserDao {

	
	public List<CategoryDetails> getCategoryDetails();
	
	public List<StateModel> getStateDetails();
	
	public List<CityModel> getCityDetails();
	
	public List<TouriestPlaceDetails> getTouriestPlaceDetails();
	
	public List<PriorityDoctorDetails> getPriorityDoctorDetails();
	
	public String checkUserAmbiguity(UserRegistrationDetails userDetails,String parameter);
	
	public String saveUserRegisterDetails(UserRegistrationDetails userDetails);
	
	public String getValidateUser(String userName,String decryptPassword);
	
	public int getUserIdVieEmailId(String userName);
	
	public List<UserRegistrationDetails> getUserDetails(int userId);
	
	public String getCategoryValues(String category,String cityName);
	
	
	
	public List<EmbassyModel> getSearchEmbassy(String cityName,String valueName);
	
	public List<TranslatorModel> getSearchTranslator(String cityName,String valueName);
	
	public List<HotelLodgeModel> getSearchHotel(String cityName,String valueName);
	
	public List<AttenderNurseModel> getSearchAttender(String cityName,String valueName);
	
	public List<DoctorsModel> getSearchDoctor(String cityName,String valueName);
	
	public List<RestaurentModel> getSearchRestaurent(String cityName,String valueName);
	
	public List<TaxiServiceModel> getSearchTaxi(String cityName,String valueName);
	
	public List<HospitalModel> getSearchHospital(String cityName,String valueName);
	
	
	
	public List<EmbassyModel> getEmbassyDetails(int id,String condition);
	
	public List<TranslatorModel> getTranslatorDetails(int id,String condition);
	
	public List<HotelLodgeModel> getHotelDetails(int id,String condition);
	
	public List<AttenderNurseModel> getAttenderDetails(int id,String condition);
	
	public List<DoctorsModel> getDoctorDetails(int id,String condition);
	
	public List<RestaurentModel> getRestaurentDetails(int id,String condition);
	
	public List<TaxiServiceModel> getTaxiDetails(int id,String condition);
	
	public List<HospitalModel> getHospitalDetails(int id,String condition);
	
	
	public String saveContactDetails(ContactUsModel contactModel);
	
	public String saveSecondOpinion(OpinionDetails opinionModel);
	
	public List<DoctorsModel> getDoctorList(String specialization);
	
	
	public List<TouriestPlaceDetails> getSearchTouriestPlaceDetails(int placeId);
	
	public List<PriorityDoctorDetails> getSearchDoctorDetails(int doctorId);
	
	public String checkCurrentPassword(int userId,String encryptCurrentPw);
	
	public String updateCurrentPassword(int userId,String encryptNewPw);
	
	public String saveLinkValidatedetails(LinkValidationModel linkValidate);
	
	public String getEmailIdFromForgetPassword(String uniqueId);
	
	public String getChangePassword(String emailId,String encPassword);
	
	public List<CountryModel> getCountryList();
	
	
}

