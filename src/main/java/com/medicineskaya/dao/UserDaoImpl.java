package com.medicineskaya.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.medicineskaya.model.AdminDetails;
import com.medicineskaya.model.AttenderNurseModel;
import com.medicineskaya.model.CategoryDetails;
import com.medicineskaya.model.CityModel;
import com.medicineskaya.model.ContactUsModel;
import com.medicineskaya.model.CountryModel;
import com.medicineskaya.model.DoctorsModel;
import com.medicineskaya.model.EmbassyModel;
import com.medicineskaya.model.HospitalModel;
import com.medicineskaya.model.HotelLodgeModel;
import com.medicineskaya.model.LinkValidationModel;
import com.medicineskaya.model.OpinionDetails;
import com.medicineskaya.model.PriorityDoctorDetails;
import com.medicineskaya.model.RestaurentModel;
import com.medicineskaya.model.StateModel;
import com.medicineskaya.model.TaxiServiceModel;
import com.medicineskaya.model.TouriestPlaceDetails;
import com.medicineskaya.model.TranslatorModel;
import com.medicineskaya.model.UserRegistrationDetails;



@Repository("userDao")
public class UserDaoImpl implements UserDao {

	static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@SuppressWarnings("unchecked")
	public List<CategoryDetails> getCategoryDetails()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from CategoryDetails  order by category_name";			
		
		List<CategoryDetails> lsdata = session.createQuery(hql).list();	
		
		session.close();

		return lsdata;		
	}
	
	@SuppressWarnings("unchecked")
	public List<StateModel> getStateDetails()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from StateModel order by state_name";			
		
		List<StateModel> lsdata = session.createQuery(hql).list();	
		
		session.close();

		return lsdata;		
	}
	
	@SuppressWarnings("unchecked")
	public List<CityModel> getCityDetails()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from CityModel order by city_name";			
		
		List<CityModel> lsdata = session.createQuery(hql).list();	
		
		session.close();

		return lsdata;		
	}
	
	@SuppressWarnings("unchecked")
	public List<TouriestPlaceDetails> getTouriestPlaceDetails()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from TouriestPlaceDetails order by place_name";			
		
		List<TouriestPlaceDetails> lsdata = session.createQuery(hql).list();	
		
		session.close();

		return lsdata;		
	}
	
	@SuppressWarnings("unchecked")
	public List<PriorityDoctorDetails> getPriorityDoctorDetails()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from PriorityDoctorDetails";			 /*order by doctor_name*/
		
		List<PriorityDoctorDetails> lsdata = session.createQuery(hql).list();	
		
		session.close();

		return lsdata;		
	}
	
	@SuppressWarnings("unchecked")
	public String checkUserAmbiguity(UserRegistrationDetails userDetails,String parameter)
	{		
			Session session = sessionFactory.openSession();
			String hql = "";
			List<UserRegistrationDetails> lsdata = new ArrayList<UserRegistrationDetails>();
			
			if(parameter.equals("Email Id"))
			{
				hql = "from UserRegistrationDetails where user_emailid='"+userDetails.getUser_emailid()+"'";
				lsdata = session.createQuery(hql).list();				
			}
			else
			{
				hql = "from UserRegistrationDetails where user_mobile='"+userDetails.getUser_mobile()+"'";
				lsdata = session.createQuery(hql).list();				
			}
			
			session.close();
			
			if(lsdata.size()>0)
			{
				return "Exist";
			}
			else
			{
				return "NotExist";
			}		
	}
	
	
	public String saveUserRegisterDetails(UserRegistrationDetails userDetails)
	{
		String txStatus = "";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		
		try 
		{		
			tx=session.beginTransaction();
			session.saveOrUpdate(userDetails);
			tx.commit();
			
			if(tx.wasCommitted())
			{
				txStatus="success";
			}
			else 
			{
				txStatus="failure";
			}
		}
		catch (Exception he) 
		{
			System.out.println(he.getMessage());
		} 
		finally 
		{
			session.close();
		}
		
		return txStatus;		
	}
	
	@SuppressWarnings("unchecked")
	public String getValidateUser(String userName,String decryptPassword)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "";
		if(userName.contains("@"))
		{
			hql = "select user_status from UserRegistrationDetails where user_emailid = '"+userName+"' and user_password='"+decryptPassword+"'";			
		}
		else
		{
			hql = "select user_status from UserRegistrationDetails where user_mobile = '"+userName+"' and user_password='"+decryptPassword+"'";			
		}
		 
		List<String> lsdata = session.createQuery(hql).list();	
		
		session.close();

		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "Invalid";
		}	
	}
	
	@SuppressWarnings("unchecked")
	public int getUserIdVieEmailId(String userName)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from UserRegistrationDetails where user_emailid = '"+userName+"'";			
		
		List<UserRegistrationDetails> lsdata = session.createQuery(hql).list();	
		
		session.close();

		if(lsdata.size()>0)
		{
			return lsdata.get(0).getUser_id();
		}
		else
		{
			return 0;
		}	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserRegistrationDetails> getUserDetails(int userId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from UserRegistrationDetails where user_id = "+userId+"";			
		
		List<UserRegistrationDetails> lsdata = session.createQuery(hql).list();	
		
		session.close();

		return lsdata;		
	}
	
	
	@SuppressWarnings("unchecked")
	public String getCategoryValues(String category,String cityName)
	{
		String result = "";
		
		Session session = sessionFactory.openSession();
		List<String> lsdata = new ArrayList<String>();
		
		if(category.equals("Embassy"))
		{
			String hql = "select embassy_name from EmbassyModel where embassy_city = '"+cityName+"' order by embassy_name";			
			
			lsdata = session.createQuery(hql).list();			
		}
		else if(category.equals("Hospital"))
		{
			String hql = "select hospital_name from HospitalModel where hospital_city = '"+cityName+"' order by hospital_name";			
			
			lsdata = session.createQuery(hql).list();
		}
		else if(category.equals("Translator"))
		{
			String hql = "select language_name from LanguageModel";			
			
			lsdata = session.createQuery(hql).list();
		}
		else if(category.equals("Hotel and Lodge"))
		{
			String hql = "select DISTINCT hotel_type from HotelLodgeModel where hotel_city = '"+cityName+"' order by hotel_type";			
			
			lsdata = session.createQuery(hql).list();						
		}
		else if(category.equals("Attender and Nurse"))
		{
			String hql = "select DISTINCT attender_gender from AttenderNurseModel where attender_city = '"+cityName+"'";			
			
			lsdata = session.createQuery(hql).list();
		}
		else if(category.equals("Doctor"))
		{
			String hql = "select DISTINCT doctor_specialist from DoctorsModel where doctor_city = '"+cityName+"' order by doctor_specialist";			
			
			lsdata = session.createQuery(hql).list();
		}
		else if(category.equals("Type of food"))
		{
			String hql = "select DISTINCT type_of_food from RestaurentModel where restaurent_city = '"+cityName+"'";			
			
			lsdata = session.createQuery(hql).list();			
		}
		else if(category.equals("Restaurent"))
		{
			String hql = "select DISTINCT type_of_food from RestaurentModel where restaurent_city = '"+cityName+"'";			
			
			lsdata = session.createQuery(hql).list();			
		}
		else if(category.equals("Taxi Service"))
		{
			String hql = "select DISTINCT service_type from TaxiServiceModel where service_city = '"+cityName+"' order by service_type";			
			
			lsdata = session.createQuery(hql).list();
		}
		else
		{
			
		}
		
		session.close();
		
		for(int i=0;i<lsdata.size();i++)
		{
			result += lsdata.get(i) + ","; 
		}
		
		if(result.length()>2)
		{
			result = result.substring(0,result.length()-1);
		}
		
		return result;	
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<EmbassyModel> getSearchEmbassy(String cityName,String valueName)
	{
		Session session = sessionFactory.openSession();
		List<EmbassyModel> lsdata = new ArrayList<EmbassyModel>();
		
		if(valueName.equals("All"))
		{
			String hql = "from EmbassyModel where embassy_city = '"+cityName+"'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from EmbassyModel where embassy_city = '"+cityName+"' and embassy_name='"+valueName+"'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<TranslatorModel> getSearchTranslator(String cityName,String valueName)
	{
		Session session = sessionFactory.openSession();
		List<TranslatorModel> lsdata = new ArrayList<TranslatorModel>();
		
		if(valueName.equals("All"))
		{
			String hql = "from TranslatorModel where translator_city = '"+cityName+"'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from TranslatorModel where translator_city = '"+cityName+"' and known_language LIKE '%"+valueName+"%'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<HotelLodgeModel> getSearchHotel(String cityName,String valueName)
	{
		Session session = sessionFactory.openSession();
		List<HotelLodgeModel> lsdata = new ArrayList<HotelLodgeModel>();
		
		if(valueName.equals("All"))
		{
			String hql = "from HotelLodgeModel where hotel_city = '"+cityName+"'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from HotelLodgeModel where hotel_city = '"+cityName+"' and hotel_type='"+valueName+"'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<AttenderNurseModel> getSearchAttender(String cityName,String valueName)
	{
		Session session = sessionFactory.openSession();
		List<AttenderNurseModel> lsdata = new ArrayList<AttenderNurseModel>();
		
		if(valueName.equals("All"))
		{
			String hql = "from AttenderNurseModel where attender_city = '"+cityName+"'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from AttenderNurseModel where attender_city = '"+cityName+"' and attender_gender='"+valueName+"'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<DoctorsModel> getSearchDoctor(String cityName,String valueName)
	{
		Session session = sessionFactory.openSession();
		List<DoctorsModel> lsdata = new ArrayList<DoctorsModel>();
		
		if(valueName.equals("All"))
		{
			String hql = "from DoctorsModel where doctor_city = '"+cityName+"'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from DoctorsModel where doctor_city = '"+cityName+"' and doctor_specialist='"+valueName+"'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<RestaurentModel> getSearchRestaurent(String cityName,String valueName)
	{
		Session session = sessionFactory.openSession();
		List<RestaurentModel> lsdata = new ArrayList<RestaurentModel>();
		
		if(valueName.equals("All"))
		{
			String hql = "from RestaurentModel where restaurent_city = '"+cityName+"'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from RestaurentModel where restaurent_city = '"+cityName+"' and type_of_food='"+valueName+"'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxiServiceModel> getSearchTaxi(String cityName,String valueName)
	{
		Session session = sessionFactory.openSession();
		List<TaxiServiceModel> lsdata = new ArrayList<TaxiServiceModel>();
		
		if(valueName.equals("All"))
		{
			String hql = "from TaxiServiceModel where service_city = '"+cityName+"'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from TaxiServiceModel where service_city = '"+cityName+"' and service_type='"+valueName+"'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<HospitalModel> getSearchHospital(String cityName,String valueName)
	{
		Session session = sessionFactory.openSession();
		List<HospitalModel> lsdata = new ArrayList<HospitalModel>();
		
		if(valueName.equals("All"))
		{
			String hql = "from HospitalModel where hospital_city = '"+cityName+"'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from HospitalModel where hospital_city = '"+cityName+"' and hospital_name='"+valueName+"'";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<EmbassyModel> getEmbassyDetails(int id,String condition)
	{
		Session session = sessionFactory.openSession();
		List<EmbassyModel> lsdata = new ArrayList<EmbassyModel>();
		
		if(condition.equals("All"))
		{
			String hql = "from EmbassyModel";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from EmbassyModel where embassy_id = "+id+"";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<TranslatorModel> getTranslatorDetails(int id,String condition)
	{
		Session session = sessionFactory.openSession();
		List<TranslatorModel> lsdata = new ArrayList<TranslatorModel>();
		
		if(condition.equals("All"))
		{
			String hql = "from TranslatorModel";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from TranslatorModel where translator_id = "+id+"";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<HotelLodgeModel> getHotelDetails(int id,String condition)
	{
		Session session = sessionFactory.openSession();
		List<HotelLodgeModel> lsdata = new ArrayList<HotelLodgeModel>();
		
		if(condition.equals("All"))
		{
			String hql = "from HotelLodgeModel";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from HotelLodgeModel where hotel_id = "+id+"";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<AttenderNurseModel> getAttenderDetails(int id,String condition)
	{
		Session session = sessionFactory.openSession();
		List<AttenderNurseModel> lsdata = new ArrayList<AttenderNurseModel>();
		
		if(condition.equals("All"))
		{
			String hql = "from AttenderNurseModel";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from AttenderNurseModel where attender_id = "+id+"";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<DoctorsModel> getDoctorDetails(int id,String condition)
	{
		Session session = sessionFactory.openSession();
		List<DoctorsModel> lsdata = new ArrayList<DoctorsModel>();
		
		if(condition.equals("All"))
		{
			String hql = "from DoctorsModel";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from DoctorsModel where doctor_id = "+id+"";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<RestaurentModel> getRestaurentDetails(int id,String condition)
	{
		Session session = sessionFactory.openSession();
		List<RestaurentModel> lsdata = new ArrayList<RestaurentModel>();
		
		if(condition.equals("All"))
		{
			String hql = "from RestaurentModel";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from RestaurentModel where restaurent_id = "+id+"";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxiServiceModel> getTaxiDetails(int id,String condition)
	{
		Session session = sessionFactory.openSession();
		List<TaxiServiceModel> lsdata = new ArrayList<TaxiServiceModel>();
		
		if(condition.equals("All"))
		{
			String hql = "from TaxiServiceModel";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from TaxiServiceModel where service_id = "+id+"";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<HospitalModel> getHospitalDetails(int id,String condition)
	{
		Session session = sessionFactory.openSession();
		List<HospitalModel> lsdata = new ArrayList<HospitalModel>();
		
		if(condition.equals("All"))
		{
			String hql = "from HospitalModel";			
			
			lsdata = session.createQuery(hql).list();	
		}
		else
		{
			String hql = "from HospitalModel where hospital_id = "+id+"";			
			
			lsdata = session.createQuery(hql).list();	
		}
		
		session.close();
		
		return lsdata;
	}
	
	
	public String saveContactDetails(ContactUsModel contactModel)
	{
		String txStatus = "";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		
		try 
		{		
			tx=session.beginTransaction();
			session.saveOrUpdate(contactModel);
			tx.commit();
			
			if(tx.wasCommitted())
			{
				txStatus="success";
			}
			else 
			{
				txStatus="failure";
			}
		}
		catch (Exception he) 
		{
			System.out.println(he.getMessage());
		} 
		finally 
		{
			session.close();
		}
		
		return txStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<DoctorsModel> getDoctorList(String specialization)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from DoctorsModel where doctor_specialist = '"+specialization+"' order by doctor_name";			
			
		List<DoctorsModel> lsdata = session.createQuery(hql).list();	
		
		session.close();
		
		return lsdata;
	}
	
	
	public String saveSecondOpinion(OpinionDetails opinionModel)
	{
		String txStatus = "";
    	
    	Session session=sessionFactory.openSession();
		Transaction tx=null;
		
		try 
		{		
			tx=session.beginTransaction();
			session.saveOrUpdate(opinionModel);
			tx.commit();
			
			if(tx.wasCommitted())
			{
				txStatus="success";
			}
			else 
			{
				txStatus="failure";
			}
		}
		catch (Exception he) 
		{
			System.out.println(he.getMessage());
		} 
		finally 
		{
			session.close();
		}
		
		return txStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<TouriestPlaceDetails> getSearchTouriestPlaceDetails(int placeId)
	{
		Session session = sessionFactory.openSession();
		
		if(placeId==0)
		{
			String hql = "from TouriestPlaceDetails order by place_name";			
			
			List<TouriestPlaceDetails> lsdata = session.createQuery(hql).list();	
			
			session.close();

			return lsdata;
		}
		else
		{
			String hql = "from TouriestPlaceDetails where place_id="+placeId+"";					
			
			List<TouriestPlaceDetails> lsdata = session.createQuery(hql).list();	
			
			session.close();

			return lsdata;
		}
		
	}
	
	
	@SuppressWarnings("unchecked")
	public List<PriorityDoctorDetails> getSearchDoctorDetails(int doctorId)
	{
		Session session = sessionFactory.openSession();
		
		if(doctorId==0)
		{
			String hql = "from PriorityDoctorDetails order by doctor_name";			
			
			List<PriorityDoctorDetails> lsdata = session.createQuery(hql).list();	
			
			session.close();

			return lsdata;
		}
		else
		{
			String hql = "from PriorityDoctorDetails where priority_doctor_id="+doctorId+"";					
			
			List<PriorityDoctorDetails> lsdata = session.createQuery(hql).list();	
			
			session.close();

			return lsdata;
		}
	}
	
	@SuppressWarnings("unchecked")
	public String checkCurrentPassword(int userId,String encryptCurrentPw)
	{
		Session session = sessionFactory.openSession();
		String hql = "";
		
		hql = "from UserRegistrationDetails where user_id="+userId+" and user_password='"+encryptCurrentPw+"'";
		List<UserRegistrationDetails> lsdata = session.createQuery(hql).list();				
		
		session.close();
		
		if(lsdata.size()>0)
		{
			return "Matched";
		}
		else
		{
			return "NotMatched";
		}		
	}
	
	
	public String updateCurrentPassword(int userId,String encryptNewPw)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update UserRegistrationDetails set user_password = :encryptNewPw where user_id = :userId");
		hql.setParameter("userId", userId);
		
		hql.setParameter("encryptNewPw",encryptNewPw);
		
	    int result  = hql.executeUpdate();
	    
	    tx.commit();
	    session.close();
	    
		if(result==1)
		{			
			return "success";	    
		}
		else
		{			
			return "failed";
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public String saveLinkValidatedetails(LinkValidationModel linkValidate)
	{
		String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(linkValidate);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;	
	}
	
	
	@SuppressWarnings("unchecked")
	public String getEmailIdFromForgetPassword(String uniqueId)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "select validate_email_id from LinkValidationModel where validate_id = '"+uniqueId+"' and validate_init_date=(select MAX(validate_init_date) from LinkValidationModel where validate_id = '"+uniqueId+"' and validate_status='Pending')";
		List<String> lsdata = session.createQuery(hql).list();	
		
		tx.commit();
		session.close();

		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "Not Exist";
		}	
	}
	
	
	@SuppressWarnings("unchecked")
	public String getChangePassword(String emailId,String encPassword)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update UserRegistrationDetails set user_password = :encPassword where user_emailid = :emailId");
		hql.setParameter("emailId", emailId);
		
		hql.setParameter("encPassword",encPassword);
		
	    int result  = hql.executeUpdate();
	    
		if(result==1)
		{			
			String uniqueId = "select validate_id from LinkValidationModel where validate_email_id = '"+emailId+"' and validate_init_date=(select MAX(validate_init_date) from LinkValidationModel where validate_email_id = '"+emailId+"' and validate_status='Pending')";
			List<String> lsdata = session.createQuery(uniqueId).list();	
			
			if(lsdata.size()>0)
			{
				org.hibernate.Query updateHql = session.createQuery("update LinkValidationModel set validate_status = :setStatus where validate_id = :uniqueId");
				updateHql.setParameter("uniqueId", lsdata.get(0));			
				updateHql.setParameter("setStatus", "Over");
				
			    int update  = updateHql.executeUpdate();
				
			    tx.commit();
			    session.close();

			    if(update==1)
				{
			    	return "success";
				}
			    else
			    {
			    	return "failed";
			    }	
			}
			else
			{
				return "failed";
			}			    
		}
		else
		{			
			return "failed";
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<CountryModel> getCountryList()
	{
		Session session = sessionFactory.openSession();
		String hql = "";
		
		hql = "from CountryModel";
		List<CountryModel> lsdata = session.createQuery(hql).list();				
		
		session.close();
		
		return lsdata;			
	}
	
	
	
	
}
