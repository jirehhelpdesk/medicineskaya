package com.medicineskaya.service;

import java.util.List;

import com.medicineskaya.bean.ResultCategoryBean;
import com.medicineskaya.bean.UserBean;
import com.medicineskaya.model.CategoryDetails;
import com.medicineskaya.model.CityModel;
import com.medicineskaya.model.ContactUsModel;
import com.medicineskaya.model.CountryModel;
import com.medicineskaya.model.OpinionDetails;
import com.medicineskaya.model.PriorityDoctorDetails;
import com.medicineskaya.model.StateModel;
import com.medicineskaya.model.TouriestPlaceDetails;
import com.medicineskaya.model.UserRegistrationDetails;



public interface UserService {
	
	
	public List<CategoryDetails> getCategoryDetails();
	
	public List<StateModel> getStateDetails();
	
	public List<CityModel> getCityDetails();
	
	public String getStateNames(String countryName);
	
	public List<TouriestPlaceDetails> getTouriestPlaceDetails();
	
	public List<TouriestPlaceDetails> getSearchTouriestPlaceDetails(int placeId);
	
	public List<PriorityDoctorDetails> getSearchDoctorDetails(int doctorId);
	
	public List<PriorityDoctorDetails> getPriorityDoctorDetails();
	
	public String checkUserAmbiguity(UserRegistrationDetails userDetails,String parameter);

	public String saveUserRegisterDetails(UserRegistrationDetails userDetails);
	
	public String getValidateUser(String userName,String password);
	
	public int getUserIdVieEmailId(String userName);
	
	public UserBean getUserDetails(int userId);
	
	public String getCategoryValues(String category,String cityName);
	
	public List<ResultCategoryBean> getSearchRequiredDetails(String category,String cityName,String valueName);
	
	public List<ResultCategoryBean> getViewMoreAboutCategory(int id,String categoryType);
	
	public String saveContactDetails(ContactUsModel contactModel);
	
	public String saveSecondOpinion(OpinionDetails opinionModel);
	
	public String getDoctorList(String specialization);
	
	
	public String updatePassword(int userId,String currentPw,String newPw);
	
	public String sendLinkForgetPassword(String emailId);
	
	public String getEmailIdFromForgetPassword(String uniqueId);
	
	public String getChangePassword(String emailId,String password);
	
	public List<CountryModel> getCountryList();
	
}