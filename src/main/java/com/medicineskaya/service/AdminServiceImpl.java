package com.medicineskaya.service;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.medicineskaya.bean.SecondOpinionBean;
import com.medicineskaya.bean.UserBean;
import com.medicineskaya.dao.AdminDao;
import com.medicineskaya.model.AdminDetails;
import com.medicineskaya.model.AttenderNurseModel;
import com.medicineskaya.model.CategoryDetails;
import com.medicineskaya.model.CityModel;
import com.medicineskaya.model.ContactUsModel;
import com.medicineskaya.model.DoctorsModel;
import com.medicineskaya.model.EmbassyModel;
import com.medicineskaya.model.FoodTypeModel;
import com.medicineskaya.model.HospitalModel;
import com.medicineskaya.model.HotelLodgeModel;
import com.medicineskaya.model.LanguageModel;
import com.medicineskaya.model.MedicalSpecialistModel;
import com.medicineskaya.model.OpinionDetails;
import com.medicineskaya.model.PriorityDoctorDetails;
import com.medicineskaya.model.RestaurentModel;
import com.medicineskaya.model.ShelterType;
import com.medicineskaya.model.TaxiServiceModel;
import com.medicineskaya.model.TouriestPlaceDetails;
import com.medicineskaya.model.TranslatorModel;
import com.medicineskaya.model.UserRegistrationDetails;
import com.medicineskaya.util.ConvertStringToDate;
import com.medicineskaya.util.PasswordGenerator;


@Service("adminService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AdminServiceImpl implements AdminService  {

	
	@Autowired
	private AdminDao adminDao;
	
	
	public List<AdminDetails> getAdminDetails()
	{
		return adminDao.getAdminDetails();
	}
	
	PasswordGenerator pwGenerator = new PasswordGenerator();
	
	
	
	@Transactional(readOnly = false)
	public String getValidateAdmin(String user_id,String password)
	{
		String encPassword = "";
		try {
			 encPassword = pwGenerator.encrypt(password);
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return adminDao.getValidateAdmin(user_id,encPassword);
	}
	
	public String setAdminAccess(String access)
	{
		return adminDao.setAdminAccess(access);
	}
	
	public List<CategoryDetails> getCategoryList()
	{
		return adminDao.getCategoryList();
	}
	
	public List<CityModel> getCityList()
	{
		return adminDao.getCityList();
	}
	
	public List<LanguageModel> getLanguageList()
	{
		return adminDao.getLanguageList();
	}
	
	public List<FoodTypeModel> getFoodTypeList()
	{
		return adminDao.getFoodTypeList();
	}
	
	public List<ShelterType> getShelterTypeList()
	{
		return adminDao.getShelterTypeList();
	}
	
	public List<MedicalSpecialistModel> getDoctorSpecialist()
	{
		return adminDao.getDoctorSpecialist();
	}
	
	public String saveCategoryDetails(MultipartHttpServletRequest request)
	{
		String fileName = "";
		String status = "";
		String renewName = "";
		
		String categoryType = request.getParameter("category_type");
		String categoryName = request.getParameter("name");
		
		
		// File Storage location
				
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/MKFileDirectories"); 	    
	    String filePath = fileResource.getString("categoryRoot");	    		
	    File ourPath = new File(filePath);	    
	    String storePath = ourPath.getPath()+"/"+""+"/"+categoryType+"/";	 	    
	    
	    //Document nameing Convention
	    String reName = "";
	    
	    reName = categoryName.replaceAll(" ","_");
	    
	    MultipartFile multipartFile = request.getFile("file");
	    
	    fileName = multipartFile.getOriginalFilename();
	   
	    String extension = FilenameUtils.getExtension(fileName); // returns "extension"
		
	    File fld = new File(storePath);
	    	
	        if(!fld.exists())
	    	{
			    fld.mkdirs();
	    	}
	        try 
	        {
	        	 multipartFile.transferTo(new File(storePath+fileName));
	        	
	        	 File f = new File(storePath+fileName); 
	        	 
	        	 renewName = reName+"."+extension;
	        	 
	             f.renameTo(new File(storePath+renewName));
	                
			} 
	        catch (Exception e) 
	        {
				// TODO Auto-generated catch block
				e.printStackTrace();					                    
	        }		        
	        
	        // End of saving File	
				
			        
			        
			        
		if(categoryType.equals("Embassy"))
		{
			EmbassyModel eModel = setEmbassyModel(request);
			eModel.setEmbassy_photo(renewName);
			
			status = adminDao.saveEmbassyModel(eModel);
			
		}
		else if(categoryType.equals("Hospital"))
		{
			HospitalModel eModel = setHospitalModel(request);
			eModel.setHospital_photo(renewName);
			
			status = adminDao.saveHospitalModel(eModel);
		}
		else if(categoryType.equals("Translator"))
		{			
			TranslatorModel eModel = setTranslatorModel(request);
			eModel.setTranslator_photo(renewName);
			
			status = adminDao.saveTranslatorModel(eModel);
		}
		else if(categoryType.equals("Hotel and Lodge"))
		{
			HotelLodgeModel eModel = setHotelLodgeModel(request);			
			eModel.setHotel_photo(renewName);
			
			status = adminDao.saveHotelLodgeModel(eModel);			
		}
		else if(categoryType.equals("Attender and Nurse"))
		{
			AttenderNurseModel eModel = setAttenderNurseModel(request);
			eModel.setAttender_photo(renewName);
			
			status = adminDao.saveAttenderNurseModel(eModel);
		}
		else if(categoryType.equals("Doctor"))
		{
			DoctorsModel eModel = setDoctorsModel(request);
			eModel.setDoctor_photo(renewName);
			
			status = adminDao.saveDoctorsModel(eModel);
		}
		else if(categoryType.equals("Restaurent"))
		{
			RestaurentModel eModel = setRestaurentModel(request);
			eModel.setRestaurent_photo(renewName);
			
			status = adminDao.saveRestaurentModel(eModel);
		}
		else if(categoryType.equals("Taxi Service"))
		{
			TaxiServiceModel eModel = setTaxiServiceModel(request);
			eModel.setService_photo(renewName);
			
			status = adminDao.saveTaxiServiceModel(eModel);
		}
		else
		{
			
		}
		
		
		return status;
	}
	
	
    public String saveTouristPlaceDetails(MultipartHttpServletRequest request,TouriestPlaceDetails placeModel)
    {
    	String fileName = "";
		String status = "";
		String renewName = "";
		
		String placeName = placeModel.getPlace_name();
		placeName = placeName.replaceAll(" ","");
		
		
		// File Storage location
				
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/MKFileDirectories"); 	    
	    String filePath = fileResource.getString("portalRoot");	    		
	    File ourPath = new File(filePath);	    
	    String storePath = ourPath.getPath()+"/"+""+"/TouristPlaces/";	 	    
	    
	    //Document nameing Convention
	    String reName = "";
	    
	    reName = placeName;
	    
	    MultipartFile multipartFile = request.getFile("file");
	    
	    fileName = multipartFile.getOriginalFilename();
	   
	    String extension = FilenameUtils.getExtension(fileName); // returns "extension"
		
	    File fld = new File(storePath);
	    	
	        if(!fld.exists())
	    	{
			    fld.mkdirs();
	    	}
	        try 
	        {
	        	 multipartFile.transferTo(new File(storePath+fileName));
	        	
	        	 File f = new File(storePath+fileName); 
	        	 
	        	 renewName = reName+"."+extension;
	        	 
	             f.renameTo(new File(storePath+renewName));
	                
			} 
	        catch (Exception e) 
	        {
				// TODO Auto-generated catch block
				e.printStackTrace();					                    
	        }		        
	        
	        // End of saving File
	        
	        placeModel.setPlace_photo(renewName);
	        placeModel.setCr_date(new Date());
	        
	        status = adminDao.saveTouristPlaceDetails(placeModel);
	        
	        return status;
    }
	
	public String saveDoctorDetails(MultipartHttpServletRequest request,PriorityDoctorDetails doctorModel)
	{
		String fileName = "";
		String status = "";
		String renewName = "";
		
		String doctorName = doctorModel.getDoctor_name();
		doctorName = doctorName.replaceAll(" ","");
		
		// File Storage location
				
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/MKFileDirectories"); 	    
	    String filePath = fileResource.getString("portalRoot");	    		
	    File ourPath = new File(filePath);	    
	    String storePath = ourPath.getPath()+"/"+""+"/PriorityDoctor/";	 	    
	    
	    //Document nameing Convention
	    String reName = "";
	    
	    reName = doctorName;
	    
	    MultipartFile multipartFile = request.getFile("file");
	    
	    fileName = multipartFile.getOriginalFilename();
	   
	    String extension = FilenameUtils.getExtension(fileName); // returns "extension"
		
	    File fld = new File(storePath);
	    	
	        if(!fld.exists())
	    	{
			    fld.mkdirs();
	    	}
	        try 
	        {
	        	 multipartFile.transferTo(new File(storePath+fileName));
	        	
	        	 File f = new File(storePath+fileName); 
	        	 
	        	 renewName = reName+"."+extension;
	        	 
	             f.renameTo(new File(storePath+renewName));
	                
			} 
	        catch (Exception e) 
	        {
				// TODO Auto-generated catch block
				e.printStackTrace();					                    
	        }		        
	        
	        // End of saving File
	        
	        doctorModel.setDoctor_photo(renewName);
	        doctorModel.setCr_date(new Date());
	        
	        status = adminDao.saveDoctorDetails(doctorModel);
	        
	        return status;
	}
	
	
	public String saveAdminDetails(String name,String designation)
	{
		return adminDao.saveAdminDetails(name,designation);
	}
	
	
	public String saveAdminPassword(String password,String new_password)
	{
		String encCurPassword = "";
		String encNewPassword = "";
		
		try {
			
			encCurPassword += pwGenerator.encrypt(password);
			encNewPassword += pwGenerator.encrypt(new_password);
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String status = "";
		
		status = adminDao.validateCurrentPassword(encCurPassword);
		
		if(status.equals("Exist"))
		{
			status = adminDao.updatePassword(encNewPassword);
			
			return status;
		}
		else
		{
			return status;
		}
		
	}
	
	
	ConvertStringToDate dateObj = new ConvertStringToDate();
	
	public List<SecondOpinionBean> getOpenionReport(String fromDate,String toDate)
	{
		Date from_date = dateObj.convertStringIntoDate(fromDate);
		Date to_date = dateObj.convertStringIntoDate(toDate);
		
		List<OpinionDetails> eModel = adminDao.getOpenionReport(from_date,to_date);
		
		List<SecondOpinionBean> userBean = getConverOpinionModelToBean(eModel);
		
		return userBean;
	}
	
	public List<UserBean> getUserReport(String fromDate,String toDate)
	{
		Date from_date = dateObj.convertStringIntoDate(fromDate);
		Date to_date = dateObj.convertStringIntoDate(toDate);
		
		List<UserRegistrationDetails> eModel = adminDao.getUserReport(from_date,to_date);
		
		List<UserBean> userBean = getConverUserModelToBean(eModel);
		
		return userBean;
	}
	
	public List<ContactUsModel> getContactReport(String fromDate,String toDate)
	{
		Date from_date = dateObj.convertStringIntoDate(fromDate);
		Date to_date = dateObj.convertStringIntoDate(toDate);
		
		return adminDao.getContactReport(from_date,to_date);
	}
	
	
	public List<ContactUsModel> getContactDetails(int patternId)
	{
		return adminDao.getContactDetails(patternId);
	}
	
	public List<SecondOpinionBean> getOpinionDetails(int patternId)
	{
		List<SecondOpinionBean> eBean = new ArrayList<SecondOpinionBean>();
		
		List<OpinionDetails> eModel =  adminDao.getOpinionDetails(patternId);
		
		eBean = getConverOpinionModelToBean(eModel);
		
		return eBean;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	////////////////     SET FILLED VALUES WITH THE MODEL  ////////////////////////
	
	
	
	public EmbassyModel setEmbassyModel(MultipartHttpServletRequest request)
	{
		EmbassyModel eModel = new EmbassyModel();
		
		eModel.setEmbassy_name(request.getParameter("name"));
		eModel.setEmbassy_email_id(request.getParameter("email"));
		eModel.setEmbassy_phone(request.getParameter("phone"));
		eModel.setEmbassy_address(request.getParameter("address"));
		eModel.setEmbassy_city(request.getParameter("city_name"));
		eModel.setCr_date(new Date());
		
		return eModel;
	}
	
	public HospitalModel setHospitalModel(MultipartHttpServletRequest request)
	{
		HospitalModel eModel = new HospitalModel();
		
		eModel.setHospital_name(request.getParameter("name"));
		eModel.setHospital_email_id(request.getParameter("email"));
		eModel.setHospital_phone(request.getParameter("phone"));
		eModel.setHospital_address(request.getParameter("address"));
		eModel.setHospital_city(request.getParameter("city_name"));
		eModel.setCr_date(new Date());
		
		return eModel;
	}
	
	public TranslatorModel setTranslatorModel(MultipartHttpServletRequest request)
	{
		String languageArray[] = request.getParameterValues("language_name");
		String languageKnown = "";
		for(int i=0;i<languageArray.length;i++)
		{
			languageKnown += languageArray[i] + ",";
		}
		
		if(languageKnown.length()>2)
		{
			languageKnown = languageKnown.substring(0, languageKnown.length()-1);
		}
		
		TranslatorModel eModel = new TranslatorModel();
		
		eModel.setTranslator_name(request.getParameter("name"));
		eModel.setTranslator_email_id(request.getParameter("email"));
		eModel.setTranslator_phone(request.getParameter("phone"));
		eModel.setTranslator_address(request.getParameter("address"));
		eModel.setTranslator_city(request.getParameter("city_name"));
		eModel.setCr_date(new Date());
		eModel.setTranslator_gender(request.getParameter("gender"));
		eModel.setKnown_language(languageKnown);
		
		return eModel;
	}
	
	public HotelLodgeModel setHotelLodgeModel(MultipartHttpServletRequest request)
	{
		HotelLodgeModel eModel = new HotelLodgeModel();
		
		eModel.setHotel_name(request.getParameter("name"));
		eModel.setHotel_email_id(request.getParameter("email"));
		eModel.setHotel_phone(request.getParameter("phone"));
		eModel.setHotel_address(request.getParameter("address"));
		eModel.setHotel_city(request.getParameter("city_name"));
		eModel.setCr_date(new Date());
		eModel.setType_of_food(request.getParameter("food_type"));
		eModel.setHotel_type(request.getParameter("hotelType"));
		
		return eModel;
	}
	
	public AttenderNurseModel setAttenderNurseModel(MultipartHttpServletRequest request)
	{
		AttenderNurseModel eModel = new AttenderNurseModel();
		
		eModel.setAttender_name(request.getParameter("name"));
		eModel.setAttender_email_id(request.getParameter("email"));
		eModel.setAttender_phone(request.getParameter("phone"));
		eModel.setAttender_address(request.getParameter("address"));
		eModel.setAttender_city(request.getParameter("city_name"));
		eModel.setCr_date(new Date());
		eModel.setAttender_gender(request.getParameter("gender"));
		
		return eModel;
	}
	
	public DoctorsModel setDoctorsModel(MultipartHttpServletRequest request)
	{
		DoctorsModel eModel = new DoctorsModel();
		
		eModel.setDoctor_name(request.getParameter("name"));
		eModel.setDoctor_email_id(request.getParameter("email"));
		eModel.setDoctor_phone(request.getParameter("phone"));
		eModel.setDoctor_address(request.getParameter("address"));
		eModel.setDoctor_city(request.getParameter("city_name"));
		eModel.setCr_date(new Date());
		eModel.setDoctor_specialist(request.getParameter("specialist"));
		eModel.setGender(request.getParameter("gender"));
		
		return eModel;
	}
	
	
	
	public RestaurentModel setRestaurentModel(MultipartHttpServletRequest request)
	{
		RestaurentModel eModel = new RestaurentModel();
		
		eModel.setRestaurent_name(request.getParameter("name"));
		eModel.setRestaurent_email_id(request.getParameter("email"));
		eModel.setRestaurent_phone(request.getParameter("phone"));
		eModel.setRestaurent_address(request.getParameter("address"));
		eModel.setRestaurent_city(request.getParameter("city_name"));
		eModel.setCr_date(new Date());
		eModel.setType_of_food(request.getParameter("food_type"));
		
		return eModel;
	}
	
	
	public TaxiServiceModel setTaxiServiceModel(MultipartHttpServletRequest request)
	{
		TaxiServiceModel eModel = new TaxiServiceModel();
		
		eModel.setService_name(request.getParameter("name"));
		eModel.setService_email_id(request.getParameter("email"));
		eModel.setService_phone(request.getParameter("phone"));
		eModel.setService_address(request.getParameter("address"));
		eModel.setService_city(request.getParameter("city_name"));
		eModel.setCr_date(new Date());
		eModel.setService_website(request.getParameter("websiteLink"));
		eModel.setService_type(request.getParameter("taxiType"));
		
		return eModel;
	}
	
	public List<UserBean> getConverUserModelToBean(List<UserRegistrationDetails> eModel)
	{
		 List<UserBean> userBean = new ArrayList<UserBean>();
		 
		 for(int i=0;i<eModel.size();i++)
		 {
			 UserBean eBean = new UserBean();
			 
			 eBean.setUser_full_name(eModel.get(i).getUser_full_name());
			 eBean.setUser_emailid(eModel.get(i).getUser_emailid());
			 eBean.setUser_mobile(eModel.get(i).getUser_mobile());
			 eBean.setUser_country(eModel.get(i).getUser_country());
			 eBean.setUser_state(eModel.get(i).getUser_state());
			 eBean.setUser_reg_time(eModel.get(i).getUser_reg_time());
			 
			 userBean.add(eBean);
		 }
		 
		 return userBean;
	}
	 
	
	
	
	public List<SecondOpinionBean> getConverOpinionModelToBean(List<OpinionDetails> eModel)
	{
		 List<SecondOpinionBean> userBean = new ArrayList<SecondOpinionBean>();
		 
		 for(int i=0;i<eModel.size();i++)
		 {
			 SecondOpinionBean eBean = new SecondOpinionBean();
			 
			 eBean.setOpinion_id(eModel.get(i).getOpinion_id());
			 eBean.setTreatment_on(eModel.get(i).getTreatment_on());
			 eBean.setDoctor_name(eModel.get(i).getDoctor_id());
			 eBean.setAppointment_date(eModel.get(i).getAppointment_date());
			 eBean.setRemark(eModel.get(i).getRemark());
			 eBean.setCr_date(eModel.get(i).getCr_date());
			 
			 List<UserRegistrationDetails> userModel = adminDao.getUserDetailsById(eModel.get(i).getUser_id());
			 
			 eBean.setUser_name(userModel.get(0).getUser_full_name());
			 eBean.setUser_emailId(userModel.get(0).getUser_emailid());
			 eBean.setUser_phone(userModel.get(0).getUser_mobile());
			 
			 userBean.add(eBean);
		 }
		 
		 return userBean;
	}
	
	
	
	
}
