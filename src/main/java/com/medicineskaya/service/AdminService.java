package com.medicineskaya.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.medicineskaya.bean.SecondOpinionBean;
import com.medicineskaya.bean.UserBean;
import com.medicineskaya.model.AdminDetails;
import com.medicineskaya.model.CategoryDetails;
import com.medicineskaya.model.CityModel;
import com.medicineskaya.model.ContactUsModel;
import com.medicineskaya.model.FoodTypeModel;
import com.medicineskaya.model.LanguageModel;
import com.medicineskaya.model.MedicalSpecialistModel;
import com.medicineskaya.model.OpinionDetails;
import com.medicineskaya.model.PriorityDoctorDetails;
import com.medicineskaya.model.ShelterType;
import com.medicineskaya.model.TouriestPlaceDetails;


public interface AdminService {

	
	public List<AdminDetails> getAdminDetails();
	
	public String getValidateAdmin(String user_id,String password);
	
	public List<CategoryDetails> getCategoryList();
	
	public List<CityModel> getCityList();
	
	public List<LanguageModel> getLanguageList();
	
	public List<FoodTypeModel> getFoodTypeList();
	
	public List<MedicalSpecialistModel> getDoctorSpecialist();
	
	public String saveCategoryDetails(MultipartHttpServletRequest request);
	
	public String saveTouristPlaceDetails(MultipartHttpServletRequest request,TouriestPlaceDetails placeModel);
	
	public String saveDoctorDetails(MultipartHttpServletRequest request,PriorityDoctorDetails doctorModel);
	
	public List<ShelterType> getShelterTypeList();
	
	public String saveAdminDetails(String name,String designation);
	
	public String saveAdminPassword(String password,String new_password);
	
	public String setAdminAccess(String access);
	
	public List<SecondOpinionBean> getOpenionReport(String fromDate,String toDate);
	
	public List<UserBean> getUserReport(String fromDate,String toDate);
	
	public List<ContactUsModel> getContactReport(String fromDate,String toDate);
	
	public List<ContactUsModel> getContactDetails(int patternId);
	
	public List<SecondOpinionBean> getOpinionDetails(int patternId);
	
}
