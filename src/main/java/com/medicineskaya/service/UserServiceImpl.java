package com.medicineskaya.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medicineskaya.bean.ResultCategoryBean;
import com.medicineskaya.bean.UserBean;
import com.medicineskaya.dao.AdminDao;
import com.medicineskaya.dao.UserDao;
import com.medicineskaya.model.AttenderNurseModel;
import com.medicineskaya.model.CategoryDetails;
import com.medicineskaya.model.CityModel;
import com.medicineskaya.model.ContactUsModel;
import com.medicineskaya.model.CountryModel;
import com.medicineskaya.model.DoctorsModel;
import com.medicineskaya.model.EmailModel;
import com.medicineskaya.model.EmbassyModel;
import com.medicineskaya.model.HospitalModel;
import com.medicineskaya.model.HotelLodgeModel;
import com.medicineskaya.model.LinkValidationModel;
import com.medicineskaya.model.OpinionDetails;
import com.medicineskaya.model.PriorityDoctorDetails;
import com.medicineskaya.model.RestaurentModel;
import com.medicineskaya.model.StateModel;
import com.medicineskaya.model.TaxiServiceModel;
import com.medicineskaya.model.TouriestPlaceDetails;
import com.medicineskaya.model.TranslatorModel;
import com.medicineskaya.model.UserRegistrationDetails;
import com.medicineskaya.util.EmailNotificationUtil;
import com.medicineskaya.util.EmailSentUtil;
import com.medicineskaya.util.PasswordGenerator;
import com.medicineskaya.util.UniquePatternGeneration;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao userDao;

	@Autowired
	private AdminDao adminDao;
	
	
	PasswordGenerator pwGenerator = new PasswordGenerator();
	
	EmailNotificationUtil emailUtil = new EmailNotificationUtil();
	
	
	public List<CategoryDetails> getCategoryDetails()
	{
		return userDao.getCategoryDetails();
	}
	
	public List<StateModel> getStateDetails()
	{
		return userDao.getStateDetails();
	}
	
	public List<CityModel> getCityDetails()
	{
		return userDao.getCityDetails();
	}
	
	public List<TouriestPlaceDetails> getTouriestPlaceDetails()
	{
		return userDao.getTouriestPlaceDetails();
	}
	
	public List<PriorityDoctorDetails> getPriorityDoctorDetails()
	{
		return userDao.getPriorityDoctorDetails();
	}
	
	
	public String checkUserAmbiguity(UserRegistrationDetails userDetails,String parameter)
	{
		return userDao.checkUserAmbiguity(userDetails,parameter);
	}
	
	public String getStateNames(String countryName)
	{
		String stateNames = "";
		
		List<StateModel> stateModel = userDao.getStateDetails();
		
		for(int i=0;i<stateModel.size();i++)
		{
			stateNames += stateModel.get(i).getState_name() + ",";
		}
		
		stateNames = stateNames.substring(0, stateNames.length()-1);
		
		return stateNames;
	}
	
	public String saveUserRegisterDetails(UserRegistrationDetails userDetails)
	{
		String saveStatus = "";
		
		try {
			
			userDetails.setUser_password(pwGenerator.encrypt(userDetails.getUser_password()));
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		userDetails.setUser_reg_time(new Date());
		userDetails.setUser_status("Active");
		
		saveStatus = userDao.saveUserRegisterDetails(userDetails);
		
		if(saveStatus.equals("success"))
		{
			String validateId = "";
			
			String link = "http://www.medicineskaya.com";
			
			String reasonFor = "";
			
			String extraInfo = "";
			
			String emailBody = emailUtil.emailNotification(userDetails.getUser_full_name(),userDetails.getUser_emailid(),"resources/wellcomeRegistration",link,"Go to Home",reasonFor,extraInfo);
				
			String mailStatus = "";
			
			ResourceBundle resource = ResourceBundle.getBundle("resources/wellcomeRegistration");
		 	 
			String mailSubject = resource.getString("mailSubject");
			
			/* SENDING MAIL */
		     
			 
			    EmailSentUtil emailStatus = new EmailSentUtil();
	 	        
				try{
					mailStatus = emailStatus.getEmailSent(userDetails.getUser_emailid(),mailSubject,emailBody);
				} 
				catch (Exception e) {
					
					// TODO Auto-generated catch block
					mailStatus = "failed";
					e.printStackTrace();
			 	}
			 		
		     
		    /* END OF SENDING MAIL */
					
			
			if(!mailStatus.equals("success"))
			{
				EmailModel emailDetail = new EmailModel();
				
				emailDetail.setEmail_id(userDetails.getUser_emailid());
				emailDetail.setEmail_subject(mailSubject);
				emailDetail.setEmail_content(emailBody);
				emailDetail.setEmail_status("failed");
				emailDetail.setCr_date(new Date());
				
				adminDao.getSaveEmailDetails(emailDetail);
			}
			
		    return saveStatus;
		}
		else
		{
			
		}
		
		return saveStatus;
		
	}
	
	
	public String getValidateUser(String userName,String password)
	{
		String status = "";
		
		try {
					
			String decryptPassword = pwGenerator.encrypt(password);
			status = userDao.getValidateUser(userName,decryptPassword);			
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 			
		return status;
	}
	
	public int getUserIdVieEmailId(String userName)
	{
		return userDao.getUserIdVieEmailId(userName);
	}
	
	
	public UserBean getUserDetails(int userId)
	{
		UserBean userbean = new UserBean();
		
		List<UserRegistrationDetails> userDetails = userDao.getUserDetails(userId);
		
		userbean = converUserModelToBean(userDetails);
		
		return userbean;
	}
	
	public String getCategoryValues(String category,String cityName)
	{
		String result = userDao.getCategoryValues(category,cityName);
		
		
		
		return result;
	}
	
	public List<ResultCategoryBean> getSearchRequiredDetails(String category,String cityName,String valueName)
	{
		List<ResultCategoryBean> resultBean = new ArrayList<ResultCategoryBean>();
		
		if(category.equals("Embassy"))
		{
			List<EmbassyModel> resultModel = userDao.getSearchEmbassy(cityName,valueName);
			
			resultBean = getConvertEmbassyModelToBean(resultModel);
		}
		else if(category.equals("Hospital"))
		{	
			List<HospitalModel> resultModel = userDao.getSearchHospital(cityName,valueName);
			
			resultBean = getConvertHospitalModelToBean(resultModel);
		}
		else if(category.equals("Translator"))
		{
			List<TranslatorModel> resultModel = userDao.getSearchTranslator(cityName,valueName);
			
			resultBean = getConvertTranslatorModelToBean(resultModel);
		}
		else if(category.equals("Hotel and Lodge"))
		{		
			List<HotelLodgeModel> resultModel = userDao.getSearchHotel(cityName, valueName);	
			
			resultBean = getConvertHotelLodgeModelToBean(resultModel);
		}
		else if(category.equals("Attender and Nurse"))
		{		
			List<AttenderNurseModel> resultModel = userDao.getSearchAttender(cityName, valueName);
			
			resultBean = getConvertAttenderNurseModelToBean(resultModel);
		}
		else if(category.equals("Doctor"))
		{		
			List<DoctorsModel> resultModel = userDao.getSearchDoctor(cityName, valueName);		
			
			resultBean = getConvertDoctorsModelToBean(resultModel);
		}
		else if(category.equals("Restaurent"))
		{
			List<RestaurentModel> resultModel = userDao.getSearchRestaurent(cityName, valueName);		
			
			resultBean = getConvertRestaurentModelToBean(resultModel);
		}
		else if(category.equals("Type of food"))
		{
			List<RestaurentModel> resultModel = userDao.getSearchRestaurent(cityName, valueName);		
			
			resultBean = getConvertRestaurentModelToBean(resultModel);
		}
		else 
		{		
			List<TaxiServiceModel> resultModel = userDao.getSearchTaxi(cityName, valueName);	
			
			resultBean = getConvertTaxiServiceModelToBean(resultModel);
		}
		
		return resultBean;
	}
	
	public List<ResultCategoryBean> getViewMoreAboutCategory(int id,String categoryType)
	{
		List<ResultCategoryBean> resultBean = new ArrayList<ResultCategoryBean>();
		
		String condition = "";
		
		if(categoryType.equals("Embassy"))
		{
			List<EmbassyModel> resultModel = userDao.getEmbassyDetails(id,condition);
			
			resultBean = getConvertEmbassyModelToBean(resultModel);
		}
		else if(categoryType.equals("Hospital"))
		{	
			List<HospitalModel> resultModel = userDao.getHospitalDetails(id,condition);
			
			resultBean = getConvertHospitalModelToBean(resultModel);
		}
		else if(categoryType.equals("Translator"))
		{
			List<TranslatorModel> resultModel = userDao.getTranslatorDetails(id,condition);
			
			resultBean = getConvertTranslatorModelToBean(resultModel);
		}
		else if(categoryType.equals("Hotel and Lodge"))
		{		
			List<HotelLodgeModel> resultModel = userDao.getHotelDetails(id,condition);	
			
			resultBean = getConvertHotelLodgeModelToBean(resultModel);
		}
		else if(categoryType.equals("Attender and Nurse"))
		{		
			List<AttenderNurseModel> resultModel = userDao.getAttenderDetails(id,condition);
			
			resultBean = getConvertAttenderNurseModelToBean(resultModel);
		}
		else if(categoryType.equals("Doctor"))
		{		
			List<DoctorsModel> resultModel = userDao.getDoctorDetails(id,condition);		
			
			resultBean = getConvertDoctorsModelToBean(resultModel);
		}
		else if(categoryType.equals("Restaurent"))
		{
			List<RestaurentModel> resultModel = userDao.getRestaurentDetails(id,condition);		
			
			resultBean = getConvertRestaurentModelToBean(resultModel);
		}
		else 
		{		
			List<TaxiServiceModel> resultModel = userDao.getTaxiDetails(id,condition);	
			
			resultBean = getConvertTaxiServiceModelToBean(resultModel);
		}
		
		return resultBean;
	}
	
	
	public List<TouriestPlaceDetails> getSearchTouriestPlaceDetails(int placeId)
	{
		return userDao.getSearchTouriestPlaceDetails(placeId);
	}
	
	public List<PriorityDoctorDetails> getSearchDoctorDetails(int doctorId)
	{
		return userDao.getSearchDoctorDetails(doctorId);
	}
	
	public String updatePassword(int userId,String currentPw,String newPw)
	{
		String status = "";
		
		String encryptCurrentPw = "";
		String encryptNewPw = "";
		
		try {
			
			encryptCurrentPw += pwGenerator.encrypt(currentPw);
			
			encryptNewPw += pwGenerator.encrypt(newPw);
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		status = userDao.checkCurrentPassword(userId,encryptCurrentPw);
		
		if(status.equals("Matched"))
		{
			status = userDao.updateCurrentPassword(userId,encryptNewPw);
			
			return status;
		}
		else
		{
			return status;
		}
				
	}
	
	
	
	
	@SuppressWarnings("static-access")
	public String sendLinkForgetPassword(String emailId)
	{
		String validateId = "";
		
		LinkValidationModel linkValidate = new LinkValidationModel();
		
		linkValidate.setValidate_email_id(emailId);
		
		try {
			
			UniquePatternGeneration generatorObject = new UniquePatternGeneration();
			
			validateId += generatorObject.generateUniqueCode(emailId);
			
			linkValidate.setValidate_id(validateId);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		String link = "http://www.medicineskaya.com/resetPassword?requnIkedij="+validateId+"";
		
		String reasonFor = "ResetPassword";
		
		String extraInfo = "NotRequired";
		
		String emailBody = emailUtil.emailNotification("User",emailId,"resources/forgetPassword",link,"Reset Passwod",reasonFor,extraInfo);
		
		
		
		String mailStatus = "";
		
		ResourceBundle resource = ResourceBundle.getBundle("resources/forgetPassword");
	 	 
		String mailSubject = resource.getString("mailSubject");
		
		
		/* SENDING MAIL */
	    
		
			    EmailSentUtil emailStatus = new EmailSentUtil();
	 	        
				try {
					mailStatus = emailStatus.getEmailSent(emailId,mailSubject,emailBody);
				} 
				catch (Exception e) {
					
					// TODO Auto-generated catch block
					mailStatus = "failed";
					e.printStackTrace();
				}
				
	  
	    /* END OF SENDING MAIL */
				
		
		if(!mailStatus.equals("success"))
		{
			EmailModel emailDetail = new EmailModel();
			
			emailDetail.setEmail_id(emailId);
			emailDetail.setEmail_subject(mailSubject);
			emailDetail.setEmail_content(emailBody);
			emailDetail.setEmail_status("failed");
			emailDetail.setCr_date(new Date());
			
			adminDao.getSaveEmailDetails(emailDetail);
		}
		
		
		
	    if(mailStatus.equals("success"))
	    {
	    	// Save email sent details
	    	
			linkValidate.setValidate_status("Pending");
			linkValidate.setValidate_init_date(new Date());
			linkValidate.setValidate_for("Forget Password");			
			
			userDao.saveLinkValidatedetails(linkValidate);
	    }
	    else
	    {
	    	
	    }	    
	    	return mailStatus;
	}
	
	
	
	
	public String getEmailIdFromForgetPassword(String uniqueId)
	{
		return userDao.getEmailIdFromForgetPassword(uniqueId);
	}
	
	public String getChangePassword(String emailId,String password)
	{
		String encPassword = "";
		try {
					
			System.out.println("password="+password);
			encPassword += pwGenerator.encrypt(password);
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return userDao.getChangePassword(emailId,encPassword);
	}
	
	
	public List<CountryModel> getCountryList()
	{
		return userDao.getCountryList();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//////////////  CONVERT USER MODEL TO USER BEAN   ///////////////////////////////
	
	public UserBean converUserModelToBean(List<UserRegistrationDetails> userDetails)
	{
		UserBean userbean = new UserBean();
		
		for(int i=0;i<userDetails.size();i++)
		{
			userbean.setUser_emailid(userDetails.get(0).getUser_emailid());
			userbean.setUser_full_name(userDetails.get(0).getUser_full_name());
			userbean.setUser_id(userDetails.get(0).getUser_id());
			userbean.setUser_mobile(userDetails.get(0).getUser_mobile());
			userbean.setUser_reg_time(userDetails.get(0).getUser_reg_time());
			userbean.setUser_status(userDetails.get(0).getUser_status());			
		}
		
		return userbean;
	}
	
	
	List<ResultCategoryBean> getConvertEmbassyModelToBean(List<EmbassyModel> resultModel)
	{
		List<ResultCategoryBean> eBeanList = new ArrayList<ResultCategoryBean>();
		
		for(int i=0;i<resultModel.size();i++)
		{
			ResultCategoryBean eBean = new ResultCategoryBean();
			
			eBean.setId(resultModel.get(i).getEmbassy_id());
			eBean.setType("Embassy");
			eBean.setName(resultModel.get(i).getEmbassy_name());
			eBean.setEmail_id(resultModel.get(i).getEmbassy_email_id());
			eBean.setPhone(resultModel.get(i).getEmbassy_phone());
			eBean.setCity(resultModel.get(i).getEmbassy_city());
			eBean.setAddress(resultModel.get(i).getEmbassy_address());
			eBean.setPhoto(resultModel.get(i).getEmbassy_photo());
			eBean.setCr_date(resultModel.get(i).getCr_date());
			
			eBeanList.add(eBean);
		}
		
		return eBeanList;
	}
	
	List<ResultCategoryBean> getConvertHospitalModelToBean(List<HospitalModel> resultModel)
	{
		List<ResultCategoryBean> eBeanList = new ArrayList<ResultCategoryBean>();
		
		for(int i=0;i<resultModel.size();i++)
		{
			ResultCategoryBean eBean = new ResultCategoryBean();
			
			eBean.setId(resultModel.get(i).getHospital_id());
			eBean.setType("Hospital");
			eBean.setName(resultModel.get(i).getHospital_name());
			eBean.setEmail_id(resultModel.get(i).getHospital_email_id());
			eBean.setPhone(resultModel.get(i).getHospital_phone());
			eBean.setCity(resultModel.get(i).getHospital_city());
			eBean.setAddress(resultModel.get(i).getHospital_address());
			eBean.setPhoto(resultModel.get(i).getHospital_photo());
			eBean.setCr_date(resultModel.get(i).getCr_date());
			
			eBeanList.add(eBean);
		}
		
		return eBeanList;
	}
	
	
	List<ResultCategoryBean> getConvertTranslatorModelToBean(List<TranslatorModel> resultModel)
	{
		List<ResultCategoryBean> eBeanList = new ArrayList<ResultCategoryBean>();
		
		for(int i=0;i<resultModel.size();i++)
		{
			ResultCategoryBean eBean = new ResultCategoryBean();
			
			eBean.setId(resultModel.get(i).getTranslator_id());
			eBean.setType("Translator");
			eBean.setName(resultModel.get(i).getTranslator_name());
			eBean.setEmail_id(resultModel.get(i).getTranslator_email_id());
			eBean.setPhone(resultModel.get(i).getTranslator_phone());
			eBean.setCity(resultModel.get(i).getTranslator_city());
			eBean.setAddress(resultModel.get(i).getTranslator_address());
			eBean.setPhoto(resultModel.get(i).getTranslator_photo());
			eBean.setCr_date(resultModel.get(i).getCr_date());
			eBean.setLanguage_known(resultModel.get(i).getKnown_language());
			eBean.setGender(resultModel.get(i).getTranslator_gender());
			
			eBeanList.add(eBean);
		}
		
		return eBeanList;
	}
	
	List<ResultCategoryBean> getConvertHotelLodgeModelToBean(List<HotelLodgeModel> resultModel)
	{
		List<ResultCategoryBean> eBeanList = new ArrayList<ResultCategoryBean>();
		
		for(int i=0;i<resultModel.size();i++)
		{
			ResultCategoryBean eBean = new ResultCategoryBean();
			
			eBean.setId(resultModel.get(i).getHotel_id());
			eBean.setType("Hotel and Lodge");
			eBean.setName(resultModel.get(i).getHotel_name());
			eBean.setEmail_id(resultModel.get(i).getHotel_email_id());
			eBean.setPhone(resultModel.get(i).getHotel_phone());
			eBean.setCity(resultModel.get(i).getHotel_city());
			eBean.setAddress(resultModel.get(i).getHotel_address());
			eBean.setPhoto(resultModel.get(i).getHotel_photo());
			eBean.setCr_date(resultModel.get(i).getCr_date());
			eBean.setType_of_food(resultModel.get(i).getType_of_food());
			eBean.setSub_type(resultModel.get(i).getHotel_type());
			
			eBeanList.add(eBean);
		}
		
		return eBeanList;
	}
	
	
	List<ResultCategoryBean> getConvertAttenderNurseModelToBean(List<AttenderNurseModel> resultModel)
	{
		List<ResultCategoryBean> eBeanList = new ArrayList<ResultCategoryBean>();
		
		for(int i=0;i<resultModel.size();i++)
		{
			ResultCategoryBean eBean = new ResultCategoryBean();
			
			eBean.setId(resultModel.get(i).getAttender_id());
			eBean.setType("Attender and Nurse");
			eBean.setName(resultModel.get(i).getAttender_name());
			eBean.setEmail_id(resultModel.get(i).getAttender_email_id());
			eBean.setPhone(resultModel.get(i).getAttender_phone());
			eBean.setCity(resultModel.get(i).getAttender_city());
			eBean.setAddress(resultModel.get(i).getAttender_address());
			eBean.setPhoto(resultModel.get(i).getAttender_photo());
			eBean.setCr_date(resultModel.get(i).getCr_date());
			eBean.setGender(resultModel.get(i).getAttender_gender());
			
			eBeanList.add(eBean);
		}
		
		return eBeanList;
	}
	
	List<ResultCategoryBean> getConvertDoctorsModelToBean(List<DoctorsModel> resultModel)
	{
		List<ResultCategoryBean> eBeanList = new ArrayList<ResultCategoryBean>();
		
		for(int i=0;i<resultModel.size();i++)
		{
			ResultCategoryBean eBean = new ResultCategoryBean();
			
			eBean.setId(resultModel.get(i).getDoctor_id());
			eBean.setType("Doctor");
			eBean.setName(resultModel.get(i).getDoctor_name());
			eBean.setEmail_id(resultModel.get(i).getDoctor_email_id());
			eBean.setPhone(resultModel.get(i).getDoctor_phone());
			eBean.setCity(resultModel.get(i).getDoctor_city());
			eBean.setAddress(resultModel.get(i).getDoctor_address());
			eBean.setPhoto(resultModel.get(i).getDoctor_photo());
			eBean.setCr_date(resultModel.get(i).getCr_date());
			eBean.setSpecialization(resultModel.get(i).getDoctor_specialist());
			eBean.setGender(resultModel.get(i).getGender());
			
			eBeanList.add(eBean);
		}
		
		return eBeanList;
	}
	
	List<ResultCategoryBean> getConvertRestaurentModelToBean(List<RestaurentModel> resultModel)
	{
		List<ResultCategoryBean> eBeanList = new ArrayList<ResultCategoryBean>();
		
		for(int i=0;i<resultModel.size();i++)
		{
			ResultCategoryBean eBean = new ResultCategoryBean();
			
			eBean.setId(resultModel.get(i).getRestaurent_id());
			eBean.setType("Restaurent");
			eBean.setName(resultModel.get(i).getRestaurent_name());
			eBean.setEmail_id(resultModel.get(i).getRestaurent_email_id());
			eBean.setPhone(resultModel.get(i).getRestaurent_phone());
			eBean.setCity(resultModel.get(i).getRestaurent_city());
			eBean.setAddress(resultModel.get(i).getRestaurent_address());
			eBean.setPhoto(resultModel.get(i).getRestaurent_photo());
			eBean.setCr_date(resultModel.get(i).getCr_date());
			eBean.setType_of_food(resultModel.get(i).getType_of_food());
			
			eBeanList.add(eBean);
		}
		
		return eBeanList;
	}
	
	
	List<ResultCategoryBean> getConvertTaxiServiceModelToBean(List<TaxiServiceModel> resultModel)
	{
		List<ResultCategoryBean> eBeanList = new ArrayList<ResultCategoryBean>();
		
		for(int i=0;i<resultModel.size();i++)
		{
			ResultCategoryBean eBean = new ResultCategoryBean();
			
			eBean.setId(resultModel.get(i).getService_id());
			eBean.setType("Taxi Service");
			eBean.setName(resultModel.get(i).getService_name());
			eBean.setEmail_id(resultModel.get(i).getService_email_id());
			eBean.setPhone(resultModel.get(i).getService_phone());
			eBean.setCity(resultModel.get(i).getService_city());
			eBean.setAddress(resultModel.get(i).getService_address());
			eBean.setPhoto(resultModel.get(i).getService_photo());
			eBean.setCr_date(resultModel.get(i).getCr_date());
			eBean.setUrlLink(resultModel.get(i).getService_website());
			eBean.setSub_type(resultModel.get(i).getService_type());
			
			eBeanList.add(eBean);
		}
		
		return eBeanList;
	}
	
	
	
	public String saveContactDetails(ContactUsModel contactModel)
	{
		contactModel.setContact_date(new Date());
		contactModel.setContact_status("Not Viewed");
		
		String status = userDao.saveContactDetails(contactModel);
		 
		if(status.equals("success"))
		{
			String validateId = "";
			
			String link = "http://www.medicineskaya.com";
			
			String reasonFor = "";
			
			String extraInfo = "";
			
			String emailBody = emailUtil.emailNotification(contactModel.getContact_name(),contactModel.getContact_email_id(),"resources/contactUs",link,"Go to Home",reasonFor,extraInfo);
				
			String mailStatus = "";
			
			ResourceBundle resource = ResourceBundle.getBundle("resources/contactUs");
		 	 
			String mailSubject = resource.getString("mailSubject");
			
			/* SENDING MAIL */
		    
			
			    EmailSentUtil emailStatus = new EmailSentUtil();
	 	        
				try {
					mailStatus = emailStatus.getEmailSent(contactModel.getContact_email_id(),mailSubject,emailBody);
				} 
				catch (Exception e) {
					
					// TODO Auto-generated catch block
					mailStatus = "failed";
					e.printStackTrace();
				}
					
		  
		    /* END OF SENDING MAIL */
					
			
			if(!mailStatus.equals("success"))
			{
				EmailModel emailDetail = new EmailModel();
				
				emailDetail.setEmail_id(contactModel.getContact_email_id());
				emailDetail.setEmail_subject(mailSubject);
				emailDetail.setEmail_content(emailBody);
				emailDetail.setEmail_status("failed");
				emailDetail.setCr_date(new Date());
				
				adminDao.getSaveEmailDetails(emailDetail);
			}
			
		    return status;
		}
		else
		{
			
		}
		 
		 
		return status;
	}
	
	public String getDoctorList(String specialization)
	{
		String result = "";
		
		List<DoctorsModel> doctorList = userDao.getDoctorList(specialization);
		
		for(int i=0;i<doctorList.size();i++)
		{
			result += doctorList.get(i).getDoctor_name() + ",";
		}
		
		if(result.length()>2)
		{
			result = result.substring(0,result.length()-1);
		}
		
		return result;
	}
	
	
	
	public String saveSecondOpinion(OpinionDetails opinionModel)
	{
		opinionModel.setCr_date(new Date());
		
		
		
		String status = userDao.saveSecondOpinion(opinionModel);
		 
		if(status.equals("success"))
		{			
			List<UserRegistrationDetails> userDetails = userDao.getUserDetails(opinionModel.getUser_id());
			
			String validateId = "";
			
			String link = "http://www.medicineskaya.com";
			
			String reasonFor = "";
			
			String extraInfo = "";
			
			String emailBody = emailUtil.emailNotification(userDetails.get(0).getUser_full_name(),userDetails.get(0).getUser_emailid(),"resources/secondOpinion",link,"Go to Home",reasonFor,extraInfo);
				
			String mailStatus = "";
			
			ResourceBundle resource = ResourceBundle.getBundle("resources/secondOpinion");
		 	 
			String mailSubject = resource.getString("mailSubject");
			
			/* SENDING MAIL */
		    
			
			    EmailSentUtil emailStatus = new EmailSentUtil();
	 	        
				try {
					mailStatus = emailStatus.getEmailSent(userDetails.get(0).getUser_emailid(),mailSubject,emailBody);
				} 
				catch (Exception e) {
					
					// TODO Auto-generated catch block
					mailStatus = "failed";
					e.printStackTrace();
				}
					
		  
		    /* END OF SENDING MAIL */
					
			
			if(!mailStatus.equals("success"))
			{
				EmailModel emailDetail = new EmailModel();
				
				emailDetail.setEmail_id(userDetails.get(0).getUser_emailid());
				emailDetail.setEmail_subject(mailSubject);
				emailDetail.setEmail_content(emailBody);
				emailDetail.setEmail_status("failed");
				emailDetail.setCr_date(new Date());
				
				adminDao.getSaveEmailDetails(emailDetail);
			}
			
		    return status;
		}
		else
		{
			
		}
		 
		 
		return status;
	}
	
	
	
}
